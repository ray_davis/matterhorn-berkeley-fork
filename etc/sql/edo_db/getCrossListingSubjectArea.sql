SELECT DISTINCT
  xlist."catalogNumber-formatted" AS catalog_id,
  xlist."subjectArea" AS dept_name
FROM
  SISEDO.API_CROSSLISTINGSV00_VW xlist
JOIN SISEDO.API_COURSEV00_VW crs ON (
  xlist."cms-version-independent-id" = crs."cms-version-independent-id"
)
WHERE xlist."displayName" LIKE '${displayName}'
