SELECT
  mtg.course_cntl_num,
  mtg.term_yr,
  mtg.term_cd,
  mtg.building_name,
  mtg.room_number,
  mtg.meeting_days,
  mtg.meeting_start_time,
  mtg.meeting_start_time_ampm_flag,
  mtg.meeting_end_time,
  mtg.meeting_end_time_ampm_flag,
  mtg.print_cd,
  course.dept_name,
  course.dept_description,
  course.catalog_id,
  course.section_num,
  course.course_title,
  term.term_name,
  term.term_start_date,
  term.term_end_date
FROM WEBCAST_class_schedule_vw mtg
JOIN WEBCAST_course_info_vw course ON (
  mtg.term_cd = course.term_cd
  AND mtg.term_yr = course.term_yr
  AND mtg.course_cntl_num = course.course_cntl_num
)
JOIN WEBCAST_term_info_vw term ON (
  course.term_yr = (term.term_yr + 2000)
  AND course.term_cd = term.term_cd
)
WHERE
  mtg.TERM_CD='${termCd}'
  AND mtg.TERM_YR=${termYr}
  AND mtg.course_cntl_num=${ccn}
  AND mtg.building_name IS NOT NULL
  AND mtg.meeting_days IS NOT NULL
