SELECT
  mtg.course_cntl_num,
  mtg.building_name,
  mtg.room_number,
  mtg.meeting_days,
  mtg.meeting_start_time,
  mtg.meeting_start_time_ampm_flag,
  mtg.meeting_end_time,
  mtg.meeting_end_time_ampm_flag,
  mtg.print_cd,
  course.dept_name,
  course.dept_description,
  course.catalog_id,
  course.section_num,
  course.course_title,
  COUNT(roster.student_ldap_uid) AS student_count
FROM WEBCAST_class_schedule_vw mtg
JOIN WEBCAST_course_info_vw course ON (
  mtg.term_cd = course.term_cd
  AND mtg.term_yr = course.term_yr
  AND mtg.course_cntl_num = course.course_cntl_num
)
JOIN WEBCAST_class_roster_vw roster ON (
  mtg.term_cd = roster.term_cd
  AND mtg.term_yr = roster.term_yr
  AND mtg.course_cntl_num = roster.course_cntl_num
)
WHERE
  mtg.TERM_CD='${termCd}'
  AND mtg.TERM_YR=${termYr}
  AND course.primary_secondary_cd = 'P'
  AND course.section_cancel_flag is null
  AND (mtg.print_cd is null or mtg.print_cd != 'C')
  AND mtg.building_name IS NOT NULL
  AND mtg.meeting_days IS NOT NULL
GROUP BY
  mtg.course_cntl_num,
  mtg.term_yr,
  mtg.term_cd,
  mtg.building_name,
  mtg.room_number,
  mtg.meeting_days,
  mtg.meeting_start_time,
  mtg.meeting_start_time_ampm_flag,
  mtg.meeting_end_time,
  mtg.meeting_end_time_ampm_flag,
  mtg.print_cd,
  course.dept_name,
  course.dept_description,
  course.catalog_id,
  course.section_num,
  course.course_title
ORDER BY
  mtg.course_cntl_num
