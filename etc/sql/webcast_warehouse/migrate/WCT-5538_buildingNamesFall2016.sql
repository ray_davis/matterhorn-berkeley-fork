--\set ON_ERROR_ROLLBACK on
--
--BEGIN;
--
--CREATE TYPE e_building_name_v2 AS ENUM ('Barker', 'Barrows', 'Birge', 'Boalt', 'Cory', 'Davis', 'DonnerLab', 'Dwinelle',
--  'Etcheverry', 'Evans', 'Giannini', 'GPB', 'GSPP', 'Haas', 'HearstMin', 'Hertz', 'Kroeber', 'Latimer', 'LeConte',
--  'Lewis', 'LiKaShing', 'McCone', 'McLaughlin', 'Moffitt', 'Morgan', 'Mulford', 'NorthGate', 'OffCampus',
--  'PauleyBallroom', 'Pimentel', 'Soda', 'Stanley', 'Tan', 'Tolman', 'ValleyLSB', 'ValleyLifeSciences', 'Wheeler', 'Wurster');
--
--ALTER TABLE course
--  ALTER building_name TYPE e_building_name_v2
--  USING (building_name::text::e_building_name_v2);
--
--DROP TYPE e_building_name;
--
--ALTER TYPE e_building_name_v2 RENAME TO e_building_name;

ALTER TYPE e_building_name ADD VALUE 'Hertz' AFTER 'HearstMin';
ALTER TYPE e_building_name ADD VALUE 'PauleyBallroom' AFTER 'OffCampus';
ALTER TYPE e_building_name ADD VALUE 'ValleyLifeSciences' AFTER 'Tolman';

--END;
