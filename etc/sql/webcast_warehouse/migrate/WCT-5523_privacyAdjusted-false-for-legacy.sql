UPDATE
  recording
SET
  video_status_adjusted=true
WHERE
  year < 2016
  OR (year = 2016 AND semester_code = 'B')
