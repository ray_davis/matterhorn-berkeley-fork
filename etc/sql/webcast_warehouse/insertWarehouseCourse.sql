<#import "macros.ftl" as macros>

INSERT INTO
  course
  (
    year, semester_code, ccn, catalog_id, title, department_name, section, description, audience_type, recording_type,
    license_type, building_name, room_number, instructor_names, youtube_playlist_id, meeting_days,
    itunes_audio_collection_id, itunes_video_collection_id, rss_feed_audio, rss_feed_video, rss_feed_screen
  )
VALUES
  (
    ${course.year?c},
    '${course.semester.termCode}',
    ${course.ccn?c},
    <#if course.catalogId??>'<@macros.escapeSql course.catalogId />'<#else>null</#if>,
    '<@macros.escapeSql title />',
    <#if department_name??>'<@macros.escapeSql department_name />'<#else>null</#if>,
    <#if course.section??>'<@macros.escapeSql course.section />'<#else>null</#if>,
    <#if description??>'<@macros.escapeSql description />'<#else>null</#if>,
    '${audience_type}',
    '${recording_type}',
    '${license_type}',
    <#if course.room??>
        <#if course.room.building??>'<@macros.escapeSql course.room.building.name() />'<#else>null</#if>,
        <#if course.room.roomNumber??>'<@macros.escapeSql course.room.roomNumber />'<#else>null</#if>,
    <#else>
        null, null,
    </#if>
    <#if course.instructors?? && (course.instructors?size > 0)>
        ARRAY[
            <#list course.instructors as instructor>
            '<@macros.escapeSql instructor />'<#if instructor_has_next>,</#if>
            </#list>
        ]
    <#else>
        null
    </#if>,
    <#if course.youTubePlaylistId??>'<@macros.escapeSql course.youTubePlaylistId />'<#else>null</#if>,
    <#if course.meetingDays?? && (course.meetingDays?size > 0)>
        '{
        <#list course.meetingDays as meetingDay>
            "${meetingDay.name()?substring(0, 3)}"<#if meetingDay_has_next>,</#if>
        </#list>
        }'
    <#else>
        null
    </#if>,
    <#if course.ITunesAudioId??>'${course.ITunesAudioId?c}'<#else>null</#if>,
    <#if course.ITunesVideoId??>'${course.ITunesVideoId?c}'<#else>null</#if>,
    <#if course.audioFeedRSS??>'<@macros.escapeSql course.audioFeedRSS />'<#else>null</#if>,
    <#if course.videoFeedRSS??>'<@macros.escapeSql course.videoFeedRSS />'<#else>null</#if>,
    <#if course.screenFeedRSS??>'<@macros.escapeSql course.screenFeedRSS />'<#else>null</#if>
  )
