<#import "macros.ftl" as macros>

UPDATE
  recording
SET
  treated_by_selenium=true
WHERE
  year=${year?c}
  AND
  semester_code='${semester_code}'
  AND
  youtube_video_id='${youtube_video_id}'
