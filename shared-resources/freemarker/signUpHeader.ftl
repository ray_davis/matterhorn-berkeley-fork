<#include "macros.ftl" >

<#assign startTimeMinutes><#if startTime??>${(startTime.minutes < 10)?string('0' + startTime.minutes, startTime.minutes)}</#if></#assign>
<#assign endTimeMinutes><#if endTime??>${(endTime.minutes < 10)?string('0' + endTime.minutes, endTime.minutes)}</#if></#assign>

<div id="log-out"><a href="/j_spring_security_logout">Log out</a></div>
<div id="ets-logo"><img src="/img/mh_logos/ets_logo.png"/></div>

<h1><#if signUpH1??>${signUpH1}<#else><@message 'defaultPageH1' /></#if></h1>


<#if courseOffering??>
    <#assign courseName = courseOffering.canonicalCourse.name />
    <h2 class="course-name">${courseName}</h2>
    <h3>
        ${courseOffering.room.building} ${courseOffering.room.roomNumber!""} &nbsp;|&nbsp;
        <#list courseOffering.meetingDays as meetingDay>
            ${meetingDay}<#if meetingDay_has_next>,</#if>
        </#list>
        <@showTimeOfDay startTime />-<@showTimeOfDay endTime />
    </h3>
<#else>
    <h2 class="course-name">Sorry, There is a Problem</h2>
</#if>

<#if participationList??>
    <#-- List instructors, bold when current user is match. -->
    <p id="instructorList">Instructor<#if (participationList?size > 1)>s</#if>:
        <#assign isAuthUser = false />
        <#list participationList as participant>
            <#assign instructor = participant.instructor />
            <#assign isCurrentUser = user.userName == instructor.calNetUID />
                <#if isCurrentUser>
                    <#assign isAuthUser = isCurrentUser />
                                                        
                </#if>    
            ${isCurrentUser?string("<b>","")}
            ${instructor.firstName} ${instructor.lastName}
            ${isCurrentUser?string("</b>","")}

            <#if isAdminUser() || isAuthUser> 
                (${participant.approved?string("","not yet ")}signed up)
            </#if>
            <#if participant_has_next>,</#if>
        </#list>
    </p>
</#if>
