<#include "macros.ftl" >

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><@message 'signUpPageTitle' /></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="shortcut icon" href="/img/favicon.ico" />

        <link rel="stylesheet" href="${cssAndJsURLPrefix}/css/sign-up.css"/>
        <link rel="stylesheet" href="${cssAndJsURLPrefix}/css/angular/ngDialog.css">

        <script type="text/javascript" src="${cssAndJsURLPrefix}/js/angular/angular.min.js"></script>
        <script type="text/javascript" src="${cssAndJsURLPrefix}/js/angular/ngDialog.min.js"></script>
        <script type="text/javascript" src="${cssAndJsURLPrefix}/js/sign-up.js"></script>
    </head>
    <body>
        <div id="pageBody" class="cct-container-main">

            <#include "signUpHeader.ftl" />

            <div class="major-section">
                <#if courseOffering??>
                    <#if hasGlobalErrors()>
                        <p class="instructional-text"><@showGlobalErrors /></p>
                    <#elseif courseOffering.scheduled>
                        <p class="instructional-text"><@message 'recordingsHaveBeenScheduled' /></p>
                    <#else>
                        <p class="instructional-text"><@message 'recordingsWillBeScheduledWhen' /></p>
                    </#if>

                    <#assign cp = courseOffering.capturePreferences />
                        <#if cp.recordingType??>
                            <div id="recordingTypeDiv">
                                <b>Recording Type:</b> <@message "${cp.recordingType}" />
                            </div>
                            <div id="recordingTimeDiv">
                                <b>Recording Time:</b>
                                <#list courseOffering.meetingDays as meetingDay>
                                    ${meetingDay}<#if meetingDay_has_next>,</#if>
                                </#list>
                                <@showTimeOfDay offsetStartTime />-<@showTimeOfDay offsetEndTime />
                            </div>

<#--
NOTE: Per WCT-5386, we no longer give users the availability choice.
                            <div id="availabilityDiv">
                                <b>Availability:</b> <#if cp.recordingAvailability??><@message "${cp.recordingAvailability}" /><#else>--</#if>
                            </div>
-->
                            <div id="publishDelayDiv">
                                <b>Publish Delay:</b>
                                <#if cp.delayPublishByDays == 0>
                                    As soon after capture as possible
                                <#else>
                                    ${cp.delayPublishByDays} day<#if (cp.delayPublishByDays > 1)>s</#if> after capture
                                </#if>
                            </div>
                        <#else>
                            <div id="noCapturePreferencesYet">
                                <@message 'noCapturePreferencesYet' />
                            </div>
                        </#if>
                    <br/>
                <#elseif hasGlobalErrors()>
                    <p class="instructional-text"><@showGlobalErrors /></p>
                <#else>
                    <h4 id="globalErrors"><@message 'noSuchCourse' /></h3>
                    <p id="contactUs"><@message 'contactUs' /></p>
                </#if>
            </div>
            <#include "signUpFooter.ftl" />
        </div>
    </body>
</html>
