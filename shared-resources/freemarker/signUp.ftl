<#include "macros.ftl" >

<#--
Declare variables.
-->
<#if courseOffering??>
    <#assign publishDelay = courseOffering.capturePreferences.delayPublishByDays />
    <#assign describePublishDelay><#if (publishDelay == 0)><@message "zeroPublishDelay" /><#else>${publishDelay} day${(publishDelay == 1)?string("","s")} after capture</#if></#assign>
</#if>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="webCastSignUp">
    <head>
        <title><@message 'signUpPageTitle' /></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="shortcut icon" href="/img/favicon.ico" />

        <link rel="stylesheet" href="${cssAndJsURLPrefix}/css/sign-up.css"/>
        <link rel="stylesheet" href="${cssAndJsURLPrefix}/css/angular/ngDialog.css">
        <link rel="stylesheet" href="${cssAndJsURLPrefix}/css/angular/ngDialog-theme-default.css">
        <link rel="stylesheet" href="${cssAndJsURLPrefix}/css/angular/ngDialog-theme-plain.css">

        <script type="text/ng-template" id="audioOnlyDemo">
            <div id="audioOnlyDemoBox" style="width:auto;">
                <iframe width="420" height="315" src="//www.youtube.com/embed/mAVkqFVkCfw" frameborder="0" allowfullscreen></iframe>
            </div>
        </script>
        <script type="text/ng-template" id="screencastDemo">
            <div id="screencastDemo" style="width:auto;">
                <iframe width="420" height="315" src="http://www.youtube.com/embed/myQhvHYW_2s" frameborder="0" allowfullscreen></iframe>
            </div>
        </script>
        <script type="text/ng-template" id="videoOnlyDemo">
            <div id="videoOnlyDemo" style="width:auto;">
                <iframe width="420" height="315" src="//www.youtube.com/embed/exo4-HoGZ3Q" frameborder="0" allowfullscreen></iframe>
            </div>
        </script>
        <script type="text/ng-template" id="videoAndScreencastDemo">
            <div id="videoAndScreencastDemo" style="width:auto;">
                <iframe width="420" height="315" src="http://www.youtube.com/embed/hs7BSi_r3nU" frameborder="0" allowfullscreen></iframe>
            </div>
        </script>
        <script type="text/javascript" src="${cssAndJsURLPrefix}/js/angular/angular.min.js"></script>
        <script type="text/javascript" src="${cssAndJsURLPrefix}/js/angular/ngDialog.min.js"></script>
        <script type="text/javascript" src="${cssAndJsURLPrefix}/js/sign-up.js"></script>
    </head>
    <body ng-controller="YouTubeDemoCtrl">
        <div id="pageBody" class="cct-container-main">

            <#include "signUpHeader.ftl" />

            <div class="moreOrLessContainer helpText" ng-class="{show: show}">
                <span class="helpText"><@message "webcastExplanation" /></span>
                <span class="helpText" ng-hide="!show"><a href="#" ng-click="show = false">less</a></span>
            </div>
            <span ng-hide="show">... <a href="#" ng-click="show = true">more</a></span>

            <form action="/signUp.html?id=${signUp.courseOfferingId}" method="POST">
                <div class="major-section">
                    <#if courseOffering.scheduled>
                        <#-- Some other instructor or admin has already set preferences. -->

                        <#assign cp = courseOffering.capturePreferences />
                        <h3 id="settingsReview"><@message "currentSettingsH3" /></h3>
                        <div id="instructionalText" class="instructional-text">
                            <@message "recordingsScheduledPleaseAgreeToTerms" />
                        </div>
                        <div id="selectedPreferences" class="major-section">
                            <div id="recordingTypeDiv">
                                <b>Recording Type:</b> <@message "${cp.recordingType}" />
                            </div>

                            <div id="recordingTimeDiv">
                                <b>Recording Time:</b>
                                <#list courseOffering.meetingDays as meetingDay>
                                    ${meetingDay}<#if meetingDay_has_next>,</#if>
                                </#list>
                                <@showTimeOfDay offsetStartTime />-<@showTimeOfDay offsetEndTime />
                            </div>

                            <div id="availabilityDiv">
                                <b>Availability:</b> <#if cp.recordingAvailability??><@message "${cp.recordingAvailability}" /><#else>--</#if>
                            </div>
                            <div id="publishDelayDiv">
                                <b>Publish Delay:</b>
                                <#if cp.delayPublishByDays == 0>
                                    As soon after capture as possible
                                <#else>
                                    ${cp.delayPublishByDays} day<#if (cp.delayPublishByDays > 1)>s</#if> after capture
                                </#if>
                            </div>
                        </div>
                    <#else>
                        <#if courseOffering.capturePreferences?? && courseOffering.capturePreferences.recordingType??>
                            <p id="instructionalText" class="instructional-text">
                                <@message "pleaseReviewOtherInstructorPreferences" />
                            </p>
                        </#if>

                        <h3 ><@message "chooseRecordingType" /></h3>

                        <@showFieldErrors "recordingType" />

                        <#assign videoCapableRoom = courseOffering.room.capability?lower_case?contains("video") />
                        <#list recordingTypeList as recordingType>
                            <#assign typeWithVideo = recordingType?lower_case?contains("video") />
                            <#assign offerRecordingType = videoCapableRoom || !typeWithVideo />
                            <#if offerRecordingType>
                                <div id="recordingTypeOptions">
                                    <#assign checked = signUp.recordingType?? && (signUp.recordingType == recordingType) />
                                    <input id="${recordingType}Option" type="radio" name="recordingType" value="${recordingType}" <#if checked>checked="checked"</#if> />
                                    <label for="${recordingType}Option"><@message "${recordingType}" /> (<@message "${recordingType}Cost"/>)</label>
                                        - <a href="#" ng-click="${recordingType}Demo()" class="demo">Play demo</a>
                                        <#-- <a href="#${recordingType}DemoBox" class="fancybox">play demo</a> -->
                                </div>
                            </#if>
                        </#list>

<input type="hidden" name="recordingAvailability" value="studentsOnly" >

<#--
NOTE: Per WCT-5386, we no longer give users the availability choice. It's studentsOnly for all, as shown using the hidden form element above.

    <h3><@message "chooseRecordingAvailability" /></h3>
    <@showFieldErrors "recordingAvailability" />

    <#list recordingAvailabilityList as recordingAvailability>
        <#assign checked = signUp.recordingAvailability?? && (signUp.recordingAvailability == recordingAvailability) />
        <#assign radioId = "${recordingAvailability}Option" />
        <input type="radio" name="recordingAvailability" class="radioItem" value="${recordingAvailability}" id="${radioId}" <#if checked>checked="checked"</#if> />
        <div id="${recordingAvailability}Label" class="radioLabel">
            <label for="${radioId}"><@message "${recordingAvailability}Details" /></label>
        </div>
        <div style="clear:both; margin-bottom: 2px"></div>
    </#list>
-->

                        <h3><@message "choosePublishDelay" /></h3>

                        <@showFieldErrors "publishDelay" />
                        <@showFieldErrors "publishDelayDays" />

                        <div id="noPublishDelayDiv" class="radioLabel">
                            <#assign checked = signUp.publishDelay?? && ("no" == signUp.publishDelay) />
                            <input type="radio" class="radioItem" id="noPublishDelay" name="publishDelay" value="no" ng-model="selected" <#if checked>ng-checked="true"</#if> />
                            <label for="publishDelayZero"><@message "zeroPublishDelay" /></label>
                        </div>
                        <div style="clear:both; margin-bottom: 2px"></div>
                        <div id="publishDelayDiv" class="radioItem">
                            <#assign checked = signUp.publishDelay?? && ("yes" == signUp.publishDelay) />
                            <input type="radio" id="publishDelay" name="publishDelay" value="yes" ng-model="selected" <#if checked>ng-checked="true"</#if> />
                            &nbsp;
                            <label for="publishDelayMany">
                                <select id="publishDelayDays" name="publishDelayDays" <#if !checked>ng-disabled="(selected === 'no') || !selected" </#if> >
                                    <option value="">Select...</option>
                                    <#list publishByDayOptions as publishByDayOption>
                                        <#assign selected = checked && signUp.publishDelayDays?? && (publishByDayOption == signUp.publishDelayDays) />
                                        <option value="${publishByDayOption}" <#if selected>selected="selected"</#if> >${publishByDayOption}</option>
                                    </#list>
                                </select> days after capture
                            </label>
                        </div>
                    </#if>
                    <br/>
                </div>
                <div class="major-section">

                    <#assign mustAgreeToTerms = !isAdminUser() />
                    <#if mustAgreeToTerms>
                        <@showFieldErrors "agreeToTerms" />
                        <input type="checkbox" name="agreeToTerms" value="true" id="agreeToTerms" ng-model="checked" />&nbsp;<label for="agree"><@message "agreeToTheTerms" /></label><br/>
                    <#else>
                        <br/>
                    </#if>
                    <input type="hidden" id="courseOfferingId" name="courseOfferingId" value="${signUp.courseOfferingId}" />
                    <input type="button" id="signUpButton" value="Sign Up" ng-disabled="<#if mustAgreeToTerms>! </#if>checked" onclick="this.disabled=true;this.value='Please wait...';submit()"/>

                    <span id="globalErrors" class="warning">
                        <@showGlobalErrors />&nbsp;
                        <#if isAdminUser() && !courseOffering.scheduled>
                            <@message "formSubmitWillScheduleRecordings" />
                        </#if>
                    </span>

                </div>
            </form>

            <#include "signUpFooter.ftl" />

        </div>
    </body>
</html>
