<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>

  <entry key="signUpPageTitle">Course Capture (Webcast) | Educational Technology Services, University of California Berkeley</entry>
  <entry key="defaultPageH1">Course Capture (Webcast)</entry>
  <entry key="signUpH1">Sign Up for Course Capture (Webcast)</entry>
  <entry key="currentSettingsH3">Current Settings</entry>
  <entry key="reviewOtherOptionsH3">Review Other Options</entry>
  <entry key="contactUs"><![CDATA[Please contact <a href="mailto:etssupport@berkeley.edu">etssupport@berkeley.edu</a> if you have questions.]]></entry>
  <entry key="signUpFinishFooter"><![CDATA[Questions? Comments? Please contact
    <a href="mailto:etssupport@berkeley.edu">etssupport@berkeley.edu</a>.<br/>You will receive an email containing the
    information above plus other details.]]></entry>
  <entry key="missingCourseId">Your request does not contain a course identifier. Please check the URL provided to you.</entry>
  <entry key="noSuchCourse">Our system has no record of the requested course information.</entry>
  <entry key="noCapturePreferencesYet">Instructor preferences have not yet been received.</entry>
  <entry key="chooseRecordingType">What do you want recorded?</entry>
  <entry key="chooseRecordingAvailability">How should recordings be made available?</entry>
  <entry key="choosePublishDelay">When should recordings be published?</entry>
  <entry key="pleaseLogIn">You must log in to access this page.</entry>
  <entry key="youAreNotCourseInstructor">Sorry, you are not authorized to access this sign up form because you are not listed as
    an instructor of the course.</entry>
  <entry key="youHaveAlreadySignedUp"><![CDATA[You have already signed up with the following settings. Please note that
    there will be a delay of approximately one hour before other systems such as bCourses and CalCentral can be updated
    to reflect that your Course Capture recordings have been scheduled.]]></entry>
  <entry key="otherInstructorSignedUp">Your co-instructor signed up with the following settings. Change these only if
    you believe your co-instructor(s) will agree.</entry>
  <entry key="youPreviouslyTheFollowing">You previously selected the following settings.</entry>
  <entry key="webcastExplanation"><![CDATA[The Course Capture (Webcast) program is the campus service, run by ETS,
    for recording and publishing classroom activity. If you sign up using this form, recordings of every class session
    will be automatically recorded and made available to students in this course via CalCentral and bCourses.
    (For details, please read <a href="http://ets.berkeley.edu/help/webcast-classroom-capture-services-explained"
    target="blank">Course Capture (Webcast) Services Explained</a>.)]]></entry>
  <entry key="publicCreativeCommons"><![CDATA[<strong>Public</strong> with a license of Creative Commons 3.0: Attribution-NonCommercials-NoDerivs]]></entry>
  <entry key="publicCreativeCommonsDetails"><![CDATA[<strong>On public distribution channels</strong>
    (<a href="http://webcast.berkeley.edu/" target="_blank">webcast.berkeley</a> website;
    <a href="http://itunes.berkeley.edu/" target="_blank">UC Berkeley on iTunesU;</a> and
    <a class="ext" href="http://www.youtube.com/user/UCBerkeley/" target="_blank">UC Berkeley on YouTube</a>) as well as on sites restricted to only students in this course (e.g. CalCentral and bCourses), with a <strong>Creative Commons CC BY-NC-ND 3.0</strong> license
    (allowing anybody to redistribute non-commercially, unchanged, and attributed to you)
    <a href=" https://ets.berkeley.edu/help/overview-licensing-and-copyright-issues-webcast-classroom-capture" target="_blank">Learn
    more about this license</a>]]></entry>
  <entry key="publicNoRedistributeDetails"><![CDATA[<strong>On public distribution channels</strong>
    (<a href="http://webcast.berkeley.edu/" target="_blank">webcast.berkeley</a> website;
    <a href="http://itunes.berkeley.edu/" target="_blank">UC Berkeley on iTunesU;</a> and
    <a class="ext" href="http://www.youtube.com/user/UCBerkeley/" target="_blank">UC Berkeley on YouTube</a>) as well as on sites restricted to only students in this course (e.g. CalCentral and bCourses), with <strong>no license to redistribute</strong>]]></entry>
  <entry key="studentsOnlyDetails"><![CDATA[<strong>Students in your course only</strong>, on sites restricted to only students in this course (e.g. CalCentral and bCourses), with <strong>no license to redistribute</strong>]]></entry>
  <entry key="publicNoRedistribute"><![CDATA[Public with no license to redistribute]]></entry>
  <entry key="studentsOnly">Students in your course only with no license to redistribute.</entry>
  <entry key="audioOnly">Audio only</entry>
  <entry key="audioOnlyCost">no cost</entry>
  <entry key="audioOnlyButtonIcon"><![CDATA[<br/><br/><img src="/img/sign_up/audio.png"><br/>]]></entry>
  <entry key="screencast">Computer Screen with Audio</entry>
  <entry key="screencastCost">no cost</entry>
  <entry key="screencastButtonIcon"><![CDATA[<br><img src="/img/sign_up/screen.png"><img src="/img/sign_up/audio.png"><br/>]]></entry>
  <entry key="videoOnly">Video with Audio</entry>
  <entry key="videoOnlyCost">$2000 cost</entry>
  <entry key="videoOnlyButtonIcon"><![CDATA[<br><br><img src="/img/sign_up/video.png"><img src="/img/sign_up/audio.png"><br/>]]></entry>
  <entry key="videoAndScreencast">Video, Computer Screen, and Audio</entry>
  <entry key="videoAndScreencastCost">$2000 cost</entry>
  <entry key="videoAndScreencastButtonIcon"><![CDATA[<br><img src="/img/sign_up/screen.png"><img src="/img/sign_up/video.png"><img src="/img/sign_up/audio.png"><br/>]]></entry>
  <entry key="zeroPublishDelay">As soon after capture as possible</entry>
  <entry key="agreeToTheTerms"><![CDATA[I have read the
    <a target="_agreement" href="https://ets.berkeley.edu/sites/default/files/ucb_media-release_courses.pdf">Audio and
    Video Recording Permission Agreement</a> and I agree to the terms stated within.]]></entry>
  <entry key="formSubmitWillScheduleRecordings">Submitting will schedule recordings!</entry>
  <entry key="recordingsHaveBeenScheduledHeader">Recordings have been scheduled</entry>
  <entry key="recordingsHaveBeenScheduled"><![CDATA[The following settings have been selected for capture, and <b>recordings
    have been scheduled</b>. Please note that there will be a delay of approximately one hour before other systems such
    as bCourses and CalCentral can be updated to reflect that your Course Capture recordings have been scheduled.]]></entry>
  <entry key="recordingsWillBeScheduledWhen">You have submitted the preferences below. Recordings will be scheduled when all instructors in
    your course have signed up.</entry>
  <entry key="conflictsWithPreviouslyScheduledRecordings">Sorry, your course conflicts with previously scheduled recordings.</entry>
  <entry key="courseMovedToIneligibleRoom"><![CDATA[This course is currently located in a room that has no capture capabilities so is
    no longer eligible for Course Capture (Webcast). Please contact <a href="mailto:etssupport@berkeley.edu">etssupport@berkeley.edu</a> with any questions.]]></entry>
  <entry key="error.signUp.seeErrors">You need to make choices above before signing up</entry>
  <entry key="error.signUp.courseOfferingIdMissing"><![CDATA[The system does not recognize the submitted course
    identifier. Please contact <a href="mailto:etssupport@berkeley.edu">etssupport@berkeley.edu</a> to report the problem.]]></entry>
  <entry key="error.signUp.recordingTypeUnspecified">You must choose one of the following:</entry>
  <entry key="error.signUp.logInRequired">Your login session has expired.</entry>
  <entry key="error.signUp.agreeToTerms">You must agree to terms by checking the box below:</entry>
  <entry key="error.signUp.publishDelayUnspecified">You must choose one of the following:</entry>
  <entry key="error.signUp.publishDelayDaysUnspecified">Please specify the number of days.</entry>
  <entry key="error.signUp.recordingAvailabilityUnspecified">You must choose one of the following:</entry>
  <entry key="error.signUp.recordingTypeUnspecified">You must choose one of the following:</entry>
  <entry key="error.signUp.userAlreadyApproved"><![CDATA[You have already submitted your preferences for this course. Please contact
    <a href="mailto:etssupport@berkeley.edu">etssupport@berkeley.edu</a> if you would like to request changes.]]></entry>
  <entry key="recordingsScheduledPleaseAgreeToTerms"><![CDATA[Recordings have been scheduled with the following preferences
    but we need you to agree to the terms described below. Please note that there will be a delay of approximately one
    hour before other systems such as bCourses and CalCentral can be updated to reflect that your Course Capture recordings have been scheduled.]]></entry>
  <entry key="pleaseReviewOtherInstructorPreferences"><![CDATA[Your co-instructor(s) signed up with the following settings. Change these only if you believe your co-instructor(s) will agree.
]]></entry>
  <entry key="error.signUp.scheduledRecordingsWithMissingPreferences"><![CDATA[Ths course has bad data in our system.
    Please contact <a href="mailto:etssupport@berkeley.edu">etssupport@berkeley.edu</a> and report the problem.]]></entry>
  <entry key="unexpectedSystemError"><![CDATA[An unexpected error occurred. Please contact
    <a href="mailto:etssupport@berkeley.edu">etssupport@berkeley.edu</a> and report the problem.]]></entry>

</properties>
