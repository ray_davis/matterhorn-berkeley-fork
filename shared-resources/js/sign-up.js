
var app = angular.module('webCastSignUp', ['ngDialog']);

app.controller('YouTubeDemoCtrl', function ($scope, ngDialog) {
    $scope.audioOnlyDemo = function () {
        ngDialog.open({
            template: 'audioOnlyDemo',
            className: 'ngdialog-theme-plain'
        });
    };
    $scope.screencastDemo = function () {
        ngDialog.open({
            template: 'screencastDemo',
            className: 'ngdialog-theme-plain'
        });
    };
    $scope.videoOnlyDemo = function () {
        ngDialog.open({
            template: 'videoOnlyDemo',
            className: 'ngdialog-theme-plain'
        });
    };
    $scope.videoAndScreencastDemo = function () {
        ngDialog.open({
            template: 'videoAndScreencastDemo',
            className: 'ngdialog-theme-plain'
        });
    };
});
