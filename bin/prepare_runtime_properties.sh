#!/bin/bash

cd "${FELIX_HOME}"

for jarFile in $(ls ${FELIX_HOME}/lib/ext/*.jar); do cp="${cp}:${jarFile}"; done

for jarFile in $(ls ${FELIX_HOME}/lib/matterhorn/*.jar); do cp="${cp}:${jarFile}"; done

export CLASSPATH="${cp}"

java org.opencastproject.util.env.PrepareMatterhornRuntimeProperties "${FELIX_HOME}"

echo "Property files have been 'compiled' in order to select environment-specifc settings."

exit 0
