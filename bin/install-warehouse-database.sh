#!/bin/bash
# ==================================================================================== #
#  DESCRIPTION: Initializes webcast_warehouse postgres database
#
# AUTHOR:    John Crossman
#
# REQUIREMENTS: postgres
#         ${MATTERHORN_HOME}/webcast_warehouse
#
# LICENSE:    Copyright 2009, 2010 The Regents of the University of California
#         Licensed under the Educational Community License, Version 2.0
#         (the "License"); you may not use this file except in compliance
#         with the License. You may obtain a copy of the License at
#
#         http://www.osedu.org/licenses/ECL-2.0
#
#         Unless required by applicable law or agreed to in writing,
#         software distributed under the License is distributed on an "AS IS"
#         BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#         or implied. See the License for the specific language governing
#         permissions and limitations under the License.
# ==================================================================================== #

# ------------------------------------------------------------------------------------ #
# Global variables with default
# ------------------------------------------------------------------------------------ #
DATABASE='webcast_warehouse'
USER='webcast_wh_app'
FILE="${MATTERHORN_HOME}/etc/sql/webcast_warehouse/schemaLoad.sql"

#===================================================================================== #
#  DESCRIPTION: Display usage information for this script
#  PARAMETERs : None
#===================================================================================== #
display_usage () {
  cat <<EOT
Usage   : $0  [-h] [-u user] [-f file]
      Initializes the ${DATABASE} database tables and user
Options :
      -u | --user=ID     : creates database user ID and grants it ALL permission
      -f | --file=SCRIPT : executes SCRIPT
      -h | --help        : print command line usage information and exit
      If a long option shows an argument as mandatory, then it is mandatory
      for the equivalent short option also.  Similarly for optional arguments.

Example : '${0}' without options will use defaults.

EOT
  return 0
}

#===================================================================================== #
#  DESCRIPTION: Outputs a message to stderr
#  PARAMETER 1: Message to write
#  PARAMETER 2: Line number of statement
#===================================================================================== #
logger () {
  local now=$(date "+%F %T")
  local logLevel="INFO"
  local message=$1
  local statement=$2
  # echo "$now  $logLevel ($0:$statement) - $message" >&2
  echo "[$logLevel : $statement] - $message" >&2
  return 0
}

# ------------------------------------------------------------------------------------ #
# Checks for options, failing if getopts returns <> 0
# ------------------------------------------------------------------------------------ #
parsedOptions=`getopt -o u:f:h --long user=:,file=:,help --name "$0" -- "$@"`
getoptReturnCode=$?
if [ $getoptReturnCode -ne 0 ]; then
  logger "Terminating due to getopt $getoptReturnCode" ${LINENO}
  exit $getoptReturnCode # Some getopt problem
fi

eval set -- "$parsedOptions"

while true; do
  case "$1" in
    -u | --user= )
      if [ -n "$2" ]; then
        USER="$2"
        else
          logger "user ID required for user option; use -h option for usage" ${LINENO}
        exit 1
        fi
        shift 2
      ;;
    -f | --file= )
      if [ -n "$2" ]; then
        FILE="$2"
        else
          logger "File name required for file option; use -h option for usage" ${LINENO}
        exit 1
        fi
        shift 2
      ;;
      -h | --help )
      display_usage
      exit 0
      ;;
    -- )
      shift
      break
      ;;
    * )
      logger "Terminating due to unsupported option $1; use -h option for usage" ${LINENO}
      exit 1
      ;;
  esac
done

# ------------------------------------------------------------------------------------ #
# Drop database and user
# ------------------------------------------------------------------------------------ #
logger "Dropping database ${DATABASE}" ${LINENO}
dropdb --if-exists ${DATABASE}

logger "Dropping user ${USER}" ${LINENO}
dropuser --if-exists ${USER}
psql --dbname "${DATABASE}" --command "DROP ROLE IF EXISTS ${USER}"

# ------------------------------------------------------------------------------------ #
# Create database and user
# ------------------------------------------------------------------------------------ #
logger "Creating database ${DATABASE}" ${LINENO}
createdb ${DATABASE}

logger "Creating user ${USER}" ${LINENO}
createuser ${USER} -D -R
psql --dbname "${DATABASE}" --command "CREATE USER ${USER}"

# ------------------------------------------------------------------------------------ #
# Execute DDL
# ------------------------------------------------------------------------------------ #
logger "Executing DDL from ${FILE}" ${LINENO}
psql --dbname "${DATABASE}" --echo-all --file "${FILE}"

# ------------------------------------------------------------------------------------ #
# Grant permissions
# ------------------------------------------------------------------------------------ #
logger "Granting ALL on database ${DATABASE} to user ${USER}" ${LINENO}
psql --dbname "${DATABASE}" --command "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO ${USER}"
psql --dbname "${DATABASE}" --command "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO ${USER}"

echo; echo "Done"; echo

exit $?
