#!/bin/bash

echo; echo
echo "----------------------------------------------------------------------------------------------------"
echo " Steps to running CourseDataMover:"
echo "   1. Compile the code"
echo "   2. Run CourseDataMover"
echo "----------------------------------------------------------------------------------------------------"
echo; echo

sleep 1 && clear

echo; echo
echo "----------------------------------------------------------------------------------------------------"
echo; echo "STEP 1: Compile the code"; echo

cd "${MATTERHORN_HOME}"

mvn install -DskipTests

echo; echo
echo "----------------------------------------------------------------------------------------------------"
sleep 1 && clear

echo "----------------------------------------------------------------------------------------------------"
echo; echo "STEP 2: Run CourseDataMover"; echo

cd modules/matterhorn-participation-service-impl

export COURSE_DATA_MOVER_FAKE_MODE=${COURSE_DATA_MOVER_FAKE_MODE:-false}
mvn -Dtest=org.opencastproject.participation.impl.CourseDataMoverRunTest test 

echo "----------------------------------------------------------------------------------------------------"
echo; echo "Thank you. We are done."
echo

exit 0
