#!/bin/bash
# ==================================================================================== #
#  DESCRIPTION: Initializes Matterhorn postgres database
#
# AUTHOR:        John Crossman, Fernando Alvarez
#
# REQUIREMENTS: postgres
#               $MATTERHORN_HOME/docs/scripts/ddl/postgres84.sql
#
# LICENSE:      Copyright 2009, 2010 The Regents of the University of California
#               Licensed under the Educational Community License, Version 2.0
#               (the "License"); you may not use this file except in compliance
#               with the License. You may obtain a copy of the License at
#
#               http://www.osedu.org/licenses/ECL-2.0
#
#               Unless required by applicable law or agreed to in writing,
#               software distributed under the License is distributed on an "AS IS"
#               BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#               or implied. See the License for the specific language governing
#               permissions and limitations under the License.
# ==================================================================================== #

# ------------------------------------------------------------------------------------ #
# Global variables with default
# ------------------------------------------------------------------------------------ #
DATABASE='matterhorn'
USER='matterhorn'
FILE="${MATTERHORN_HOME}/docs/scripts/ddl/postgres84.sql"

#===================================================================================== #
#  DESCRIPTION: Display usage information for this script
#  PARAMETERs : None
#===================================================================================== #
display_usage () {
    cat <<EOT
Usage   : install-matterhorn-database.sh [-h] [-u user] [-f file]
          Initializes the Matterhorn database tables and user
Options :
          -u | --user=ID       : creates database user ID and grants it ALL permission
          -f | --file=SCRIPT   : executes SCRIPT
          -h | --help          : print command line usage information and exit
          If a long option shows an argument as mandatory, then it is mandatory
          for the equivalent short option also.  Similarly for optional arguments.

Example : 'install-matterhorn-database.sh' without options will use defaults consistent
          with the standard matterhorn configuration. Providing a value for -u requires
          corresponding changes to $MATTERHORN_HOME/etc/config.properties or .mhruntime.cf.
EOT
    return 0
}

#===================================================================================== #
#  DESCRIPTION: Outputs a message to stderr
#  PARAMETER 1: Message to write
#  PARAMETER 2: Line number of statement
#===================================================================================== #
logger () {
    local now=$(date "+%F %T")
    local logLevel="INFO"
    local message=$1
    local statement=$2
    echo "$now  $logLevel ($0:$statement) - $message" >&2
    return 0
}

# ------------------------------------------------------------------------------------ #
# Checks for options, failing if getopts returns <> 0
# ------------------------------------------------------------------------------------ #
parsedOptions=`getopt -o u:f:h --long user=:,file=:,help --name "$0" -- "$@"`
getoptReturnCode=$?
if [ $getoptReturnCode -ne 0 ]; then
    logger "Terminating due to getopt $getoptReturnCode" $LINENO
    exit $getoptReturnCode # Some getopt problem
fi

eval set -- "$parsedOptions"

while true; do
  case "$1" in
    -u | --user= )
      if [ -n "$2" ]; then
        USER="$2"
      else
        logger "user ID required for user option; use -h option for usage" $LINENO
        exit 1
      fi
      shift 2
      ;;
    -f | --file= )
      if [ -n "$2" ]; then
        FILE="$2"
      else
        logger "File name required for file option; use -h option for usage" $LINENO
        exit 1
      fi
      shift 2
      ;;
    -h | --help )
      display_usage
      exit 0
      ;;
    -- )
      shift
      break
      ;;
    * )
      logger "Terminating due to unsupported option $1; use -h option for usage" $LINENO
      exit 1
      ;;
  esac
done

# ------------------------------------------------------------------------------------ #
# Drop database and user
# ------------------------------------------------------------------------------------ #
logger "Dropping database $DATABASE" $LINENO
dropdb --if-exists $DATABASE

logger "Dropping user $USER" $LINENO
dropuser --if-exists $USER

# ------------------------------------------------------------------------------------ #
# Create database and user
# ------------------------------------------------------------------------------------ #
logger "Creating database $DATABASE" $LINENO
createdb $DATABASE

logger "Creating user $USER" $LINENO
#createuser $USER -D -R -q
psql --dbname matterhorn --command "CREATE USER $USER"

# ------------------------------------------------------------------------------------ #
# Execute DDL
# ------------------------------------------------------------------------------------ #
logger "Executing DDL from $FILE" $LINENO
psql --dbname matterhorn --echo-all --file "${FILE}"

# ------------------------------------------------------------------------------------ #
# Grant permissions
# ------------------------------------------------------------------------------------ #
logger "Granting ALL on database $DATABASE to user $USER" $LINENO
psql --dbname matterhorn --command "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO matterhorn"

exit $?
