#!/bin/bash

##
# Compile Hadoop fix and copy jar to 'operations' git module
##

# Declarations
baseDirectory="${FELIX_HOME}/patches/WCT-3011"
jarsDirectory="${baseDirectory}/jars"
originalJarFile="org.apache.felix.http.bundle-2.2.0.jar"
patchPrefix="org.apache.felix.http.bundle-2.2.0-WCT-3011"
patchedJarFile="${patchPrefix}.jar"
patchedJarFileSources="${patchPrefix}-sources.jar"
tmpDir="/var/tmp/${patchPrefix}"
hadoopJettyZip="jetty-hadoop-fix-src.zip"
hadoopJettyDir="${tmpDir}/jetty-hadoop-fix/modules/jetty"
unzippedOriginal="${tmpDir}/original"

echo; echo "Create a temporary directory for transient files"
rm -Rf ${tmpDir}
mkdir ${tmpDir}

echo; echo "Maven build of Hadoop Jetty module"
cp "${baseDirectory}/${hadoopJettyZip}" ${tmpDir}
cd "${tmpDir}"
unzip "${hadoopJettyZip}"
cd "${hadoopJettyDir}"
mvn clean install -DskipTests

if [[ $? != 0 ]] ; then
    echo; echo "Maven build failed at ${hadoopJettyDir}"
    exit 1
fi

echo; echo "Explode existing JAR file"
mkdir "${unzippedOriginal}"
cd "${unzippedOriginal}"
cp "${jarsDirectory}/${originalJarFile}" "${unzippedOriginal}" \
    || { echo "Failed to copy ${existingJarFile}" ; exit 1; }
jar xf "${originalJarFile}"
rm "${originalJarFile}"

echo; echo "Patch recently compiled classes"

cp ${hadoopJettyDir}/target/classes/org/mortbay/io/nio/SelectChannelEndPoint* \
    ${unzippedOriginal}/org/mortbay/io/nio/
cp ${hadoopJettyDir}/target/classes/org/mortbay/io/nio/SelectorManager* \
    ${unzippedOriginal}/org/mortbay/io/nio/

echo; echo "Copy new JAR to permanent location"
cd "${unzippedOriginal}"
jar cmf "META-INF/MANIFEST.MF" ${patchedJarFile} META-INF/ javax/ org/  \
    || { echo "Failed to create ${patchedJarFile}" ; exit 1; }
cp ${patchedJarFile} ${jarsDirectory}

echo; echo "Build 'sources' JAR file"
cd "${hadoopJettyDir}/src/main/java"
jar cMf ${patchedJarFileSources} org/ || { echo "Failed to jar the sources: ${patchedJarFileSources}" ; exit 1; }
cp ${patchedJarFileSources} ${jarsDirectory}

# Clean
rm -Rf ${tmpDir}

echo; echo "IMPORTANT: Your next step is to Git commit the modified files in ${jarsDirectory}"

echo; echo "Finished."

exit 0
