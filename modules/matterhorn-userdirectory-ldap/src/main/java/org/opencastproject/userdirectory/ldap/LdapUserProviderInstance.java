/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.userdirectory.ldap;

import org.apache.commons.lang.math.NumberUtils;
import org.opencastproject.security.api.CachingUserProviderMXBean;
import org.opencastproject.security.api.User;
import org.opencastproject.security.api.UserProvider;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.util.data.Collections;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;
import org.springframework.security.ldap.userdetails.LdapUserDetailsService;

import java.lang.management.ManagementFactory;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

/**
 * A UserProvider that reads user roles from LDAP entries.
 */
public class LdapUserProviderInstance implements UserProvider, CachingUserProviderMXBean {

  /** The logger */
  private static final Logger logger = LoggerFactory.getLogger(LdapUserProviderInstance.class);

  private final String pid;
  private final String searchBase;
  private final String searchFilter;
  private final String roleAttributesGlob;
  private final String userDn;
  private final String password;
  private final String url;
  private final int cacheExpiration;
  private final List<String> ignoreUserNameList;

  /** The spring ldap service */
  private LdapUserDetailsService delegate = null;

  /** The organization id */
  private String organization = null;

  /** Total number of requests made to load users */
  private AtomicLong requests = null;

  /** The number of requests made to ldap */
  private AtomicLong ldapLoads = null;

  /** A cache of users, which lightens the load on the LDAP server */
  private final Cache<String, User> cache;

  /**
   * Constructs an ldap user provider with the needed settings.
   *
   * @param pid
   *          the pid of this service
   * @param organization
   *          the organization
   * @param searchBase
   *          the ldap search base
   * @param searchFilter
   *          the ldap search filter
   * @param url
   *          the url of the ldap server
   * @param userDn
   *          the user to authenticate as
   * @param password
   *          the user credentials
   * @param roleAttributesGlob
   *          the comma separate list of ldap attributes to treat as roles
   * @param cacheSize
   *          the number of users to cache
   * @param cacheExpiration
   *          the number of minutes to cache users
   */
  // CHECKSTYLE:OFF
  LdapUserProviderInstance(String pid, String organization, String searchBase, String searchFilter, String url,
          String userDn, String password, String roleAttributesGlob, int cacheSize, int cacheExpiration, List<String> ignoreUserNameList)
          throws ConfigurationException {
    // CHECKSTYLE:ON
    this.pid = pid;
    this.searchBase = searchBase;
    this.searchFilter = searchFilter;
    this.url = url;
    this.roleAttributesGlob = roleAttributesGlob;
    this.userDn = userDn;
    this.password = password;
    this.organization = organization;
    this.cacheExpiration = cacheExpiration;
    this.ignoreUserNameList = ignoreUserNameList;
    resetLDAPService();
    registerMBean(pid);
    logger.warn("Set up the LDAP user cache with maximumSize=" + cacheSize + " and cacheExpiration=" + cacheExpiration);
    cache = CacheBuilder.newBuilder().maximumSize(cacheSize).expireAfterWrite(cacheExpiration, TimeUnit.MINUTES).build();
  }

  /**
   * Registers an MXBean.
   */
  protected void registerMBean(final String pid) {
    requests = new AtomicLong();
    ldapLoads = new AtomicLong();
    try {
      final ObjectName name = LdapUserProviderFactory.getObjectName(pid);
      final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
      try {
        mbs.unregisterMBean(name);
      } catch (InstanceNotFoundException e) {
        logger.debug(name + " was not registered");
      }
      mbs.registerMBean(this, name);
    } catch (Exception e) {
      logger.warn("Unable to register {} as an MBean: {}", this, e);
    }
  }

  /**
   * {@inheritDoc}
   *
   * @see org.opencastproject.security.api.UserProvider#getOrganization()
   */
  @Override
  public String getOrganization() {
    return organization;
  }

  /**
   * {@inheritDoc}
   *
   * @see org.opencastproject.security.api.UserProvider#loadUser(java.lang.String)
   */
  @Override
  public User loadUser(final String userNameUntrimmed) {
    final String userName = StringUtils.trimToNull(userNameUntrimmed);
    if (userName == null || (ignoreUserNameList != null && ignoreUserNameList.contains(userName))) {
      return null;
    }
    logger.debug("LdapUserProvider is loading user " + userName);
    requests.incrementAndGet();
    final User user;
    final User userFromCache = cache.getIfPresent(userName);
    if (userFromCache == null) {
      final User userFromLDAP = loadUserFromLdap(userName);
      if (userFromLDAP == null) {
        user = null;
      } else {
        user = userFromLDAP;
        cache.put(userName, user);
      }
    } else {
      user = userFromCache;
    }
    return user;
  }

  /**
   * Loads a user from LDAP.
   *
   * @param userName
   *          the username
   * @return the user
   */
  protected User loadUserFromLdap(String userName) {
    if (delegate == null || cache == null) {
      throw new IllegalStateException("The LDAP user detail service has not yet been configured");
    }
    ldapLoads.incrementAndGet();
    UserDetails userDetails;

    Thread currentThread = Thread.currentThread();
    final ClassLoader originalClassLoader = currentThread.getContextClassLoader();
    try {
      currentThread.setContextClassLoader(LdapUserProviderFactory.class.getClassLoader());
      try {
        userDetails = loadUserByUsername(userName);
      } catch (final UsernameNotFoundException e) {
        cache.invalidate(userName);
        return null;
      }
      final Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
      String[] roles = null;
      if (authorities != null) {
        int i = 0;
        roles = new String[authorities.size()];
        for (GrantedAuthority authority : authorities) {
          String role = authority.getAuthority();
          roles[i++] = role;
        }
      }
      final User user =  new User(userDetails.getUsername(), getOrganization(), roles);
      cache.put(userName, user);
      return user;
    } finally {
      currentThread.setContextClassLoader(originalClassLoader);
    }
  }

  private UserDetails loadUserByUsername(final String userNameUntrimmed) {
    final String userName = StringUtils.stripToEmpty(userNameUntrimmed);
    logger.warn("loadUserByUsername: " + userName);
    return NumberUtils.isDigits(userName)
            ? new PersonDetails(userName)
            : delegate.loadUserByUsername(userName);
  }

  /**
   * {@inheritDoc}
   *
   * @see org.opencastproject.security.api.CachingUserProviderMXBean#getCacheHitRatio()
   */
  public float getCacheHitRatio() {
    if (requests.get() == 0) {
      return 0;
    }
    return (float) (requests.get() - ldapLoads.get()) / requests.get();
  }

  private void resetLDAPService() {
    logger.warn("Reset LDAP service with pid=" + pid
            + ", organization=" + organization
            + ", searchBase=" + searchBase
            + ", searchFilter=" + searchFilter
            + ", url=" + url
            + ", roleAttributesGlob=" + roleAttributesGlob
            + ", userDn=" + userDn
            + ", password=" + password
            + ", organization=" + organization
            + ", cacheExpiration=" + cacheExpiration
            + ", ignoreUserNameList=" + ignoreUserNameList);
    final DefaultSpringSecurityContextSource contextSource = new DefaultSpringSecurityContextSource(url);
    // Fix for WCT-5461. See http://docs.oracle.com/javase/7/docs/technotes/guides/jndi/jndi-ldap.html
    final Map<String, String> env = new HashMap<String, String>();
    final String twoMinutes = "120000";
    env.put("com.sun.jndi.ldap.connect.timeout", twoMinutes);
    env.put("com.sun.jndi.ldap.read.timeout", twoMinutes);
    contextSource.setBaseEnvironmentProperties(env);
    if (StringUtils.isNotBlank(userDn)) {
      contextSource.setPassword(password);
      contextSource.setUserDn(userDn);
      // Required so that authentication will actually be used
      contextSource.setAnonymousReadOnly(false);
    } else {
      logger.warn("No LDAP service credentials in config file. We will use anonymous read-only.");
      contextSource.setAnonymousReadOnly(true);
    }
    try {
      contextSource.afterPropertiesSet();
    } catch (Exception e) {
      throw new org.opencastproject.util.ConfigurationException("Unable to create a spring context source", e);
    }
    final FilterBasedLdapUserSearch userSearch = new FilterBasedLdapUserSearch(searchBase, searchFilter, contextSource);
    userSearch.setReturningAttributes(roleAttributesGlob.split(","));
    delegate = new LdapUserDetailsService(userSearch);

    if (StringUtils.isNotBlank(roleAttributesGlob)) {
      LdapUserDetailsMapper mapper = new LdapUserDetailsMapper();
      mapper.setRoleAttributes(roleAttributesGlob.split(","));
      delegate.setUserDetailsMapper(mapper);
    }
  }

  private class PersonDetails implements UserDetails {

    private final String userName;

    public PersonDetails(final String userName) {
      this.userName = userName;
    }

    @Override
    public Collection<SimpleGrantedAuthority> getAuthorities() {
      return Collections.list(new SimpleGrantedAuthority("ROLE_EMPLOYEE-TYPE-ACADEMIC"),
              new SimpleGrantedAuthority("ROLE_EMPLOYEE-TYPE-STAFF"));
    }

    @Override
    public String getPassword() {
      throw new UnsupportedOperationException("When we query Oracle views we can not pull LDAP password.");
    }

    @Override
    public String getUsername() {
      return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
      return true;
    }

    @Override
    public boolean isAccountNonLocked() {
      return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
      return false;
    }

    @Override
    public boolean isEnabled() {
      return true;
    }
  }
}
