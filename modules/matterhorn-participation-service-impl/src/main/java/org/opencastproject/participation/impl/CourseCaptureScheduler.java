/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import static org.opencastproject.util.data.Tuple.tuple;

import org.opencastproject.capture.CaptureParameters;
import org.opencastproject.capture.admin.api.Agent;
import org.opencastproject.capture.admin.api.CaptureAgentStateService;
import org.opencastproject.mediapackage.MediaPackageElementFlavor;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.License;
import org.opencastproject.participation.model.PublicationChannel;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.SignUp;
import org.opencastproject.rest.RestConstants;
import org.opencastproject.scheduler.api.SchedulerException;
import org.opencastproject.scheduler.api.SchedulerService;
import org.opencastproject.security.api.AccessControlList;
import org.opencastproject.security.api.RecordingAccessRights;
import org.opencastproject.security.api.UnauthorizedException;
import org.opencastproject.series.api.SeriesException;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.MapUtils;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Tuple;
import org.opencastproject.util.env.EnvironmentUtil;

import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Manages the scheduling of events for courses.
 *
 */
public class CourseCaptureScheduler {

  private final SchedulerService schedulerService;
  private final CaptureAgentStateService captureAgentStateService;
  private final SeriesService seriesService;

  public CourseCaptureScheduler(final SchedulerService schedulerService, final SeriesService seriesService,
                                final CaptureAgentStateService captureAgentStateService) {
    this.schedulerService = schedulerService;
    this.seriesService = seriesService;
    this.captureAgentStateService = captureAgentStateService;
  }

  /**
   * Schedules one or more events for a course.
   * 
   * @param seriesId  the extant series to associate with these events
   * @param courseData  data used to schedule the event
   * @param signUp  instructor options used to schedule the event
   * @param captureAgentName  name of the agent that will record the event
   * @return episode {@code DublinCoreCatalog} for each event scheduled
   * @throws SchedulerException if the request was invalid, or resulted in zero events scheduled
   */
  public Map<Long, Tuple<Long, DublinCoreCatalog>> scheduleMatterhornRecordings(final String seriesId, final CourseData courseData, final SignUp signUp,
                                                                                final String captureAgentName) throws SchedulerException {
    final TimeZone tz = TimeZone.getDefault();
    final DublinCoreCatalogImpl d = DublinCoreCatalogImpl.newInstance();
    d.set(DublinCore.PROPERTY_TITLE, courseData.getRecordingTitle());
    d.set(DublinCore.PROPERTY_IS_PART_OF, seriesId);
    d.set(DublinCore.PROPERTY_CREATOR, courseData.getInstructors());
    d.set(DublinCore.PROPERTY_SPATIAL, captureAgentName);
    final String license = signUp.getRecordingAvailability().getLicense().getSalesforceValue();
    d.set(DublinCore.PROPERTY_LICENSE, license);
    d.set(DublinCoreCatalogImpl.PROPERTY_AGENT_TIMEZONE, tz.getID());
    final int startTimeMilitaryPST = Integer.parseInt(courseData.getOffsetStartTime());
    final int endTimeMilitaryPST = Integer.parseInt(courseData.getOffsetEndTime());
    final Date semesterStartDate = CourseUtils.dateFromSalesforceFormat(courseData.getTerm().getStartDate());
    d.set(DublinCoreCatalogImpl.PROPERTY_RECURRENCE, CourseUtils.getRecurrenceRule(semesterStartDate, tz, courseData.getMeetingDays(), startTimeMilitaryPST));
    d.set(DublinCore.PROPERTY_TEMPORAL, CourseUtils.getTemporalPropertyTemplate(courseData.getTerm(), startTimeMilitaryPST, endTimeMilitaryPST));
    
    //
    final Map<Long, Tuple<Long, DublinCoreCatalog>> map = new HashMap<Long, Tuple<Long, DublinCoreCatalog>>();
    final Map<String, String> workflowProperties = getWorkflowProperties(signUp, captureAgentName, false);
    final Long[] createdIDs;
    try {
      createdIDs = schedulerService.addReccuringEvent(d, workflowProperties);
      if (createdIDs.length == 0) {
        throw new SchedulerException("Request was valid, but resulted in zero events scheduled");
      }
      for (final long id : createdIDs) {
        final Tuple<Long, DublinCoreCatalog> tuple = tuple(id, schedulerService.getEventDublinCore(id));
        schedulerService.updateCaptureAgentMetadata(MapUtils.toProperties(workflowProperties), tuple);
        map.put(id, tuple);
      }
    } catch (final UnauthorizedException e) {
      throw new SchedulerException(e);
    } catch (final NotFoundException e) {
      throw new SchedulerException(e);
    }
    return map;
  }

  /**
   * Creates a series for a course.
   * 
   * @param courseData  series related
   * @param availability  determines publishing restrictions
   * @param acl  internal security
   * @return  series {@code DublinCoreCatalog} created
   * @throws SchedulerException  if the request was invalid, or the series already exists
   */
  public DublinCoreCatalog createSeries(final CourseData courseData, final RecordingAvailability availability,
          final AccessControlList acl) throws SchedulerException {
    final DublinCoreCatalog series;
    final String seriesId = courseData.getCourseOfferingId();
    if (isExistingSeries(seriesId)) {
      throw new SchedulerException("Series already exists: " + seriesId);
    } else {
      final DublinCoreCatalogImpl catalog = DublinCoreCatalogImpl.newInstance();
      catalog.set(DublinCore.PROPERTY_IDENTIFIER, seriesId);
      catalog.set(DublinCore.PROPERTY_TITLE, courseData.getSeriesTitle());
      catalog.set(DublinCore.PROPERTY_DESCRIPTION, courseData.getSeriesDescription());
      catalog.set(DublinCore.PROPERTY_CONTRIBUTOR, courseData.getInstructors());

      License license = (availability == null) ? RecordingAvailability.studentsOnly.getLicense() : availability.getLicense();
      catalog.set(DublinCore.PROPERTY_LICENSE, license.getSalesforceValue());
      RecordingAccessRights rights = (availability == null) ? RecordingAccessRights.studentsOnlyAccessRights : getRecordingAccessRights(availability);
      catalog.set(DublinCore.PROPERTY_ACCESS_RIGHTS, rights.name());

      catalog.set(DublinCoreCatalogImpl.PROPERTY_ANNOTATION, Boolean.FALSE.toString());

      catalog.setFlavor(new MediaPackageElementFlavor("dublincore", "series"));
      //
      try {
        series = seriesService.updateSeries(catalog);
        seriesService.updateAccessControl(seriesId, acl);
      } catch (final Exception e) {
        throw new SchedulerException(e);
      }
    }
    return series;
  }

  private Map<String, String> getWorkflowProperties(final SignUp signUp, final String captureAgentName, final boolean trimHold) {
    final Integer days = signUp.getPublishDelayDays();
    final RecordingAvailability recordingAvailability = signUp.getRecordingAvailability();
    final Map<String, String> m = new HashMap<String, String>();
    m.put(CaptureParameters.CAPTURE_DEVICE_NAMES, getCaptureDeviceOptions(signUp.getRecordingType()));
    m.put(CaptureParameters.INGEST_WORKFLOW_DEFINITION, getDefaultWorkflow());
    m.put(CaptureParameters.WORKFLOW_CONFIG_TRIM_HOLD, Boolean.toString(trimHold));
    m.put(CaptureParameters.RECORDING_AVAILABILITY, recordingAvailability.name());
    m.put(CaptureParameters.DELAY_PUBLISH_BY_DAYS, days.toString());
    for (final PublicationChannel channel : recordingAvailability.getDistribution()) {
      m.put(CaptureParameters.KEY_PREFIX_WORKFLOW_CONFIG + '.' + channel.getSalesforceValue(), Boolean.TRUE.toString());
    }
    m.put(CaptureParameters.AGENT_NAME, captureAgentName);
    m.put(SchedulerService.WORKFLOW_OPERATION_KEY_SCHEDULE_LOCATION, captureAgentName);
    return m;
  }

  private RecordingAccessRights getRecordingAccessRights(final RecordingAvailability availability) {
    return availability == null || availability.getRecordingAccessRights() == null
            ? RecordingAccessRights.studentsOnlyAccessRights
            : availability.getRecordingAccessRights();
  }

  private boolean isExistingSeries(final String seriesId) throws SchedulerException {
    try {
      return seriesService.getSeries(seriesId) != null;
    } catch (final SeriesException e) {
      throw new SchedulerException(e);
    } catch (final NotFoundException e) {
      return false;
    } catch (final UnauthorizedException e) {
      throw new SchedulerException(e);
    }
  }

  /**
   * @see org.opencastproject.capture.admin.api.CaptureAgentStateService#getKnownAgents()
   * @return true if agentName matches {@link org.opencastproject.capture.CaptureParameters#AGENT_NAME} property of an Agent
   */
  public boolean isKnownAgent(final String agentName) {
    boolean knownAgent = false;
    final Collection<Agent> knownAgents = captureAgentStateService.getKnownAgents().values();
    for (final Agent agent : knownAgents) {
      final String property = StringUtils
          .trimToNull(agent.getConfiguration().getProperty(CaptureParameters.AGENT_NAME));
      if (property != null && StringUtils.containsIgnoreCase(property, agentName)) {
        knownAgent = true;
        break;
      }
    }
    return knownAgent;
  }

  private String getCaptureDeviceOptions(final RecordingType recordingType) {
    final String options;
    switch (recordingType) {
      case audioOnly:
        options = "audio";
        break;
      case screencast:
        options = "audio,screen";
        break;
      case videoOnly:
        options = "audio,video";
        break;
      case videoAndScreencast:
        options = "audio,screen,video";
        break;
      default:
        throw new UnsupportedOperationException("Unsupported recording type: " + recordingType);
    }
    return options;
  }

  private String getDefaultWorkflow() {
    return EnvironmentUtil.getMatterhornConfigProperties().get(RestConstants.WORKFLOW_DEFINITION_DEFAULT);
  }
}
