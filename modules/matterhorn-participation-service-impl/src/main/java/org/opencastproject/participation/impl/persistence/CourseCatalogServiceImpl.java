/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import com.google.common.base.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.job.api.EManagedService;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogList;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.security.api.DefaultOrganization;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.util.SecurityUtil;
import org.opencastproject.series.api.SeriesQuery;
import org.opencastproject.series.api.SeriesService;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * An OSGi service-enabled wrapper for {@code UCBerkeleyCourseDatabaseImpl}.
 *
 */
// TODO split out cache code (as a ProvidesCourseCatalogData with a factory?) when time allows
public class CourseCatalogServiceImpl extends EManagedService implements CourseCatalogService {

  private static final String REFRESH_AFTER_WRITE = "org.opencastproject.participation.impl.persistence.CourseCatalogServiceImpl.refreshAfterWrite";
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private SeriesService seriesService;
  private SecurityService securityService;
  private ExecutorService executor;
  private Notifier notifier;
  private ScheduledExecutorService publishStats;
  private UCBerkeleyCourseDatabaseImpl courseDatabase;
  private LoadingCache<String, Optional<Booking>> cachedCrossListedBookings;
  private LoadingCache<Term, Optional<Set<CourseData>>> cachedCourseData;
  private long refreshAfterWrite;
  private boolean isRefreshEnabled;
  private boolean isCacheEnabled;
  private boolean cacheIsLoaded;

  @SuppressWarnings("unused")
  protected synchronized void activate(final ComponentContext context) throws Exception {
    logger.info("Activating CourseCatalogServiceImpl");

    setCourseDatabase(new UCBerkeleyCourseDatabaseImpl(CourseUtils.getCourseDatabaseCredentials(), notifier));
    executor = Executors.newFixedThreadPool(6);
    cacheIsLoaded = false;

    publishStats = Executors.newScheduledThreadPool(2);
    publishStats.scheduleWithFixedDelay(new CachedCrossListedBookingsStats(this), 2, 60, TimeUnit.MINUTES);
  }

  @SuppressWarnings("unused")
  protected synchronized void deactivate(final ComponentContext context) {
    publishStats.shutdownNow();
    executor.shutdownNow();
    cachedCrossListedBookings.invalidateAll();
    cachedCourseData.invalidateAll();
  }

  @Override
  public Set<CourseData> getCourseData(final Semester semester, final int year) {
    if (courseDatabase == null) {
      throw new IllegalStateException("Course catalog database not accessible");
    }
    final Set<CourseData> courseData = isCacheEnabled
            ? getCached(cachedCourseData, Term.construct(semester, year))
            : courseDatabase.getCourseData();
    if (courseData.isEmpty()) {
      logger.debug("No courses found for " + semester + ", " + year);
    }
    return courseData;
  }

  @Override
  public CourseData getCourse(final CourseKey courseKey) {
    return courseDatabase.getCourse(courseKey);
  }

  @Override
  public Booking getCrossListedCourseSet(final String referenceCourseId) {
    if (!isCacheEnabled) {
      return loadBooking(referenceCourseId).orNull();
    } else {
      if (cacheIsLoaded) {
        return getCached(cachedCrossListedBookings, referenceCourseId);
      } else {
        logger.debug("Cache not available, returning null");
        return null;
      }
    }
  }

  @Override
  protected void updatedConfiguration(Properties properties) throws ConfigurationException {
    try {
      if (properties == null) {
        throw new IllegalStateException("Missing valid Properties file");
      }
      logger.info("Updated " + this.getClass().getSimpleName() + " with properties containing " + properties.size()
              + " keys");
      setRefreshAfterWrite(properties);
    } catch (final Throwable e) {
      throw new ConfigurationException(this.getClass().getSimpleName(), "Unable to load configuration properties", e);
    }
    if (isCacheEnabled) {
      buildCachedCourseData();
      buildCachedCrossListedBookings();
      seedCachedCrossListedBookings();
      cacheIsLoaded = true;
    }
  }

  // Package access for unit testing
  private CacheStats getCacheStats() {
    return cachedCrossListedBookings.stats();
  }

  private void buildCachedCourseData() {
    final CacheLoader<Term, Optional<Set<CourseData>>> cacheLoader = new CacheLoader<Term, Optional<Set<CourseData>>>() {
      public Optional<Set<CourseData>> load(final Term term) {
        final Set<CourseData> courseData = courseDatabase.getCourseData();
        return Optional.fromNullable(courseData);
      }
    };
    if (isRefreshEnabled) {
      cachedCourseData = CacheBuilder.newBuilder().refreshAfterWrite(refreshAfterWrite, TimeUnit.HOURS).recordStats().build(cacheLoader);
    } else {
      cachedCourseData = CacheBuilder.newBuilder().recordStats().build(cacheLoader);
    }
  }

  private void buildCachedCrossListedBookings() {
    if (isRefreshEnabled) {
      final CacheLoader<String, Optional<Booking>> cacheLoader = new CacheLoader<String, Optional<Booking>>() {
        public Optional<Booking> load(final String seriesId) {
          return loadBooking(seriesId);
        }

        public ListenableFuture<Optional<Booking>> reload(final String seriesId, Optional<Booking> cachedBooking) {
          if (!isRefreshEnabled) {
            return Futures.immediateFuture(cachedBooking);
          } else {
            ListenableFutureTask<Optional<Booking>> task = ListenableFutureTask
                    .create(new Callable<Optional<Booking>>() {
                      public Optional<Booking> call() {
                        return loadBooking(seriesId);
                      }
                    });
            executor.execute(task);
            return task;
          }
        }
      };
      cachedCrossListedBookings = CacheBuilder.newBuilder().refreshAfterWrite(refreshAfterWrite, TimeUnit.HOURS)
              .recordStats().build(cacheLoader);
    } else {
      final CacheLoader<String, Optional<Booking>> cacheLoader = new CacheLoader<String, Optional<Booking>>() {
        public Optional<Booking> load(final String seriesId) {
          return loadBooking(seriesId);
        }
      };
      cachedCrossListedBookings = CacheBuilder.newBuilder().recordStats().build(cacheLoader);
    }
  }

  private void seedCachedCrossListedBookings() {
    cachedCrossListedBookings.invalidateAll();
    final Set<Term> terms = new HashSet<Term>();
    try {
      final SeriesQuery query = new SeriesQuery();
      final int seriesCount = seriesService.getSeriesCount();
      query.setCount(seriesCount);

      // TODO probably a security util for this - find it
      final DefaultOrganization defaultOrg = new DefaultOrganization();
      securityService.setOrganization(defaultOrg);
      securityService.setUser(SecurityUtil.createSystemUser("matterhorn_system_account", defaultOrg));

      final DublinCoreCatalogList dublinCoreCatalogList = seriesService.getSeries(query);
      logger.debug("Seeding cache with " + dublinCoreCatalogList.getCatalogList().size() + " series");
      for (DublinCoreCatalog catalog : dublinCoreCatalogList.getCatalogList()) {
        String seriesId = catalog.getFirst(DublinCoreCatalog.PROPERTY_IDENTIFIER);
        if (StringUtils.isNotBlank(seriesId)) {
          final Optional<Booking> bookingWrapper = loadBooking(seriesId);
          if (bookingWrapper != null && bookingWrapper.isPresent()) {
            cachedCrossListedBookings.put(seriesId, Optional.fromNullable(bookingWrapper.get()));
            if (parseTerm(seriesId) != null) {
              terms.add(parseTerm(seriesId));
            }
          }
        }
      }
    } catch (Exception e) {
      throw new CourseDatabaseException("Failed to seed cache with series", e);
    }
    for (Term term : terms) {
      try {
        final List<Booking> bookings = queryBookings(term);
        logger.debug("Seeding cache with " + bookings.size() + " bookings for " + term);
        for (final Booking booking : bookings) {
          final Set<CourseData> crossListedCourses = booking.getCrossListedCourses();
          for (final CourseData courseData : crossListedCourses) {
            cachedCrossListedBookings.put(CourseUtils.getSeriesId(courseData), Optional.fromNullable(booking));
          }
        }
      } catch (Exception e) {
        throw new CourseDatabaseException("Failed to get cross-listed courses for " + term.getSemester() + ", "
                + term.getYear(), e);
      }
    }
  }

  private Optional<Booking> loadBooking(final String seriesId) {
    Booking foundBooking = null;
    try {
      // TODO replace with a query that gets the booking as opposed to all bookings then iterate approach below
      final Term term = parseTerm(seriesId);
      if (term != null) {
        FIND_MATCH:
        for (final Booking booking : queryBookings(term)) {
          final Set<CourseData> crossListedCourses = booking.getCrossListedCourses();
          for (final CourseData courseData : crossListedCourses) {
            if (CourseUtils.getSeriesId(courseData).equals(seriesId)) {
              foundBooking = booking;
              break FIND_MATCH;
            }
          }
        }
      }
    } catch (Exception e) {
      throw new CourseDatabaseException("Failed to get cross-listed courses for seriesId " + seriesId + ": "
              + e.getMessage(), e);
    }
    return Optional.fromNullable(foundBooking);
  }

  private List<Booking> queryBookings(final Term term) throws CourseDatabaseException {
    if (courseDatabase == null) {
      throw new CourseDatabaseException("Course catalog database not accessible", new IllegalStateException());
    } else {
      final Set<CourseData> courseData = getCourseData(term.getSemester(), term.getYear());
      final List<Booking> crossListedBookings = courseDatabase.getCrossListedCourses(courseData);
      if (crossListedBookings.isEmpty()) {
        logger.debug("No cross-listed courses found for " + term.getSemester() + ", " + term.getYear());
      } else {
        logger.debug(crossListedBookings.size() + " cross-listed course sets found for " + term.getSemester() + ", "
                + term.getYear());
      }
      return crossListedBookings;
    }
  }

  private void setRefreshAfterWrite(Properties properties) {
    final String refreshAfterWriteStr = (String) properties.get(REFRESH_AFTER_WRITE);
    if (StringUtils.isBlank(refreshAfterWriteStr)) {
      isRefreshEnabled = false;
      isCacheEnabled = true;
      logger.warn("Course Catalog service configured to never refresh cache entries");
    } else {
      isRefreshEnabled = true;
      refreshAfterWrite = Long.parseLong(refreshAfterWriteStr);
      if (refreshAfterWrite < 0) {
        refreshAfterWrite = 0L;
      }
      if (refreshAfterWrite == 0) {
        isCacheEnabled = false;
        logger.warn("Course Catalog service configured to refresh cache entries immediately");
      } else {
        isCacheEnabled = true;
        logger.info("Course Catalog service configured to refresh cache entries after " + refreshAfterWrite + "  hours");
      }
    }
  }

  private static Term parseTerm(final String seriesId) {
    final CourseKey key = CourseUtils.getCourseKey(seriesId);
    return (key == null) ? null : Term.construct(key.getSemester(), key.getYear());
  }

  public void setSeriesService(final SeriesService seriesService) {
    this.seriesService = seriesService;
  }

  public void setSecurityService(final SecurityService securityService) {
    this.securityService = securityService;
  }

  public void setCourseDatabase(final UCBerkeleyCourseDatabaseImpl courseDatabase) {
    this.courseDatabase = courseDatabase;
  }

  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }

  private class CachedCrossListedBookingsStats implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(CourseCatalogServiceImpl.class);

    private CourseCatalogServiceImpl courseCatalogService;

    CachedCrossListedBookingsStats(final CourseCatalogServiceImpl courseCatalogService) {
      this.courseCatalogService = courseCatalogService;
    }

    @Override
    public void run() {
      logger.debug(courseCatalogService.getCacheStats().toString());
    }
  }

  private <K, V> V getCached(final LoadingCache<K, Optional<V>> cache, final K key) {
    try {
      return cache.get(key).orNull();
    } catch (UncheckedExecutionException e) {
      throw new CourseDatabaseException("Could not retrieve from cache using key " + key, e);
    } catch (ExecutionException e) {
      throw new CourseDatabaseException("Could not retrieve from cache using key " + key, e);
    }
  }

}