/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

/**
 * @author John Crossman
 */
public class SignUp {

  private String courseOfferingId;
  private RecordingAvailability recordingAvailability;
  private RecordingType recordingType;
  private YesOrNo publishDelay;
  private Integer publishDelayDays;
  private boolean agreeToTerms;

  public String getCourseOfferingId() {
    return courseOfferingId;
  }

  public void setCourseOfferingId(final String courseOfferingId) {
    this.courseOfferingId = courseOfferingId;
  }

  public RecordingAvailability getRecordingAvailability() {
    return recordingAvailability;
  }

  public void setRecordingAvailability(final RecordingAvailability recordingAvailability) {
    this.recordingAvailability = recordingAvailability;
  }

  public RecordingType getRecordingType() {
    return recordingType;
  }

  public void setRecordingType(final RecordingType recordingType) {
    this.recordingType = recordingType;
  }

  public YesOrNo getPublishDelay() {
    return publishDelay;
  }

  public void setPublishDelay(final YesOrNo publishDelay) {
    this.publishDelay = publishDelay;
  }

  public Integer getPublishDelayDays() {
    return (publishDelayDays != null && YesOrNo.yes.equals(publishDelay)) ? publishDelayDays : 0;
  }

  public void setPublishDelayDays(final Integer publishDelayDays) {
    this.publishDelayDays = publishDelayDays;
  }

  public boolean isAgreeToTerms() {
    return agreeToTerms;
  }

  public void setAgreeToTerms(final boolean agreeToTerms) {
    this.agreeToTerms = agreeToTerms;
  }

}
