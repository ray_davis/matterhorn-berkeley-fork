/**
 * Copyright 2009, 2010 The Regents of the University of California
 * Licensed under the Educational Community License, Version 2.0
 * (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * <p/>
 * http://www.osedu.org/licenses/ECL-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author John Crossman
 */
public class Person {

  private final CourseKey courseKey;

  private final String calNetUID;
  private final String role;
  private final String firstName;
  private final String lastName;
  private final String email;
  private final String department;

  public Person(final CourseKey courseKey, final String calNetUID, final String role, final String firstName,
          final String lastName, final String email, final String department) {
    this.courseKey = courseKey;
    this.calNetUID = calNetUID;
    this.role = role;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.department = department;
  }

  public CourseKey getCourseKey() {
    return courseKey;
  }

  public String getCalNetUID() {
    return calNetUID;
  }

  public String getRole() {
    return role;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getEmail() {
    return email;
  }

  public String getDepartment() {
    return department;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public boolean equals(final Object obj) {
    final boolean equals;
    if (obj instanceof Person) {
      final Person that = (Person) obj;
      equals = courseKey.equals(that.getCourseKey()) && calNetUID.equals(that.getCalNetUID());
    } else {
      equals = false;
    }
    return equals;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(courseKey).append(calNetUID).toHashCode();
  }

}
