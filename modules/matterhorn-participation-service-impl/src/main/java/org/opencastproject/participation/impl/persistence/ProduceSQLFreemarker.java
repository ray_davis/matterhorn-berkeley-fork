/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.impl.Column;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author John Crossman
 */
public class ProduceSQLFreemarker implements ProducesSQL {

  private final Configuration configuration = new Configuration();
  private final String fileExtension;

  public ProduceSQLFreemarker(final File templateDirectory, final String fileExtension) throws IOException {
    configuration.setDirectoryForTemplateLoading(templateDirectory);
    this.fileExtension = fileExtension;
  }

  @Override
  public String produceSQL(final QueryType queryType, final Map<Column, Object> tokenMap) {
    try {
      final Template template = configuration.getTemplate(queryType.name() + "." + fileExtension);
      final StringWriter result = new StringWriter();
      template.process(stringifyKeys(tokenMap), result);
      return StringUtils.trimToEmpty(result.toString());
    } catch (final Exception e) {
      throw new RuntimeException(e);
    }
  }

  private Map<String, Object> stringifyKeys(final Map<Column, Object> tokenMap) {
    final Map<String, Object> map = new HashMap<String, Object>();
    if (tokenMap != null) {
      for (final Column column : tokenMap.keySet()) {
        map.put(column.name(), tokenMap.get(column));
      }
    }
    return map;
  }

}
