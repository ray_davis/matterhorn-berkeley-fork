/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Properties;

import oracle.jdbc.rowset.OracleCachedRowSet;

/**
 * @author John Crossman
 */
public class ResultSetProperties extends OracleCachedRowSet {

  private final Properties properties;

  public ResultSetProperties(final Properties properties) throws SQLException {
    super();
    this.properties = properties;
  }

  @Override
  public int getInt(final String columnName) throws SQLException {
    return Integer.parseInt(properties.getProperty(columnName));
  }

  @Override
  public String getString(final String columnName) throws SQLException {
    return properties.getProperty(columnName);
  }

  @Override
  public Timestamp getTimestamp(final String columnName) throws SQLException {
    final Date date = getDate(columnName);
    return date == null ? null : new java.sql.Timestamp(date.getTime());
  }

  @Override
  public Date getDate(final String columnName) throws SQLException {
    final String property = properties.getProperty(columnName);
    try {
      final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
      final java.util.Date date = dateFormat.parse(property);
      return new Date(date.getTime());
    } catch (final ParseException e) {
      throw new SQLException("Failed to parse date: " + property, e);
    }
  }
}
