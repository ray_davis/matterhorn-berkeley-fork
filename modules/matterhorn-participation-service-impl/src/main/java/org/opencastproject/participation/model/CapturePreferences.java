/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;


import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Abstraction for an instructor's elections during Opt-in.
 * 
 * @author Fernando Alvarez
 */
public class CapturePreferences {

  private RecordingType recordingType;
  private RecordingAvailability recordingAvailability;
  private int delayPublishByDays;
  
  public CapturePreferences() {
  }

  public CapturePreferences(final RecordingType recordingType, final RecordingAvailability recordingAvailability,
                            final Integer delayPublishByDays) {
    this.recordingType = recordingType;
    this.recordingAvailability = recordingAvailability;
    this.delayPublishByDays = (delayPublishByDays == null) ? 0 : delayPublishByDays;
  }

  public RecordingType getRecordingType() {
    return recordingType;
  }
  
  public void setRecordingType(RecordingType recordingType) {
    this.recordingType = recordingType;
  }

  public RecordingAvailability getRecordingAvailability() {
    return recordingAvailability;
  }
  
  public void setRecordingAvailability(RecordingAvailability recordingAvailability) {
    this.recordingAvailability = recordingAvailability;
  }
  
  public int getDelayPublishByDays() {
    return delayPublishByDays;
  }

  public void setDelayPublishByDays(int delayPublishByDays) {
    this.delayPublishByDays = delayPublishByDays;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + delayPublishByDays;
    result = prime * result + ((recordingAvailability == null) ? 0 : recordingAvailability.hashCode());
    result = prime * result + ((recordingType == null) ? 0 : recordingType.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CapturePreferences other = (CapturePreferences) obj;
    if (delayPublishByDays != other.delayPublishByDays)
      return false;
    if (recordingAvailability != other.recordingAvailability)
      return false;
    if (recordingType != other.recordingType)
      return false;
    return true;
  }
}
