/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.opencastproject.salesforce.RepresentsSalesforceField;

import javax.xml.bind.annotation.XmlEnum;

/**
 * Represents the encoding of the valid Fall, Spring, and Summer values to a character code.
 *  
 */
@XmlEnum
public enum Semester implements RepresentsSalesforceField {

  Spring('B'), Summer('C'), Fall('D');

  private final char termCode;

  Semester(final char termCode) {
    this.termCode = termCode;
  }

  public char getTermCode() {
    return termCode;
  }
  
  public static Semester parseTermCode(char termCode) {
    final Semester semester;
    switch(termCode){
      case 'D': semester = Semester.Fall;
                break;
      case 'B': semester = Semester.Spring;
                break;
      case 'C': semester = Semester.Summer;
                break;
      default:  semester = null;
  }
    return semester;
  }

  @Override
  public String getSalesforceValue() {
    return name();
  }

}
