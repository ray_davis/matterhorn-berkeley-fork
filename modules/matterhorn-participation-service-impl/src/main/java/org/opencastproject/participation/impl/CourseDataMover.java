/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.CourseDatabaseException;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.NotFoundException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;

/**
 * @author John Crossman
 */
class CourseDataMover extends TimerTask {

  private enum ColJSON implements Column<String> {
    year, semester, semesters, ccnSet;

    public Class<String> getType() {
      return String.class;
    }
  }

  private final char br = '\n';
  private final Logger logger;

  private Notifier notifier;
  private ProvidesCourseCatalogData courseDatabase;
  private CourseManagementService courseManagementService;
  private boolean throwExceptionInRunMethod = false;

  CourseDataMover() {
    this(LoggerFactory.getLogger(CourseDataMover.class));
  }

  CourseDataMover(Logger logger) {
    this.logger = logger;
  }

  @Override
  public void run() {
    final Set<Term> termSet = courseManagementService.getAllTerms();
    final List<CourseData> coursesUpdated = new LinkedList<CourseData>();
    final StringBuilder message = new StringBuilder();
    boolean errorOccurred = false;
    try {
      if (termSet.size() == 0) {
        message.append(br).append("[WARN] Zero terms in 'not-closed' stage returned from Salesforce.").append(br).append(br);
      } else {
        final List<Room> allRooms = new LinkedList<Room>(courseManagementService.getAllRooms());
        final Map<Term, List<CourseData>> feedForJunction = new HashMap<Term, List<CourseData>>();
        for (final Term term : termSet) {
          // Get courses and then reconcile cross-listings
          final Set<CourseData> allCourseData = courseDatabase.getCourseData();
          final Set<CourseData> reconciledCourseData = Booking.reconcileCrossListedCourses(allCourseData);
          final Set<CourseData> validCourseData = CourseUtils.removeCoursesWithUnrecognizedRooms(reconciledCourseData, allRooms);
          coursesUpdated.addAll(createOrUpdateCourses(validCourseData, term, allRooms, message));
          feedForJunction.put(term, coursesUpdated);
        }
        generateJsonFeedForJunction(feedForJunction);
      }
    } catch (final Exception e) {
      appendException(message, e);
      errorOccurred = true;
      if (throwExceptionInRunMethod) {
        // We do not rethrow the exception because that will invoke cancel() on the Timer instance.
        throw new CourseDatabaseException(message.toString(), e);
      }
    } finally {
      logger.info(message.toString());
      if (errorOccurred || (termSet.size() > 0 && coursesUpdated.size() > 0)) {
        final String whenRun = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        final String subject = ClassUtils.getShortClassName(this.getClass()) + ": " + whenRun;
        notifier.notifyEngineeringTeam(subject, message.toString());
      }
    }
  }

  private void generateJsonFeedForJunction(final Map<Term, List<CourseData>> feedForJunction) throws IOException {
    final JSONArray terms = new JSONArray();
    for (final Term next : feedForJunction.keySet()) {
      final JSONObject term = new JSONObject();
      put(term, ColJSON.year, next.getYear());
      put(term, ColJSON.semester, next.getSemester().name());
      final List<CourseData> courses = feedForJunction.get(next);
      final JSONArray ccnEntries = new JSONArray();
      for (final CourseData course : courses) {
        ccnEntries.put(course.getCanonicalCourse().getCcn());
      }
      put(term, ColJSON.ccnSet, ccnEntries);
      terms.put(term);
    }
    final JSONObject jsonFeed = new JSONObject();
    put(jsonFeed, ColJSON.semesters, terms);
    final String jsonContent = StringUtils.replace(jsonFeed.toString(), "},{\"year", "},\n{\"year");
    //
    final File dir = new File("/var/www/html/warehouse");
    if (dir.exists() || dir.mkdirs()) {
      FileUtils.writeStringToFile(new File(dir, "eligible-for-webcast.json"), jsonContent, "UTF-8");
    } else {
      throw new IllegalStateException("Unable to create directory " + dir.getAbsolutePath());
    }
  }

  private Collection<CourseData> createOrUpdateCourses(final Set<CourseData> incomingCourseData, final Term term, final List<Room> allRooms,
                                                       final StringBuilder message) throws NotFoundException {
    final List<CourseData> coursesUpdated = new LinkedList<CourseData>();
    // Create or update instructors attached to courses. Plus, put salesforce IDs back to courses.
    final Set<Instructor> instructorSet = CourseUtils.extractInstructorSet(incomingCourseData);
    final List<Instructor> instructorListFromSalesforce = courseManagementService.createOrUpdateInstructors(instructorSet);
    CourseUtils.setSalesforceIDs(incomingCourseData, allRooms, instructorListFromSalesforce, term);
    courseManagementService.setSalesforceRoomCapability(incomingCourseData, allRooms);
    courseManagementService.createOrUpdateCourses(incomingCourseData);
    message.append("[INFO] ").append(incomingCourseData.size()).append(" courses in capture-enabled.").append(br).append(
            br);
    int courseIndex = 1;
    for (final CourseData courseData : incomingCourseData) {
      message.append(courseIndex++).append(". ").append(courseData).append(br).append(br);
    }
    coursesUpdated.addAll(incomingCourseData);
    return coursesUpdated;
  }

  void setCourseDatabase(final ProvidesCourseCatalogData courseDatabase) {
    this.courseDatabase = courseDatabase;
  }

  public void setCourseManagementService(final CourseManagementService courseManagementService) {
    this.courseManagementService = courseManagementService;
  }

  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }

  void setThrowExceptionInRunMethod(final boolean throwExceptionInRunMethod) {
    this.throwExceptionInRunMethod = throwExceptionInRunMethod;
  }

  private void appendException(final StringBuilder b, final Exception e) {
    b.append("Exception class: ").append(e.getClass().getSimpleName()).append('\n');
    b.append("Exception message: ").append(e.getMessage()).append('\n');
    b.append(ExceptionUtils.getStackTrace(e)).append('\n');
  }

  private void put(final JSONObject entry, Column<?> column, final Object object) {
    if (object != null) {
      try {
        entry.put(column.name(), object);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
  }

}
