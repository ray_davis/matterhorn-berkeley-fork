/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.opencastproject.participation.CourseUtils;

/**
 * @author John Crossman
 */
public class CourseKey implements HasCourseKey {

  private final int year;
  private final Semester semester;
  private final int ccn;

  public CourseKey(final int year, final Semester semester, final int ccn) {
    this.year = year;
    this.semester = semester;
    this.ccn = ccn;
  }

  public int getYear() {
    return year;
  }

  public Semester getSemester() {
    return semester;
  }

  public int getCcn() {
    return ccn;
  }
  
  @Override
  public final boolean equals(final Object o) {
    return CourseUtils.equals(this, o);
  }

  @Override
  public final int hashCode() {
    return CourseUtils.hashCode(this);
  }

  @Override public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
