/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.opencastproject.participation.impl.Column;

/**
 * Columns names of the Oracle materialized view we're using to pull course data. Package-local
 * for sake of unit test.
 */
enum ColStr implements Column<String> {

  course_title, catalog_id, dept_description, instruction_format, meeting_start_time, meeting_start_time_ampm_flag,
  meeting_end_time, meeting_end_time_ampm_flag, term_name, term_yr, building_name, meeting_days, section_num,
  room_number, first_name, last_name, email_address, ldap_uid, dept_name, term_cd, location, display_name;

  @Override
  public Class<String> getType() {
    return String.class;
  }

}
