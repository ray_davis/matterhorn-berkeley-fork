/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.impl.MVUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Room;
import org.opencastproject.util.date.DateUtil;
import org.opencastproject.util.date.TimeOfDay;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
class ResultConverterForCourse extends AbstractResultSetRowConverter<CourseData> {

  private final boolean includeStudentCount;

  ResultConverterForCourse(final boolean includeStudentCount) {
    this.includeStudentCount = includeStudentCount;
  }

  @Override
  public CourseData convert(final ResultSet r) throws SQLException {
    final String catalogId = get(r, ColStr.catalog_id);
    final String instructionFormat = get(r, ColStr.instruction_format);
    final String sectionNum = get(r, ColStr.section_num);
    final CourseData c = new CourseData();
    final Integer ccn = get(r, ColInt.course_cntl_num);
    final String deptName = get(r, ColStr.dept_name);
    final String displayName = get(r, ColStr.display_name);
    String name;
    if (StringUtils.isNotBlank(displayName)) {
      name = displayName;
    } else if (StringUtils.isNotBlank(deptName) && StringUtils.isNotBlank(catalogId)) {
      name = deptName + ' ' + catalogId;
    } else {
      name = get(r, ColStr.course_title);
    }
    if (StringUtils.isNotBlank(sectionNum)) {
      name = name.concat(", ").concat(StringUtils.isBlank(instructionFormat) ? sectionNum : instructionFormat + ' ' + sectionNum);
    }
    c.setCanonicalCourse(new CanonicalCourse(ccn, name, name));
    c.setCatalogId(catalogId);
    c.setDeptName(deptName);
    c.setDisplayName(displayName);
    c.setDeptDescription(get(r, ColStr.dept_description));
    c.setTerm(term);
    //
    if (edoDatabase) {
      c.setStartTime(StringUtils.remove(get(r, ColStr.meeting_start_time), ':'));
      c.setEndTime(StringUtils.remove(get(r, ColStr.meeting_end_time), ':'));
    } else {
      String amPmFlag = get(r, ColStr.meeting_start_time_ampm_flag);
      final TimeOfDay startTime = getTimeOfDay(get(r, ColStr.meeting_start_time), amPmFlag);
      c.setStartTime(DateUtil.getMilitaryTime(startTime));
      final TimeOfDay endTime = getTimeOfDay(get(r, ColStr.meeting_end_time), get(r, ColStr.meeting_end_time_ampm_flag));
      c.setEndTime(DateUtil.getMilitaryTime(endTime));
    }
    c.setRoom(getRoom(r));
    String meetingDays = StringUtils.trimToNull(get(r, ColStr.meeting_days));
    if (edoDatabase) {
      if (meetingDays != null) {
        List<DayOfWeek> dayList = new LinkedList<DayOfWeek>();
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
          if (meetingDays.contains(dayOfWeek.getEdoToken())) {
            dayList.add(dayOfWeek);
          }
        }
        c.setMeetingDays(dayList);
      }
    } else {
      c.setMeetingDays(MVUtils.getMeetingDays(meetingDays));
    }
    c.setSection(sectionNum);
    if (includeStudentCount) {
      c.setStudentCount(get(r, ColInt.student_count));
    }
    return c;
  }

  /**
   * Package-local for sake of unit test.
   * @param hoursMinutes for example, "0830", "1100", "0500", etc.
   * @param amPmFlag "A" for AM and "P" for PM
   * @return appropriate according to rules above
   */
  static TimeOfDay getTimeOfDay(final String hoursMinutes, final String amPmFlag) {
    final TimeOfDay timeOfDay;
    if (StringUtils.isBlank(hoursMinutes) || StringUtils.isBlank(amPmFlag)) {
      timeOfDay = null;
    } else {
      final Integer hoursMinutesNumber = Integer.parseInt(hoursMinutes);
      final int hour = hoursMinutesNumber / 100;
      final int minutes = (hoursMinutesNumber % 100);
      final TimeOfDay.DayPeriod dayPeriod = ("A".equalsIgnoreCase(amPmFlag)) ? TimeOfDay.DayPeriod.AM : TimeOfDay.DayPeriod.PM;
      timeOfDay = new TimeOfDay(hour, minutes, dayPeriod);
    }
    return timeOfDay;
  }

  private Room getRoom(final ResultSet r) throws SQLException {
    final Room room;
    if (edoDatabase) {
      final String location = extractLocation(r);
      if (location == null || location.equals("Requested General Assignment")) {
        room = null;
      } else {
        room = new Room();
        final String[] tokens = StringUtils.split(location);
        int length = tokens.length;
        final String lastToken = tokens[length - 1];
        if (length > 1 && lastToken.matches(".*\\d+.*")) {
          room.setBuilding(StringUtils.trimToNull(StringUtils.join(ArrayUtils.subarray(tokens, 0, length - 1), ' ')));
          room.setRoomNumber(StringUtils.trimToNull(lastToken));
        } else {
          room.setBuilding(location);
        }
      }
    } else {
      room = new Room();
      final String building = get(r, ColStr.building_name);
      if (StringUtils.containsIgnoreCase(building, "wheeler") && StringUtils.containsIgnoreCase(building, "aud")) {
        room.setBuilding("Wheeler");
        room.setRoomNumber("150");
      } else {
        room.setBuilding(StringUtils.trimToNull(building));
        final String roomNumber = parseRoomNumber(get(r, ColStr.room_number));
        room.setRoomNumber(roomNumber);
      }
    }
    return room;
  }

  private String extractLocation(final ResultSet r) throws SQLException {
    String location = StringUtils.trimToNull(get(r, ColStr.location));
    location = StringUtils.replace(location, "Genetics & Plant Bio", "GPB");
    return location;
  }

  /**
   * Package-local for sake of unit test.
   * @param roomNumberString for example: '0358A', ' D0025', '0009', etc.
   * @return extracted number
   */
  private String parseRoomNumber(final String roomNumberString) {
    final String parsed;
    if (StringUtils.isBlank(roomNumberString)) {
      parsed = null;
    } else if (StringUtils.isNumeric(roomNumberString)) {
      parsed = Integer.valueOf(roomNumberString).toString();
    } else {
      parsed = StringUtils.trimToNull(roomNumberString);
    }
    return parsed;
  }
}
