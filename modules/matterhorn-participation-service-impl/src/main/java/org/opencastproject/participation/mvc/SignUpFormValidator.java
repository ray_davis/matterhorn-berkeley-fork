/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.SignUp;
import org.opencastproject.participation.model.YesOrNo;
import org.opencastproject.security.api.User;
import org.opencastproject.security.util.SecurityUtil;
import org.springframework.validation.Errors;

import java.util.Set;

/**
 * @author John Crossman
 */
public class SignUpFormValidator {

  public void validateBeforeProcess(final CourseOffering courseOffering, final User user, final Errors errors) {
    if (courseOffering == null) {
      errors.reject("error.signUp.courseOfferingIdMissing");
    } else if (user == null) {
      errors.reject("error.signUp.logInRequired");
    } else if (courseOffering.getRoom() == null || courseOffering.getRoom().getCapability() == null) {
      errors.reject("courseMovedToIneligibleRoom");
    } else if (courseOffering.isScheduled()) {
      final CapturePreferences cp = courseOffering.getCapturePreferences();
      if (cp == null || cp.getRecordingType() == null || cp.getRecordingAvailability() == null) {
        errors.reject("error.signUp.scheduledRecordingsWithMissingPreferences");
      }
      if (SecurityUtil.isAdminUser(user)) {
        errors.reject("recordingsHaveBeenScheduled");
      }
    }
    if (courseOffering != null) {
      for (final Participation p : courseOffering.getParticipationSet()) {
        if (p.isApproved() && user.getUserName().equals(p.getInstructor().getCalNetUID())) {
          errors.reject("youHaveAlreadySignedUp");
        }
      }
    }
  }

  public void validatePost(final SignUp signUp, final User user, final Set<Participation> participationSet, final boolean mustOnlyAgreeToTerms, final Errors errors) {
    if (signUp.getCourseOfferingId() == null) {
      errors.reject("error.signUp.courseOfferingIdMissing");
    }
    if (user == null) {
      errors.reject("error.signUp.logInRequired");
    } else {
      if (!SecurityUtil.isAdminUser(user) && !signUp.isAgreeToTerms()) {
        errors.reject("agreeToTerms", "error.signUp.agreeToTerms");
      }
      for (final Participation p : participationSet) {
        final String userName = user.getUserName();
        final boolean sameUser = p.getInstructor().getCalNetUID().equals(userName);
        if (sameUser && p.isApproved()) {
          errors.reject("error.signUp.userAlreadyApproved");
        }
      }
    }
    if (!mustOnlyAgreeToTerms) {
      if (signUp.getPublishDelay() == null) {
        errors.rejectValue("publishDelay", "error.signUp.publishDelayUnspecified");
      } else if (YesOrNo.yes.equals(signUp.getPublishDelay()) && signUp.getPublishDelayDays() == null) {
        errors.rejectValue("publishDelayDays", "error.signUp.publishDelayDaysUnspecified");
      }
      if (signUp.getRecordingAvailability() == null) {
        errors.rejectValue("recordingAvailability", "error.signUp.recordingAvailabilityUnspecified");
      }
      if (signUp.getRecordingType() == null) {
        errors.rejectValue("recordingType", "error.signUp.recordingTypeUnspecified");
      }
    }
    if (errors.hasFieldErrors()) {
      errors.reject("error.signUp.seeErrors");
    }
  }

  boolean isAuthorized(final User user, final Set<Participation> participationSet) {
    boolean authorized = false;
    if (SecurityUtil.isAdminUser(user)) {
      authorized = true;
    } else {
      final String userName = StringUtils.trimToNull(user.getUserName());
      if (participationSet != null && userName != null) {
        for (final Participation participation : participationSet) {
          if (userName.equals(participation.getInstructor().getCalNetUID())) {
            authorized = true;
            break;
          }
        }
      }
    }
    return authorized;
  }
}
