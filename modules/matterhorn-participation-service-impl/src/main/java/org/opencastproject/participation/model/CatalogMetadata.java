/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author John Crossman
 */
public class CatalogMetadata {

  private String deptName;
  private String catalogId;

  public String getDeptName() {
    return deptName;
  }

  public void setDeptName(final String deptName) {
    this.deptName = deptName;
  }

  public String getCatalogId() {
    return catalogId;
  }

  public void setCatalogId(final String catalogId) {
    this.catalogId = catalogId;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(deptName).append(catalogId).toHashCode();
  }

  @Override
  public boolean equals(final Object o) {
    CatalogMetadata other = o instanceof CatalogMetadata ? (CatalogMetadata) o : null;
    return other != null && StringUtils.equals(deptName, other.getDeptName()) && StringUtils.equals(catalogId, other.getCatalogId());
  }
}
