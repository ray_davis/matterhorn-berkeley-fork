/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.participation.model;

import org.apache.commons.collections.SetUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * <code>Booking</code> provides an abstraction for a room that has been
 * booked, or of more interest over-booked. This last condition is a result
 * of cross-listing courses in the course catalog. A room is over-booked when
 * two or more courses are scheduled for that room on the same days of the week
 * starting at the same time.
 *
 * @author Fernando Alvarez
 *
 */
public class Booking {

  private final Term term;
  private final Room room;
  private final List<DayOfWeek> bookedDays;
  private final String bookedFrom;
  private final SortedSet<CourseData> crossListedCourses = new TreeSet<CourseData>(new CrossListedCourseDataComparator());

  private Booking(CourseData course) {
    this.term = course.getTerm();
    this.room = course.getRoom();
    this.bookedDays = course.getMeetingDays();
    this.bookedFrom = course.getStartTime();
    this.crossListedCourses.add(course);
  }

  /**
   * Implements an algorithm to reconcile the over-booking (i.e. cross-listed courses) of a room.
   *
   * @param courses potentially cross-listed courses for the term
   * @return set of courses without any cross-listed courses
   */
  public static Set<CourseData> reconcileCrossListedCourses(final Set<CourseData> courses) {
    final Set<CourseData> noCrossListedCourses = new HashSet<CourseData>();
    for (final Booking bookedRoom : bookCourses(courses)) {
      noCrossListedCourses.add(bookedRoom.getCourseData());
    }
    return noCrossListedCourses;
  }

  boolean isCrossListed() {
    return getcrossListedCourses().size() > 1;
  }

  public static List<Booking> crossListedBookings(final Set<CourseData> courses) {
    final List<Booking> crossListedCourses = new ArrayList<Booking>();
    for (final Booking booking : bookCourses(courses)) {
      if (booking.isCrossListed()) {
        crossListedCourses.add(booking);
      }
    }
    return crossListedCourses;
  }

  @SuppressWarnings("all")
  public Set<CourseData> getCrossListedCourses() {
    return SetUtils.unmodifiableSet(crossListedCourses);
  }

  private static Collection<Booking> bookCourses(final Set<CourseData> courses) {
    final Map<String, Booking> bookingMap = new HashMap<String, Booking>(courses.size());
    for (final CourseData course : courses) {
      if (course.getRoom() != null && course.getStartTime() != null && course.getMeetingDays() != null) {
        final String key = course.getYear() + StringUtils.EMPTY + course.getTerm().getSemester().getTermCode()
                + toString(course.getRoom()) + toString(course.getMeetingDays()) + course.getStartTime();
        if (bookingMap.containsKey(key)) {
          bookingMap.get(key).getcrossListedCourses().add(course);
        } else {
          bookingMap.put(key, new Booking(course));
        }
      } else {
        LoggerFactory.getLogger(Booking.class).warn("Course has NULL properties and we must ignore it: " + course);
      }
    }
    return bookingMap.values();
  }

  private static String toString(final List<DayOfWeek> meetingDays) {
    Collections.sort(meetingDays, new Comparator<DayOfWeek>() {
      @Override public int compare(final DayOfWeek d1, final DayOfWeek d2) {
        return new Integer(d1.getIndexAccordingToMaterializedView()).compareTo(d2.getIndexAccordingToMaterializedView());
      }
    });
    final StringBuilder b = new StringBuilder();
    for (final DayOfWeek d : meetingDays) {
      b.append(d.name());
    }
    return b.toString();
  }

  private static String toString(final Room room) {
    final String building = room.getBuilding() == null ? StringUtils.EMPTY : room.getBuilding().toLowerCase();
    final String roomNumer = room.getRoomNumber() == null ? StringUtils.EMPTY : room.getRoomNumber().toLowerCase();
    return building + roomNumer;
  }

  private CourseData getCourseData() {
    final Set<Participation> participationList = new HashSet<Participation>();
    for (CourseData c : getcrossListedCourses()) {
      if (c.getParticipationSet() != null) {
        participationList.addAll(c.getParticipationSet());
      }
    }
    CourseData course = getcrossListedCourses().first();
    course.getParticipationSet().clear();
    course.getParticipationSet().addAll(participationList);
    return course;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((bookedDays == null) ? 0 : bookedDays.hashCode());
    result = prime * result + ((bookedFrom == null) ? 0 : bookedFrom.hashCode());
    result = prime * result + ((room == null) ? 0 : room.hashCode());
    result = prime * result + ((term == null) ? 0 : term.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Booking other = (Booking) obj;
    if (bookedDays == null) {
      if (other.bookedDays != null) {
        return false;
      }
    } else if (!bookedDays.equals(other.bookedDays)) {
      return false;
    }
    if (bookedFrom == null) {
      if (other.bookedFrom != null) {
        return false;
      }
    } else if (!bookedFrom.equals(other.bookedFrom)) {
      return false;
    }
    if (room == null) {
      if (other.room != null) {
        return false;
      }
    } else if (!room.equals(other.room)) {
      return false;
    }
    if (term == null) {
      if (other.term != null) {
        return false;
      }
    } else if (!term.equals(other.term)) {
      return false;
    }
    return true;
  }

  private SortedSet<CourseData> getcrossListedCourses() {
    return crossListedCourses;
  }

  // Used to order cross-listed courses within a Booking
  private static class CrossListedCourseDataComparator implements Comparator<CourseData> {
    @Override
    public int compare(final CourseData c1, final CourseData c2) {
      final String title1 = StringUtils.trimToEmpty(c1.getCanonicalCourse().getTitle());
      final String title2 = StringUtils.trimToEmpty(c2.getCanonicalCourse().getTitle());
      final boolean sameTitle = StringUtils.equalsIgnoreCase(title1, title2);
      final String deptName1 = StringUtils.trimToEmpty(c1.getDeptName());
      final String deptName2 = StringUtils.trimToEmpty(c2.getDeptName());
      return sameTitle ? deptName1.compareTo(deptName2) : title1.compareTo(title2);
    }
  }

}
