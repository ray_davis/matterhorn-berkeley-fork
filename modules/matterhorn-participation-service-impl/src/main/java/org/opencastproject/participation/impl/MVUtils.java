/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.model.DayOfWeek;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
public final class MVUtils {

  private static final Logger logger = LoggerFactory.getLogger(MVUtils.class);

  /**
   * Private.
   */
  private MVUtils() {
  }

  /**
   * Examples:
   *  " M  W  " returns list: Monday, Wednesday
   *  "  T T " returns list: Tuesday, Thursday
   * @param untrimmed expected value from materialized view
   * @return according to rules above
   */
  public static List<DayOfWeek> getMeetingDays(final String untrimmed) {
    final String parseThis = StringUtils.stripEnd(untrimmed, null);
    final List<DayOfWeek> meetingDays = new LinkedList<DayOfWeek>();
    final int length = StringUtils.length(parseThis);
    if (length > 0) {
      if (!StringUtils.containsIgnoreCase(parseThis, "unsched")) {
        for (int index = 0; index < length; index++) {
          final char c = parseThis.charAt(index);
          if (c != ' ') {
            final DayOfWeek dayOfWeek = getByIndex(index);
            if (dayOfWeek == null) {
              logger.warn("Null " + DayOfWeek.class.getSimpleName() + " returned for index = " + index);
            } else if (Character.toLowerCase(dayOfWeek.name().charAt(0)) != Character.toLowerCase(c)) {
              throw new IllegalArgumentException(dayOfWeek + " does NOT have the expected leading character: " + c);
            } else {
              meetingDays.add(dayOfWeek);
            }
          }
        }
      }
    }
    return meetingDays.isEmpty() ? null : meetingDays;
  }

  private static DayOfWeek getByIndex(final int index) {
    DayOfWeek dayOfWeek = null;
    final int length = DayOfWeek.values().length;
    if (index >= 0 && index < length) {
      for (final DayOfWeek next : DayOfWeek.values()) {
        if (next.getIndexAccordingToMaterializedView() == index) {
          dayOfWeek = next;
          break;
        }
      }
    } else {
      logger.warn("Index value is expected to be greater than 0 and less than " + length);
    }
    return dayOfWeek;
  }

}
