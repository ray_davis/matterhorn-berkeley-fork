/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.StringUtils;

import java.util.Comparator;

/**
 * Used to order cross-listed courses within a Booking.
 * TODO: Resolve WCT-5170 by using this comparator instead of {@link org.opencastproject.participation.model.Booking.CrossListedCourseDataComparator}
 * @author John Crossman
 */
public class CrossListedCourseComparator implements Comparator<CourseData> {

  @Override
  public int compare(final CourseData c1, final CourseData c2) {
    final String name1 = StringUtils.trimToEmpty(c1.getCanonicalCourse().getName());
    final String name2 = StringUtils.trimToEmpty(c2.getCanonicalCourse().getName());
    final Integer lengthName1 = name1.length();
    final Integer lengthName2 = name2.length();
    final int compare;
    if (lengthName1.equals(lengthName2)) {
      compare = StringUtils.trimToEmpty(c1.getCatalogId()).compareTo(c2.getCatalogId());
    } else {
      compare = lengthName1.compareTo(lengthName2);
    }
    return compare;
  }

}