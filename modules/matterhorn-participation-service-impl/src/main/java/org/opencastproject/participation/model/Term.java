/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.salesforce.HasSalesforceId;

import java.util.Properties;

/**
 * @author John Crossman
 */
public class Term implements HasSalesforceId {

  private final Semester semester;
  private final Integer year;
  private final String startDate;
  private final String endDate;
  private String salesforceID;

  public static Term getTermFromProperties() {
    final Properties properties = CourseUtils.getCourseManagementServiceProperties();
    final String termId = properties.getProperty("term.id");
    return construct(termId, properties.getProperty("term.startDate"), properties.getProperty("term.endDate"));
  }

  public static Term construct(final String edoTermId,
                               final String startDate,
                               final String endDate) {
    final char termCd = termCodeEdoToLegacy(edoTermId.charAt(3));
    final int termYr = Integer.parseInt(edoTermId.charAt(0) + "0" + edoTermId.charAt(1) + edoTermId.charAt(2));
    return construct(Semester.parseTermCode(termCd), termYr, startDate, endDate, null);
  }

  public static Term construct(final Semester semester,
                               final Integer year,
                               final String startDate,
                               final String endDate,
                               final String salesforceID) {
    return new Term(semester, year, startDate, endDate, salesforceID);
  }

  public static Term construct(final Semester semester, final Integer year) {
    return new Term(semester, year, null, null, null);
  }

  private Term(final Semester semester,
               final Integer year,
               final String startDate,
               final String endDate,
               final String salesforceID) {
    this.semester = semester;
    this.year = year;
    this.startDate = startDate;
    this.endDate = endDate;
    this.salesforceID = salesforceID;
  }

  public String getSalesforceID() {
    return salesforceID;
  }

  public void setSalesforceID(String salesforceID) {
    this.salesforceID = salesforceID;
  }

  public String getStartDate() {
    return startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public Semester getSemester() {
    return semester;
  }

  public Integer getYear() {
    return year;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("semester", semester)
        .append("year", year)
        .append("startDate", startDate)
        .append("endDate", endDate).toString();
  }

  
  @Override
  public boolean equals(final Object that) {
    return (that instanceof Term) && new EqualsBuilder().append(this.getSemester(), ((Term) that).getSemester())
        .append(this.getYear(), ((Term) that).getYear()).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(semester).append(year).toHashCode();
  }

  private static char termCodeEdoToLegacy(final char semesterCodeEdo) {
    switch (semesterCodeEdo) {
      case '2':
        return 'B';
      case '5':
        return 'C';
      case '8':
        return 'D';
      default:
        throw new UnsupportedOperationException("Unrecognized semester code (EDO): " + semesterCodeEdo);
    }
  }

}
