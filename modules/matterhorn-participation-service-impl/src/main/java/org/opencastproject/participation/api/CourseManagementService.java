/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.api;

import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Term;
import org.opencastproject.security.api.User;
import org.opencastproject.util.NotFoundException;

import java.util.List;
import java.util.Set;

public interface CourseManagementService {

  /**
   * @param courseOfferingId Null not allowed.
   * @return null when not course not found.
   */
  CourseOffering getCourseOffering(String courseOfferingId);

  /**
   * @param courseOfferingId never null
   * @param user never null
   * @param capturePreferences never null
   * @throws NotFoundException
   */
  void updateCapturePreferences(String courseOfferingId, User user, CapturePreferences capturePreferences) throws NotFoundException;


  void approveCapturePreferences(String courseOfferingId, String calNetUID) throws NotFoundException;

  /**
   * System notifies Salesforce that recordings have been scheduled.
   * @param courseOfferingId never null
   * @throws NotFoundException when course not found
   */
  void setRecordingsScheduledTrue(String courseOfferingId) throws NotFoundException;

  /**
   * Update or insert records in Salesforce.
   * @param courseOfferingSet one or more courses. Never null.
   */
  void createOrUpdateCourses(Set<CourseData> courseOfferingSet);

  /**
   * Update or insert records in Salesforce.
   * @param incomingInstructorSet zero or more instructors. Never null.
   */
  List<Instructor> createOrUpdateInstructors(Set<Instructor> incomingInstructorSet);

  /**
   * @param courseOfferingId never null
   * @return name of capture agent, using conventions based on room information.
   */
  String getCaptureAgentName(String courseOfferingId);

  /**
   * Get all semester records, with 'active' status, from Salesforce.
   * @return zero or more records.
   */
  Set<Term> getAllTerms();

  /**
   * Get all rooms, capture-enabled or otherwise, known to Salesforce.
   * @return zero or more records. Never null.
   */
  Set<Room> getAllRooms();
  
  /**
   * Adds a room's course capture capability to Course data from the Materialized View.
   * 
   * @param incomingCourseData Courses with presumably null Room capability
   * @param allRooms list pulled from Salesforce. All entries must have non-null Room capability
   */
  void setSalesforceRoomCapability(Set<CourseData> incomingCourseData, List<Room> allRooms);

}
