/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.model.CatalogMetadata;

import java.sql.ResultSet;
import java.sql.SQLException;

class ResultConverterForCatalogMetadata extends AbstractResultSetRowConverter<CatalogMetadata> {

  @Override
  public CatalogMetadata convert(final ResultSet r) throws SQLException {
    String deptName = get(r, ColStr.dept_name);
    String catalogId = get(r, ColStr.catalog_id);
    final CatalogMetadata c;
    if (StringUtils.isBlank(deptName) && StringUtils.isBlank(catalogId)) {
      c = null;
    } else {
      c = new CatalogMetadata();
      c.setDeptName(deptName);
      c.setCatalogId(catalogId);
    }
    return c;
  }

}
