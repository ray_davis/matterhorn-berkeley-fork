/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import org.opencastproject.job.api.EManagedService;
import org.opencastproject.notify.Notifier;
import org.opencastproject.util.data.Collections;

import com.sforce.soap.partner.Error;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.bind.XmlObject;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author John Crossman
 */
public final class SalesforceConnectorServiceImpl extends EManagedService implements SalesforceConnectorService {

  private Logger logger = LoggerFactory.getLogger(SalesforceConnectorServiceImpl.class);

  private SalesforceConnectionFactory connectionFactory;

  private Notifier notifier;

  /**
   * Constructor.
   */
  public SalesforceConnectorServiceImpl() {
  }

  /**
   * Constructor.
   */
  public SalesforceConnectorServiceImpl(final Logger logger) {
    this.logger = logger;
  }

  @Override
  public QueryResult query(final SalesforceQueryType queryType, final Map<String, ?> args) {
    try {
      String query = queryType.getSOQL();
      if (args != null) {
        for (final String key : args.keySet()) {
          query = query.replaceAll("\\{" + key + "\\}", args.get(key).toString());
        }
      }
      logger.debug(query);
      final PartnerConnection connection = connectionFactory.getConnection();
      return cleanse(connection.query(query));
    } catch (final ConnectionException e) {
      final String message = "Salesforce query failed: " + queryType;
      notifier.notifyEngineeringTeam(message, e);
      throw new SalesforceConnectionException(message, e);
    }
  }

  @Override
  public SaveResult[] update(final SObject... sObjects) {
    try {
      final PartnerConnection connection = connectionFactory.getConnection();
      final SaveResult[] saveResults = connection.update(sObjects);
      reportError(saveResults);
      return saveResults;
    } catch (final ConnectionException e) {
      final StringBuilder message = new StringBuilder("Update failed on sObjects.length = ").append(sObjects.length);
      for (final SObject sObject : sObjects) {
        message.append("; sObject.id = ").append(sObject == null ? "null" : sObject.getId());
      }
      notifier.notifyEngineeringTeam(message.toString(), e);
      throw new SalesforceConnectionException(message.toString(), e);
    }
  }

  @Override
  public SaveResult[] create(final List<SObject> sObjectList) throws ConnectionException {
    final SObject[] sObjects = sObjectList.toArray(new SObject[sObjectList.size()]);
    final PartnerConnection connection = connectionFactory.getConnection();
    final SaveResult[] saveResults = connection.create(sObjects);
    reportError(saveResults);
    return saveResults;
  }

  @Override
  protected void updatedConfiguration(final Properties properties) throws ConfigurationException {
    connectionFactory = new SalesforceConnectionFactory(properties, notifier, logger);
  }

  private void reportError(final SaveResult[] saveResults) {
    for (final SaveResult saveResult : saveResults) {
      final com.sforce.soap.partner.Error[] errors = saveResult.getErrors();
      if (errors != null && errors.length > 0) {
        for (final Error error : errors) {
          logger.error("SaveResult #" + saveResult.getId() + " error: " + error.getMessage());
        }
      }
    }
  }

  public void activate(final ComponentContext cc) throws Exception {
    logger.info("Activating SalesforceConnectorServiceImpl Service");
  }

  /**
   * @param notifier Required property. Used to notify engineering when server errors happen.
   */
  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }

  private QueryResult cleanse(final QueryResult queryResult) {
    if (queryResult.getRecords() != null) {
      for (final SObject record : queryResult.getRecords()) {
        if (record.hasChildren()) {
          final List<String> names = new LinkedList<String>();
          final List<XmlObject> children = Collections.toList(record.getChildren());
          for (final XmlObject child : children) {
            final String localPart = SalesforceUtils.getLocalPart(child);
            if (names.contains(localPart)) {
              record.removeField(localPart);
            } else {
              names.add(localPart);
            }
          }
        }
      }
    }
    return queryResult;
  }
}
