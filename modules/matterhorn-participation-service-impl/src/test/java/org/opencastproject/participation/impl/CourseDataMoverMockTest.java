/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceConnectorService;
import org.opencastproject.salesforce.SalesforceObjectTransformer;
import org.opencastproject.salesforce.SalesforceQueryType;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Collections;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
@Ignore
public class CourseDataMoverMockTest {

  private CourseDataMover dataMover;
  private SalesforceObjectTransformer salesforceObjectTransformer;
  private ProvidesCourseCatalogData courseDatabase;
  private VoidNotifier notifier;
  private Term term;

  @Before
  public void before() {
    term = Term.construct(Semester.Fall, 2013, null, null, null);
    term.setSalesforceID(term.hashCode() + "");
    final CourseManagementServiceImpl courseManagementService = new CourseManagementServiceImpl();
    final MockSalesforceConnectorService salesforceConnectorService = new MockSalesforceConnectorService();
    //
    salesforceObjectTransformer = createMock(SalesforceObjectTransformer.class);
    expect(salesforceObjectTransformer.getTerm(anyObject(SObject.class))).andReturn(term).anyTimes();
    expect(salesforceObjectTransformer.getInstructor(anyObject(SObject.class))).andReturn(createInstructor()).anyTimes();
    salesforceObjectTransformer.setFieldsForCourseUpdate(anyObject(SObject.class), anyObject(CourseData.class));
    expectLastCall();
    courseManagementService.setSalesforceConnectorService(salesforceConnectorService);
    courseManagementService.setSalesforceObjectTransformer(salesforceObjectTransformer);
    courseDatabase = createMock(ProvidesCourseCatalogData.class);
    //
    dataMover = new CourseDataMover();
    dataMover.setCourseManagementService(courseManagementService);
    dataMover.setThrowExceptionInRunMethod(true);
    dataMover.setCourseDatabase(courseDatabase);
    notifier = new VoidNotifier();
    dataMover.setNotifier(notifier);
    dataMover.setThrowExceptionInRunMethod(true);
  }

  @Test
  public void testNoCaptureEnabledRooms() throws NotFoundException {
    final Room room = getRoom(null);
    expect(salesforceObjectTransformer.getRoom(anyObject(SObject.class))).andReturn(room).anyTimes();
    final CourseData course = createCourse(room);
    final Set<CourseData> set = Collections.set(course);
    expect(courseDatabase.getCourseData()).andReturn(set).once();
    // No rooms are capture enabled so EXPECT NO CALL to salesforceObjectTransformer
    expectExtractAssociatedCourseOfferings(course);
    replay(salesforceObjectTransformer, courseDatabase);
    dataMover.run();
    // No notification because nothing created in Salesforce
    assertFalse(notifier.isEngineeringNotified());
  }

  @Test
  public void testUpsertCourses() throws NotFoundException {
    final Room room = getRoom(RoomCapability.screencastAndVideo);
    expect(salesforceObjectTransformer.getRoom(anyObject(SObject.class))).andReturn(room).anyTimes();
    final CourseData course = createCourse(room);
    final Set<CourseData> set = Collections.set(course);
    expect(courseDatabase.getCourseData()).andReturn(set).once();
    for (final CourseData courseData : set) {
      expect(salesforceObjectTransformer.getSObjectForCourseCreation(courseData)).andReturn(getMockSObject()).once();
    }
    expectExtractAssociatedCourseOfferings(course);
    replay(salesforceObjectTransformer, courseDatabase);
    dataMover.run();
    assertTrue(notifier.isEngineeringNotified());
  }

  private SObject getMockSObject() {
    final SObject s = new SObject();
    s.setId("id-" + s.hashCode());
    return s;
  }

  private void expectExtractAssociatedCourseOfferings(final CourseData course) {
    final Map<CourseData, SObject> map = new HashMap<CourseData, SObject>();
    final CourseOffering courseOffering = new CourseOffering();
    courseOffering.setCanonicalCourse(course.getCanonicalCourse());
    courseOffering.setTerm(term);
    map.put(courseOffering, getMockSObject());
    expect(salesforceObjectTransformer.extractAssociatedCourseOfferings(anyObject(SObject[].class),
            anyObject(Semester.class), anyObject(Integer.class))).andReturn(map).once();
  }

  private Instructor createInstructor() {
    final Instructor instructor = new Instructor();
    final long time = new Date().getTime();
    instructor.setCalNetUID("calNetUID-" + time);
    return instructor;
  }

  private CourseData createCourse(final Room room) {
    final CourseOffering courseOffering = new CourseOffering();
    courseOffering.setTerm(term);
    final int randomNumber = new Object().hashCode();
    final CanonicalCourse canonicalCourse = new CanonicalCourse(randomNumber, "Name", "Title-" + randomNumber);
    courseOffering.setCanonicalCourse(canonicalCourse);
    courseOffering.setRoom(room);
    courseOffering.setDeptName("Dept-" + randomNumber);
    courseOffering.setParticipationSet(new HashSet<Participation>());
    courseOffering.setMeetingDays(Collections.list(DayOfWeek.Tuesday, DayOfWeek.Thursday));
    courseOffering.setStartTime("1330");
    return courseOffering;
  }

  private Room getRoom(final RoomCapability capability) {
    final Room room = new Room();
    room.setBuilding("building");
    room.setRoomNumber(new Object().hashCode() + "");
    room.setCapability(capability);
    room.setSalesforceID(room.hashCode() + "");
    return room;
  }

  private class MockSalesforceConnectorService implements SalesforceConnectorService {
    @Override
    public QueryResult query(SalesforceQueryType queryType, Map<String, ?> args) {
      return new MockQueryResult();
    }

    @Override
    public SaveResult[] update(SObject... sObjects) {
      return new SaveResult[] { new SaveResult() };
    }

    @Override
    public SaveResult[] create(List<SObject> sObjectList) throws ConnectionException {
      return new SaveResult[] { new SaveResult() };
    }
  }

  private class MockQueryResult extends QueryResult {

    private MockQueryResult() {
    }

    @Override public SObject[] getRecords() {
      return new SObject[] { new SObject() };
    }
  }
}
