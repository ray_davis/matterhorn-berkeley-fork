/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.junit.Before;
import org.junit.Test;
import org.opencastproject.participation.model.Instructor;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;

/**
 * @author John Crossman
 */
public class ResultConverterForInstructorTest extends AbstractResultConverterTest {

  private ResultSet r;

  @Before
  public void before() throws SQLException {
    r = createMock(ResultSet.class);
    expectReturn(r, ColStr.term_yr, "2014");
    expectReturn(r, ColStr.term_cd, "B");
    expectReturn(r, ColInt.course_cntl_num, 99999);
    expectReturn(r, ColStr.ldap_uid);
    expectReturn(r, ColStr.first_name);
    expectReturn(r, ColStr.last_name);
    expectReturn(r, ColStr.email_address);
    expectReturn(r, ColStr.dept_description);
    replay(r);
  }

  @Test
  public void testConvert() throws SQLException {
    final Instructor instructor = new ResultConverterForInstructor().convert(r);
    assertEquals(ColStr.first_name.name(), instructor.getFirstName());
    assertEquals(ColStr.last_name.name(), instructor.getLastName());
    assertEquals(ColStr.email_address.name(), instructor.getEmail());
    assertEquals(ColStr.ldap_uid.name(), instructor.getCalNetUID());
  }

}
