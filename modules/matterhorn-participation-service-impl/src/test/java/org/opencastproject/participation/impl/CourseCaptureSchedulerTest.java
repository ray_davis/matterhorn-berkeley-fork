/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_ACCESS_RIGHTS;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_CONTRIBUTOR;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_CREATOR;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_DESCRIPTION;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_IDENTIFIER;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_IS_PART_OF;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_LICENSE;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_SPATIAL;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_TEMPORAL;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_TITLE;

import org.opencastproject.capture.CaptureParameters;
import org.opencastproject.capture.admin.api.Agent;
import org.opencastproject.capture.admin.api.CaptureAgentStateService;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.metadata.dublincore.DublinCoreValue;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.SignUp;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceFieldInstructor;
import org.opencastproject.scheduler.api.SchedulerException;
import org.opencastproject.scheduler.api.SchedulerService;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.data.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

/**
 * A suite of tests focusing on the preparation and submission of a template {@link DublinCoreCatalog} to the
 * {@link SchedulerService}. Each test embodies the fact that {@link CourseCaptureScheduler} is architecturally
 * an adapter bridging the course offering to Matterhorn events.
 * <p>
 * Coverage:
 * <p><ul>
 * <li>{@link CourseCaptureScheduler#scheduleMatterhornRecordings}
 * <li>{@link CourseCaptureScheduler#createSeries}
 * <li>{@link CourseCaptureScheduler#isKnownAgent}
 * <ul><p>
 */
@RunWith(MockitoJUnitRunner.class)
public class CourseCaptureSchedulerTest {
  private static final String SERIES_ID = "2014B69490";
  private static final String RECORDING_TITLE = "Physics 137A, 002";
  private static final String RECORDING_TITLE_AFTER_REMOVING_SECTION = "Physics 137A";
  private static final String KNOWN_CAPTURE_AGENT_NAME = "giannini-141";
  private static final String PUBLISH_DELAY_DAYS = "7";
  private CourseCaptureScheduler courseCaptureScheduler;
  @Mock private SchedulerService schedulerService;
  @Mock private CaptureAgentStateService captureAgentStateService;
  @Mock private SeriesService seriesService;
  @Mock private DublinCoreCatalog dummyCatalog;
  @Mock private SignUp signUp;
  @Captor private ArgumentCaptor<DublinCoreCatalog> argument;
  @Captor private ArgumentCaptor<Map<String, String>> workflowProperties;

  /**
   * {@link CourseCaptureScheduler#scheduleMatterhornRecordings} transforms course offering data to a template containing
   * episode data. The transformation from {@link CourseOffering} to episode {@link DublinCoreCatalog} is successful if
   * the {@code DublinCoreCatalog} used to invoke the {@link org.opencastproject.series.api.SeriesService} method matches the
   * {@code CourseOffering} data originally passed to the {@code scheduleMatterhornRecordings} method.
   */
  @Test
  public void transformsCourseToEpisodeCatalog() throws Exception {
    final TimeZone tz = TimeZone.getDefault();
    final Long eventId = 99999L;
    Long[] createdIds = new Long[] {eventId};
    CourseOffering courseData = getCourseOfferingSeededWithRequiredFields();
    when(schedulerService.getEventDublinCore(anyLong())).thenReturn(dummyCatalog);
    when(schedulerService.addReccuringEvent(any(DublinCoreCatalog.class), anyMapOf(String.class, String.class))).thenReturn(createdIds);

    courseCaptureScheduler.scheduleMatterhornRecordings(SERIES_ID, courseData, signUp, KNOWN_CAPTURE_AGENT_NAME);

    verify(schedulerService).addReccuringEvent(argument.capture(), anyMapOf(String.class, String.class));
    final DublinCoreCatalog episode = argument.getValue();
    assertThat(episode.getFirst(PROPERTY_TITLE)).isEqualTo(RECORDING_TITLE_AFTER_REMOVING_SECTION);
    assertThat(episode.getFirst(PROPERTY_IS_PART_OF)).isEqualTo(SERIES_ID);
    assertThat(episode.get(PROPERTY_CREATOR)).containsOnly(new DublinCoreValue("Joe Dokes"),new DublinCoreValue("Jane Doe"));
    assertThat(episode.getFirst(PROPERTY_SPATIAL)).isEqualTo(KNOWN_CAPTURE_AGENT_NAME);
    assertThat(episode.getFirst(PROPERTY_LICENSE)).isEqualTo(RecordingAvailability.studentsOnly.getLicense().getSalesforceValue());
    assertThat(episode.getFirst(DublinCoreCatalogImpl.PROPERTY_AGENT_TIMEZONE)).isEqualTo(tz.getID());
    assertThat(episode.getFirst(DublinCoreCatalogImpl.PROPERTY_RECURRENCE)).isEqualTo("FREQ=WEEKLY;BYDAY=TU,TH,SA;BYHOUR=0;BYMINUTE=7");
    assertThat(episode.getFirst(PROPERTY_TEMPORAL)).isEqualTo("start=2014-05-06T00:07:00Z; end=2014-05-10T01:03:00Z; scheme=W3C-DTF;");
  }

  /**
   * {link CourseCaptureScheduler#scheduleMatterhornRecordings} must return at least one episode
   * {@code DublinCoreCatalog} or throw a {@code SchedulerException}.
   */
  @Test(expected = SchedulerException.class)
  public void scheduledZeroEventsResultsInException() throws Exception {
    final int expectedCreatedIds = 0;
    CourseOffering courseData = getCourseOfferingSeededWithRequiredFields();
    Long[] noCreatedIds = new Long[expectedCreatedIds];
    when(schedulerService.addReccuringEvent(any(DublinCoreCatalog.class), anyMapOf(String.class, String.class))).thenReturn(noCreatedIds);
    courseCaptureScheduler.scheduleMatterhornRecordings(SERIES_ID, courseData, signUp, KNOWN_CAPTURE_AGENT_NAME);
  }

  /**
   * {@link CourseCaptureScheduler#scheduleMatterhornRecordings} transforms instructor course capture data to
   * workflow properties. The transformation from {@link SignUp} to {@code Map<String, String>} is successful if
   * the {@code Map<String, String>} used to invoke the {@link SeriesService} method matches the
   * {@code SignUp} data originally passed to the {@code scheduleMatterhornRecordings} method.
   */
  @Test
  public void transformsSignUpToWorkflowProperties() throws Exception {
    final Long eventId = 99999L;
    Long[] createdIds = new Long[] {eventId};
    CourseOffering courseData = getCourseOfferingSeededWithRequiredFields();
    when(schedulerService.getEventDublinCore(anyLong())).thenReturn(dummyCatalog);
    when(schedulerService.addReccuringEvent(any(DublinCoreCatalog.class), anyMapOf(String.class, String.class))).thenReturn(createdIds);

    courseCaptureScheduler.scheduleMatterhornRecordings(SERIES_ID, courseData, signUp, KNOWN_CAPTURE_AGENT_NAME);

    verify(schedulerService).addReccuringEvent(any(DublinCoreCatalog.class), workflowProperties.capture());
    final Map<String, String> wfProperties = workflowProperties.getValue();
    assertThat(wfProperties.get(CaptureParameters.CAPTURE_DEVICE_NAMES)).isEqualTo("audio");
    assertThat(wfProperties.get(CaptureParameters.INGEST_WORKFLOW_DEFINITION)).isEqualTo("lecture-process-publish-ucb-standard");
    assertThat(wfProperties.get(CaptureParameters.WORKFLOW_CONFIG_TRIM_HOLD)).isEqualTo(Boolean.FALSE.toString());
    assertThat(wfProperties.get(CaptureParameters.RECORDING_AVAILABILITY)).isEqualTo(RecordingAvailability.studentsOnly.name());
    assertThat(wfProperties.get(CaptureParameters.DELAY_PUBLISH_BY_DAYS)).isEqualTo(PUBLISH_DELAY_DAYS);
    assertThat(wfProperties.get(CaptureParameters.KEY_PREFIX_WORKFLOW_CONFIG + "." + "mmm")).isEqualTo(Boolean.TRUE.toString());
    assertThat(wfProperties.get(CaptureParameters.KEY_PREFIX_WORKFLOW_CONFIG + "." + "youtube")).isEqualTo(Boolean.TRUE.toString());
    assertThat(wfProperties.get(CaptureParameters.AGENT_NAME)).isEqualTo(KNOWN_CAPTURE_AGENT_NAME);
    assertThat(wfProperties.get(SchedulerService.WORKFLOW_OPERATION_KEY_SCHEDULE_LOCATION)).isEqualTo(KNOWN_CAPTURE_AGENT_NAME);
  }

  /**
   * {@link CourseCaptureScheduler#createSeries} throws a {@code SchedulerException} if the series already exists.
   */
  @Test(expected = SchedulerException.class)
  public void createDuplicateSeriesIsNotAllowed() throws Exception  {
    CourseOffering courseData = getCourseOfferingSeededWithRequiredFields();
    when(seriesService.getSeries(SERIES_ID)).thenReturn(dummyCatalog);
    courseCaptureScheduler.createSeries(courseData, null, null);
  }

  /**
   * {@link CourseCaptureScheduler#createSeries} creates a series if it doesn't already exist.
   */
  @Test
  public void createSeries() throws Exception {
    CourseOffering courseData = getCourseOfferingSeededWithRequiredFields();
    when(seriesService.getSeries(SERIES_ID)).thenReturn(null);
    when(seriesService.updateSeries(any(DublinCoreCatalog.class))).thenReturn(dummyCatalog);
    RecordingAvailability availability = RecordingAvailability.publicCreativeCommons;

    final DublinCoreCatalog series = courseCaptureScheduler.createSeries(courseData, availability, null);
    assertThat(series).isNotNull();
  }

  /**
   * {@link CourseCaptureScheduler#createSeries} transforms course offering data to series catalog data. The
   * transformation from {@link CourseOffering} to series {@link DublinCoreCatalog} is successful if the
   * {@code DublinCoreCatalog} used to invoke the {@link org.opencastproject.series.api.SeriesService} method matches the
   * {@code CourseOffering} data originally passed to the {@code createSeries} method.
   */
  @Test
  public void transformsCourseToSeriesCatalog() throws Exception {
    CourseOffering courseData = getCourseOfferingSeededWithRequiredFields();
    RecordingAvailability availability = RecordingAvailability.studentsOnly;
    when(seriesService.getSeries(SERIES_ID)).thenReturn(null);

    courseCaptureScheduler.createSeries(courseData, availability, null);
    verify(seriesService).updateSeries(argument.capture());
    final DublinCoreCatalog series = argument.getValue();
    assertThat(series.getFirst(PROPERTY_IDENTIFIER)).isEqualTo(courseData.getCourseOfferingId());
    assertThat(series.getFirst(PROPERTY_TITLE)).isEqualTo(courseData.getSeriesTitle());
    assertThat(series.getFirst(PROPERTY_DESCRIPTION)).isEqualTo(courseData.getSeriesDescription());
    assertThat(series.get(PROPERTY_CONTRIBUTOR)).containsOnly(new DublinCoreValue("Joe Dokes"),new DublinCoreValue("Jane Doe"));
    assertThat(series.getFirst(PROPERTY_LICENSE)).isEqualTo(availability.getLicense().getSalesforceValue());
    assertThat(series.getFirst(PROPERTY_ACCESS_RIGHTS)).isEqualTo(availability.getRecordingAccessRights().name());
  }

  /**
   * Return true if the agent is a known agent.
   */
  @Test
  public void RecognizesKnownAgent() {
    final String agentName = "giannini-141";
    Map<String, Agent> agents = new HashMap<String, Agent>();
    Agent anAgent = mock(Agent.class);
    agents.put(KNOWN_CAPTURE_AGENT_NAME, anAgent);
    Properties configuration = new Properties();
    configuration.put(CaptureParameters.AGENT_NAME, KNOWN_CAPTURE_AGENT_NAME);
    when(anAgent.getConfiguration()).thenReturn(configuration);
    when(captureAgentStateService.getKnownAgents()).thenReturn(agents);

    final boolean isKnownAgent = courseCaptureScheduler.isKnownAgent(agentName);
    assertThat(isKnownAgent).isTrue();
  }

  /**
   * Return false if the agent is not a known agent.
   */
  @Test
  public void doesNotRecognizeUnknownAgent() {
    final String unknownAgent = "dwinelle-8";

    //Just so we have a collection of known agents to test against
    Map<String, Agent> agents = new HashMap<String, Agent>();
    Agent anAgent = mock(Agent.class);
    agents.put(KNOWN_CAPTURE_AGENT_NAME, anAgent);
    Properties configuration = new Properties();
    configuration.put(CaptureParameters.AGENT_NAME, KNOWN_CAPTURE_AGENT_NAME);
    when(anAgent.getConfiguration()).thenReturn(configuration);
    when(captureAgentStateService.getKnownAgents()).thenReturn(agents);

    final boolean isKnownAgent = courseCaptureScheduler.isKnownAgent(unknownAgent);
    assertThat(isKnownAgent).isFalse();
  }

  @Before
  public void instantiateCourseCaptureSchedulerAndInjectCollaborators() throws Exception {
    when(signUp.getRecordingAvailability()).thenReturn(RecordingAvailability.studentsOnly);
    when(signUp.getPublishDelayDays()).thenReturn(Integer.parseInt(PUBLISH_DELAY_DAYS));
    when(signUp.getRecordingType()).thenReturn(RecordingType.audioOnly);
    courseCaptureScheduler = new CourseCaptureScheduler(schedulerService, seriesService, captureAgentStateService);
  }

  private CourseOffering getCourseOfferingSeededWithRequiredFields() {
    final CourseOffering courseData = new CourseOffering();
    courseData.setCourseOfferingId(SERIES_ID);
    courseData.setCanonicalCourse(new CanonicalCourse(69490, RECORDING_TITLE, "Quantum Physics"));
    final Term term = Term.construct(Semester.Spring, 2014, "2014-05-05", "2014-05-09", null);
    courseData.setTerm(term);
    courseData.setRoom(new Room("salesforceID", "giannini", "141", RoomCapability.screencastAndVideo));
    courseData.setMeetingDays(new ArrayList<DayOfWeek>(Arrays.asList(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday)));
    courseData.setStartTime("1700");
    courseData.setEndTime("1800");
    final Instructor i1 = new Instructor("calNetUID1");
    i1.setFirstName("Jane");
    i1.setLastName("Doe");
    final Participation p1 = new Participation(SalesforceFieldInstructor.Instructor_1, i1, false);
    final Instructor i2 = new Instructor("calNetUID2");
    i2.setFirstName("Joe");
    i2.setLastName("Dokes");
    courseData.setCapturePreferences(new CapturePreferences(RecordingType.audioOnly, RecordingAvailability.studentsOnly, 1));
    final Participation p2 = new Participation(SalesforceFieldInstructor.Instructor_2, i2, true);
    courseData.setParticipationSet(Collections.set(p1, p2));
    return courseData;
  }
}
