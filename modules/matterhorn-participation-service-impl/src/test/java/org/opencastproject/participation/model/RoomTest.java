/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author John Crossman
 */
public class RoomTest {

  @Test
  public void testEquals() {
    final Room room1 = new Room("salesforceID-1", "building-X", "1", null);
    final Room room2 = new Room("salesforceID-2", "BUILDING-X", "  1", null);
    assertEquals(room1, room2);
  }

  @Test
  public void testEqualsPaddedWithZeroes() {
    final Room room1 = new Room("salesforceID-1", "building-X", "0001", null);
    final Room room2 = new Room("salesforceID-2", "BUILDING-X", "1", null);
    assertEquals(room1, room2);
  }

  @Test
  public void testContains() {
    final Set<Room> set = new HashSet<Room>();
    final Room clone = new Room();
    for (int index = 1; index < 5; index++) {
      final Room room = new Room("salesforceID-" + index, "BUILDING-" + (index * 10), (index * 100) + "", RoomCapability.screencast);
      set.add(room);
      if (index == 3) {
        clone.setBuilding(room.getBuilding().toLowerCase());
        clone.setRoomNumber(room.getRoomNumber());
      }
    }
    assertTrue(set.contains(clone));
  }

  @Test
  public void testNotEqualsBuilding() {
    final Room room1 = new Room("salesforceID-1", "building-X", "1", null);
    final Room room2 = new Room("salesforceID-1", "building-Y", "1", null);
    assertFalse(room1.equals(room2));
  }

  @Test
  public void testEqualsBuildingWhenHasSpace() {
    final Room room1 = new Room(null, " DONNER LAB", "155 ", null);
    final Room room2 = new Room("salesforceID-1", "Donner-Lab ", " 155", RoomCapability.screencast);
    assertTrue(room1.equals(room2));
    //
    final Set<Room> set = new HashSet<Room>();
    set.add(room1);
    assertTrue(set.contains(room2));
  }

  @Test
  public void testNotEqualsRoomNumber() {
    final Room room1 = new Room("salesforceID-1", "building-X", "1", null);
    final Room room2 = new Room("salesforceID-1", "building-X", "1a", null);
    assertFalse(room1.equals(room2));
  }

  @Test
  public void testNotEqualsWhenNullRoomNumber() {
    final Room room1 = new Room("salesforceID-1", "building-X", "1", null);
    final Room room2 = new Room("salesforceID-1", "building-X", null, null);
    assertFalse(room1.equals(room2));
  }

  @Test
  public void testTrimAndFixRoomNumber() {
    for (final boolean useConstructor : new boolean[] {true, false}) {
      assertEquals(null, createRoom("building", null, useConstructor).getRoomNumber());
      assertEquals(null, createRoom("building", "  ", useConstructor).getRoomNumber());
      assertEquals("101", createRoom("building", " 101 ", useConstructor).getRoomNumber());
      assertEquals("101", createRoom("building", " 101.0", useConstructor).getRoomNumber());
      assertEquals("101", createRoom("building", " 101.00 ", useConstructor).getRoomNumber());
      assertEquals("101 A", createRoom("building", " 101 A ", useConstructor).getRoomNumber());
      assertEquals("Auditorium", createRoom("building", "Auditorium", useConstructor).getRoomNumber());
    }
  }

  private Room createRoom(final String building, final String roomNumber, final boolean useConstructor) {
    final Room room;
    if (useConstructor) {
      room = new Room(null, building, roomNumber, null);
    } else {
      room = new Room();
      room.setBuilding(building);
      room.setRoomNumber(roomNumber);
    }
    return room;
  }

}
