/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation;

import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.env.EProperties;
import org.opencastproject.util.env.EnvironmentUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

/**
 * @author John Crossman
 */
public class UnitTestUtils {

  public static Properties getSalesforceConnectionProperties() throws IOException {
    final File file = EnvironmentUtil.getFileUnderFelixHome(
            "etc/services/org.opencastproject.salesforce.SalesforceConnectorServiceImpl.properties");
    final Properties p = new Properties();
    p.load(new FileInputStream(file));
    return EnvironmentUtil.createEProperties(p, true);
  }

  public static DatabaseCredentials getTestCatalogCredentials() throws IOException {
    final File file = EnvironmentUtil.getFileUnderFelixHome(
            "etc/services/org.opencastproject.participation.impl.CourseManagementServiceImpl.properties");
    final Properties p = new Properties();
    p.load(new FileInputStream(file));
    final EProperties eProperties = EnvironmentUtil.createEProperties(p, true);
    return CourseUtils.getCatalogCredentials(eProperties);
  }

  public static CourseOffering createRandomCourseOffering(final Term term, final Room room) {
    final int ccn = (int) new Date().getTime() % 10000;
    final CanonicalCourse cc = new CanonicalCourse(ccn, "Course Name", "Course Title");
    final CourseOffering c = new CourseOffering();
    c.setCanonicalCourse(cc);
    c.setTerm(term);
    c.setSection("002");
    c.setMeetingDays(Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday));
    c.setStartTime("1330");
    c.setEndTime("1500");
    c.setCourseOfferingId(new Date().getTime() * Math.random() + "");
    final List<Participation> list = Collections.list(createRandomParticipation(), createRandomParticipation());
    c.setParticipationSet(new HashSet<Participation>(list));
    c.setSalesforceID("salesforceID-" + c.hashCode());
    c.setRoom(room);
    return c;
  }

  private static Participation createRandomParticipation() {
    final Instructor i = new Instructor();
    i.setCalNetUID(new Object().hashCode() + "");
    i.setDepartment("Dept " + Math.random());
    i.setEmail(i.getCalNetUID() + "@berkeley.edu");
    i.setFirstName("First name of " + i.getCalNetUID());
    i.setLastName("Last name of " + i.getCalNetUID());
    return new Participation(null, i, false);
  }

  public static Term getFall2014() {
    return Term.construct(Semester.Fall, 2014, "2014-08-20", "2014-12-10", null);
  }

  public static Room getRoom(String building, String roomNumber) {
    final Room room = new Room();
    room.setBuilding(building);
    room.setRoomNumber(roomNumber);
    room.setSalesforceID(room.hashCode() + "");
    return room;
  }
}
