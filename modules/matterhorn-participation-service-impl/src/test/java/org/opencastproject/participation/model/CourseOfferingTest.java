/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.opencastproject.metadata.dublincore.DublinCoreValue;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.sforce.soap.partner.sobject.SObject;

import org.apache.commons.lang.WordUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public class CourseOfferingTest {
  
  private CourseOffering courseOffering;
  private Instructor instructor1;
  private Instructor instructor2;
  
  private static final String COURSE_DEPARTMENT = "Physics";
  private static final String COURSE_CATALOG_ID = "137A";
  private static final String COURSE_SECTION = "002";
  private static final Semester SEMESTER_NAME = Semester.Spring;
  private static final int SEMESTER_YEAR = 2014;
  private static final String COURSE_NAME = COURSE_DEPARTMENT + " " + COURSE_CATALOG_ID + ", " + COURSE_SECTION;
  private static final String COURSE_DESCRIPTION = "Quantum Mechanics";

  @Test
  public void testEquals() {
    final Term term = courseOffering.getTerm();
    //
    final CourseData courseData = new CourseData();
    final CanonicalCourse cc = new CanonicalCourse(courseOffering.getCanonicalCourse().getCcn(), null, null);
    courseData.setCanonicalCourse(cc);
    final Term termClone = Term.construct(term.getSemester(), term.getYear(), term.getStartDate(), term.getEndDate(), null);
    courseData.setTerm(termClone);
    // Assert
    assertEquals(courseOffering, courseData);
    // Test in context of a Map
    final Map<CourseData, SObject> map = new HashMap<CourseData, SObject>();
    map.put(courseOffering, new SObject());
    assertTrue(map.containsKey(courseData));
  }

  @Test
  public void testNotEquals() {
    final Term term = courseOffering.getTerm();
    final CourseData courseData = new CourseData();
    courseData.setCanonicalCourse(courseOffering.getCanonicalCourse());
    courseData.setTerm(Term.construct(term.getSemester(), term.getYear() + 1, term.getStartDate(), term.getEndDate(), null));
    assertFalse(courseOffering.equals(courseData));
  }
  
  @Test
  public void testGetSeriesTitle() {
    assertThat(courseOffering.getSeriesTitle()).isEqualTo(COURSE_NAME + " - " + SEMESTER_NAME + " " +  SEMESTER_YEAR); 
  }
  
  @Test
  public void testGetRecordingTitle() {
    assertThat(courseOffering.getRecordingTitle()).isEqualTo(COURSE_DEPARTMENT + " " + COURSE_CATALOG_ID); 
  }
  
  @Test
  public void testGetInstructors() {
    assertThat(courseOffering.getInstructors()).containsOnly(valueOf(instructor1), valueOf(instructor2));
  }
  
  @Test
  public void testGetSeriesDescription() {
    assertThat(courseOffering.getSeriesDescription()).isEqualTo(COURSE_DESCRIPTION + " - " + valueOf(instructor1).getValue() + ", " + valueOf(instructor2).getValue());
  }
  

  @Before
  public void buildCourseOffering() {
    courseOffering = new CourseOffering();
    courseOffering.setSalesforceID("1234567");
    courseOffering.setCourseOfferingId("2013B54321");
    courseOffering.setCanonicalCourse(new CanonicalCourse(12340, COURSE_NAME, COURSE_DESCRIPTION));

    final Term term = Term.construct(SEMESTER_NAME, SEMESTER_YEAR, "2014-08-22", "2014-12-05", null);
    courseOffering.setTerm(term);
    
    courseOffering.setSection("002");

    List<DayOfWeek> meetingDays = new LinkedList<DayOfWeek>();
    meetingDays.add(DayOfWeek.Tuesday);
    meetingDays.add(DayOfWeek.Thursday);
    courseOffering.setMeetingDays(meetingDays);

    courseOffering.setStartTime("0900");
    courseOffering.setEndTime("1000");

    Room room = new Room();
    room.setBuilding("Pimentel");
    room.setCapability(RoomCapability.screencastAndVideo);
    room.setSalesforceID("123456");
    room.setRoomNumber("1");
    courseOffering.setRoom(room);

    courseOffering.setScheduled(false);
    courseOffering.setStageOfApproval(StageOfApproval.approved);
    courseOffering.setStageOfLifecycle(SalesforceProjectLifecycle.prospecting);

    Participation participation1 = new Participation();
    participation1.setApproved(true);
    instructor1 = new Instructor();
    instructor1.setCalNetUID("212386");
    instructor1.setDepartment("ETS");
    instructor1.setSalesforceID("98876554");
    instructor1.setEmail("posey@bekeley.edu");
    instructor1.setFirstName("Buster");
    instructor1.setLastName("Posey");
    instructor1.setRole("Faculty");
    participation1.setInstructor(instructor1);
    //
    Participation participation2 = new Participation();
    participation2.setApproved(false);
    instructor2 = new Instructor();
    instructor2.setCalNetUID("322587");
    instructor2.setDepartment("ETS");
    instructor2.setSalesforceID("1234567");
    instructor2.setEmail("lincecum@bekeley.edu");
    instructor2.setFirstName("Tim");
    instructor2.setLastName("Lincecum");
    instructor2.setRole("Faculty");
    participation2.setInstructor(instructor2);
    //
    final Set<Participation> participationList = new HashSet<Participation>();
    participationList.add(participation1);
    participationList.add(participation2);
    courseOffering.setParticipationSet(participationList);

    CapturePreferences capPrefs = new CapturePreferences();
    capPrefs.setRecordingType(RecordingType.videoAndScreencast);
    capPrefs.setRecordingAvailability(RecordingAvailability.publicCreativeCommons);
    capPrefs.setDelayPublishByDays(0);
    courseOffering.setCapturePreferences(capPrefs);
  }
  
  private DublinCoreValue valueOf(Instructor instructor) {
    return new DublinCoreValue(WordUtils.capitalizeFully(instructor.getFirstName()) + ' ' + WordUtils.capitalizeFully(instructor.getLastName()));
  }

}
