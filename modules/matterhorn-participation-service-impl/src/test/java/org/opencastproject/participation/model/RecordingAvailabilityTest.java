/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.junit.Test;
import org.opencastproject.security.api.RecordingAccessRights;

import java.util.Set;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class RecordingAvailabilityTest {

  @Test
  public void testGetRecordingAccessRights() {
    assertSame(RecordingAccessRights.publicAccessRights,
            RecordingAvailability.publicCreativeCommons.getRecordingAccessRights());
    assertSame(RecordingAccessRights.publicAccessRights,
            RecordingAvailability.publicNoRedistribute.getRecordingAccessRights());
    assertSame(RecordingAccessRights.studentsOnlyAccessRights,
            RecordingAvailability.studentsOnly.getRecordingAccessRights());
  }

  @Test
  public void testGetLicense() {
    assertSame(License.creativeCommons, RecordingAvailability.publicCreativeCommons.getLicense());
    assertSame(License.allRightsReserved, RecordingAvailability.publicNoRedistribute.getLicense());
    assertSame(License.allRightsReserved, RecordingAvailability.studentsOnly.getLicense());
  }

  @Test
  public void testGetSalesforceValue() {
    assertTrue(RecordingAvailability.publicCreativeCommons.getSalesforceValue().contains("Public"));
    assertTrue(RecordingAvailability.publicNoRedistribute.getSalesforceValue().contains("Public"));
    assertTrue(RecordingAvailability.studentsOnly.getSalesforceValue().contains("Students"));
  }

  @Test
  public void testGetDistribution1() {
    final Set<PublicationChannel> distribution = RecordingAvailability.publicCreativeCommons.getDistribution();
    assertTrue(distribution.contains(PublicationChannel.Matterhorn));
    assertTrue(distribution.contains(PublicationChannel.YouTube));
  }

  @Test
  public void testGetDistribution2() {
    final Set<PublicationChannel> distribution = RecordingAvailability.publicNoRedistribute.getDistribution();
    assertTrue(distribution.contains(PublicationChannel.Matterhorn));
    assertTrue(distribution.contains(PublicationChannel.YouTube));
  }

  @Test
  public void testGetDistribution3() {
    final Set<PublicationChannel> distribution = RecordingAvailability.studentsOnly.getDistribution();
    assertTrue(distribution.contains(PublicationChannel.Matterhorn));
    assertTrue(distribution.contains(PublicationChannel.YouTube));
  }

}
