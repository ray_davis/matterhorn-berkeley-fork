/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import org.opencastproject.composer.api.ComposerService;
import org.opencastproject.composer.api.EncodingProfile;
import org.opencastproject.job.api.Job;
import org.opencastproject.job.api.JobContext;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElement;
import org.opencastproject.mediapackage.MediaPackageElementFlavor;
import org.opencastproject.mediapackage.MediaPackageElementParser;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.mediapackage.Track;
import org.opencastproject.workflow.api.AbstractWorkflowOperationHandler;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationInstance;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workflow.api.WorkflowOperationResult.Action;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The workflow definition for handling "publish" operations
 */
public class MergeVideosWorkflowOperationHandler extends AbstractWorkflowOperationHandler {

  /** The logging facility */
  private final Logger logger = LoggerFactory.getLogger(getClass());

  /** The composer service */
  private ComposerService composerService;

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#getConfigurationOptions()
   */
  @Override
  public SortedMap<String, String> getConfigurationOptions() {
    final SortedMap<String, String> options = new TreeMap<String, String>();
    options.put(KEY_SOURCE_FLAVORS, "Two videos we will merge into a side-by-side composition, intended for YouTube publication");
    options.put(KEY_TARGET_FLAVOR, "Flavor to be used by YouTube publication operation");
    options.put(KEY_ENCODING_PROFILE, "The ffmpeg profile used to merge videos");
    options.put(KEY_TARGET_TAG, "Tag used to identify media prepared for YouTube publication");
    return options;
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#start(org.opencastproject.workflow.api.WorkflowInstance,
   *      JobContext)
   */
  public WorkflowOperationResult start(final WorkflowInstance workflowInstance, final JobContext context)
      throws WorkflowOperationException {
    logger.info("Begin method: start()");
    final MediaPackage mediaPackage = workflowInstance.getMediaPackage();

    final Track presenterTrack = getTrack(mediaPackage, getFlavor("presenter/trimmed"));
    final boolean presenterTrackHasVideo = (presenterTrack != null) && presenterTrack.hasVideo();
    
    // Audio-only for YouTube
    final Track presenterTrackAudioOnly = getTrack(mediaPackage, getFlavor("presenter/AudioOnly"));
    final boolean presenterTrackHasAudioOnly = (presenterTrackAudioOnly != null) && presenterTrack.hasAudio();
    
    final Track presentationTrack = getTrack(mediaPackage, getFlavor("presentation/trimmed"));
    final boolean presentationTrackHasVideo = (presentationTrack != null) && presentationTrack.hasVideo();
    long totalTimeInQueue = 0;
    final Track track;
    final WorkflowOperationInstance currentOperation = workflowInstance.getCurrentOperation();
    if (presenterTrackHasVideo && presentationTrackHasVideo) {
      logger.info("presenterTrack: " + getPath(presenterTrack) + "; presentationTrack: " + getPath(presentationTrack));
      final String encodingProfileName = getEncodingProfileName(currentOperation);
      final EncodingProfile encodingProfile = composerService.getProfile(encodingProfileName);
      final Job job = mergeVideoFiles(presenterTrack, presentationTrack, encodingProfile);
      final String payload = job.getPayload();
      if (payload.length() == 0) {
        throw new WorkflowOperationException("Empty payload of job: "
            + ToStringBuilder.reflectionToString(job, ToStringStyle.MULTI_LINE_STYLE));
      } else {
        try {
          track = (Track) MediaPackageElementParser.getFromXml(payload);
          logger.info("Result of video merger: " + getPath(track));
        } catch (final MediaPackageException e) {
          throw new WorkflowOperationException("Failed to get XML from payload of job: "
              + ToStringBuilder.reflectionToString(job, ToStringStyle.MULTI_LINE_STYLE), e);
        }
        totalTimeInQueue += job.getQueueTime();
      }
    } else if (presenterTrackHasVideo) {
      logger.info("No video merge necessary. Presenter track is the only video.");
      track = (Track) presenterTrack.clone();
    } else if (presentationTrackHasVideo) {
      logger.info("No video merge necessary. Presentation track is the only video.");
      track = (Track) presentationTrack.clone();
    } else if (presenterTrackHasAudioOnly) {     
      logger.info("No video tracks found, using audio only.");
      track = (Track) presenterTrackAudioOnly.clone();
    } 
    else {
      logger.info("No video merge necessary. No audio or video tracks found.");
      track = null;
    }
    if (track != null) {
      logger.info("Apply target tags and flavors to: " + getPath(track));
      applyTargetTagsAndFlavors(mediaPackage, track, currentOperation);
    }
    final WorkflowOperationResult result = createResult(mediaPackage, Action.CONTINUE, totalTimeInQueue);
    logger.info("Finish method: start()");
    return result;
  }

  /**
   * @param mediaPackage Never null
   * @param flavor Never null
   * @return Null when no such track found
   */
  private Track getTrack(final MediaPackage mediaPackage, final MediaPackageElementFlavor flavor) {
    final Track track;
    final Track[] tracks = mediaPackage.getTracks(flavor);
    final int length = (tracks == null) ? 0 : tracks.length;
    switch (length) {
      case 0:
        track = null;
        break;
      default:
        track = tracks[0];
        break;
    }
    return track;
  }

  private String getPath(final MediaPackageElement track) {
    return track.getURI() == null ? null : track.getURI().getPath();
  }

  private void applyTargetTagsAndFlavors(final MediaPackage mediaPackage, final Track track,
                                         final WorkflowOperationInstance currentOperation)
      throws WorkflowOperationException {
    final String targetTagName = getConfiguration(currentOperation, KEY_TARGET_TAG);
    final String targetFlavorName = getTargetFlavorName(currentOperation);
    logger.info("Apply target tag '" + targetTagName + "' and flavor '"
        + targetFlavorName + "' to " + getPath(track));
    if (targetTagName != null) {
      track.addTag(targetTagName);
    }
    if (targetFlavorName == null) {
      throw new WorkflowOperationException("Missing target flavor configuration.");
    }
    final MediaPackageElementFlavor targetFlavor = getFlavor(targetFlavorName);
    track.setFlavor(targetFlavor);
    mediaPackage.add(track);
  }

  /**
   * @see ComposerService#mergeVideoFiles(org.opencastproject.mediapackage.Track, org.opencastproject.mediapackage.Track, String)
   * @param presenterTrack Never null
   * @param presentationTrack Never null
   * @param encodingProfile Never null; instructs which ffmpeg command variation to use.
   * @return job performed
   * @throws WorkflowOperationException when {@link #waitForStatus(org.opencastproject.job.api.Job...)} returns false
   */
  private Job mergeVideoFiles(final Track presenterTrack, final Track presentationTrack, final EncodingProfile encodingProfile) throws WorkflowOperationException {
    try {
      logger.info("Merge video files with encoding profile: " + encodingProfile);
      final Job job = composerService.mergeVideoFiles(presenterTrack, presentationTrack, encodingProfile.getIdentifier());
      // Wait for the jobs to return
      final boolean success = waitForStatus(job).isSuccess();
      if (!success) {
        throw new WorkflowOperationException("Failed encoding on job: " + ToStringBuilder.reflectionToString(job, ToStringStyle.MULTI_LINE_STYLE));
      }
      return job;
    } catch (final Exception e) {
      throw new WorkflowOperationException("Failed perform video overlay using encodingProfile '" + encodingProfile 
          + "'.\nPresenter track: " + ToStringBuilder.reflectionToString(presenterTrack, ToStringStyle.MULTI_LINE_STYLE)
          + "'Presentation track: " + ToStringBuilder.reflectionToString(presentationTrack, ToStringStyle.MULTI_LINE_STYLE), e);
    }
  }

  /**
   * Callback for the OSGi declarative services configuration.
   *
   * @param composerService
   *          the local composer service
   */
  protected void setComposerService(final ComposerService composerService) {
    this.composerService = composerService;
  }

}
