/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import org.opencastproject.job.api.JobContext;
import org.opencastproject.mediapackage.Catalog;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElements;
import org.opencastproject.metadata.dublincore.DCMIPeriod;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.metadata.dublincore.EncodingSchemeUtils;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workflow.api.WorkflowOperationResult.Action;
import org.opencastproject.workspace.api.Workspace;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;


/**
 * Pauses or continues a workflow based on the episode catalog's 
 * dcterms:available start date.
 * 
 * @author Fernando Alvarez
 */
public class PublishDelayedWorkflowOperationHandler extends ResumableWorkflowOperationHandlerBase {
  
  private static final Logger logger = LoggerFactory.getLogger(PublishDelayedWorkflowOperationHandler.class);
  
  /** The workflow operation property that stores the publish date time, as milliseconds since 1970 */
  public static final String WORKFLOW_OPERATION_KEY_SCHEDULE_PUBLISH_DELAY = "schedule.publishDelay";

  // There is no special UI; we use the Delay tab in Recordings
  // However, this value cannot be null
  public static final String UI_RESOURCE_PATH = "";

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.handler.ResumableWorkflowOperationHandlerBase#activate(org.osgi.service.component.ComponentContext)
   */
  @Override
  public void activate(ComponentContext componentContext) {
    super.activate(componentContext);

    // Set the operation's action link title
    setHoldActionTitle("Cancel Publish Delay");

    // Add the ui piece that displays the schedule information
    registerHoldStateUserInterface(UI_RESOURCE_PATH);
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.handler.ResumableWorkflowOperationHandlerBase#start(org.opencastproject.workflow.api.WorkflowInstance, JobContext)
   */
  
  // It would be a lot easier (most of this code goes away) if we used 
  // org.opencastproject.workflow.config.delayPublishByDays to determine if the
  // workflow should be paused. However, this would require synchronizing this
  // second piece of data with the actual publish delay date (i.e. the value 
  // used in the current code) which is still required in order to actually
  // publish the recording on the desired date. 
  //
  // That said, both findDublinCoreCatalog and parseDublinCoreCatalog should 
  // really be public methods in some utility class.
  
  @Override
  public WorkflowOperationResult start(WorkflowInstance workflowInstance, JobContext context) throws WorkflowOperationException {
    logger.debug("Running publish-delayed operation");
    final DublinCoreCatalog dc = findDublinCoreCatalog(workflowInstance.getMediaPackage());
    final Calendar publishDate = Calendar.getInstance();
    
    if (StringUtils.isBlank(dc.getFirst(DublinCore.PROPERTY_AVAILABLE))) {
      publishDate.setTime(new Date());
    } else {
      // Note dcterms:available is a period, and it's start date has a 00:00 time
      DCMIPeriod availablePeriod = EncodingSchemeUtils.decodePeriod(dc.getFirst(DublinCore.PROPERTY_AVAILABLE));
      publishDate.setTime(availablePeriod.getStart());
    }
    logger.debug("Recording {} available as of {}", dc.getFirst(DublinCore.PROPERTY_TITLE), publishDate.toString());
    
    // The UI will pick up the publish date from this value
    workflowInstance.setConfiguration(WORKFLOW_OPERATION_KEY_SCHEDULE_PUBLISH_DELAY, Long.toString(publishDate.getTime().getTime()));
    
    final Calendar now = Calendar.getInstance();
    now.setTime(new Date());
    
    // The setting of the WorkflowOperationResult object determine how the workflow will be processed
    final WorkflowOperationResult result;
    if (publishDate.after(now)) {
      result = createResult(Action.PAUSE);
      logger.debug("Publish will be delayed");
    } else {
      result = createResult(Action.CONTINUE);
      logger.debug("Publish will not be delayed");
    }
    result.setAllowsContinue(true);
    result.setAllowsAbort(true);
    return result;
  }
  
  public DublinCoreCatalog findDublinCoreCatalog(MediaPackage mediaPackage) throws WorkflowOperationException {
    DublinCoreCatalog dc = null;

    // There could be multiple catalogs, one per episode; we'll assume the last
    // is the one we want
    // TODO is there a better way to get the DublinCoreCatalog from a media package?
    for (Catalog catalog : mediaPackage.getCatalogs(MediaPackageElements.EPISODE)) {
      dc = parseDublinCoreCatalog(catalog, workspace);
    }
    if (dc == null) {
      throw new WorkflowOperationException("The media package does not contain a Dublin Core Catatlog");
    }
    return dc;
  }

  private DublinCoreCatalog parseDublinCoreCatalog(Catalog catalog, Workspace workspace) {
    InputStream is = null;
    try {
      File dcFile = workspace.get(catalog.getURI());
      is = new FileInputStream(dcFile);
      return new DublinCoreCatalogImpl(is);
    } catch (Exception e) {
      logger.error("Error loading Dublin Core metadata: {}", e.getMessage());
    } finally {
      IOUtils.closeQuietly(is);
    }
    return null;
  }
  
  // Inject reference to Workspace service
  private Workspace workspace;
  public void setWorkspace(Workspace workspace) {
    this.workspace = workspace;
  }

}
