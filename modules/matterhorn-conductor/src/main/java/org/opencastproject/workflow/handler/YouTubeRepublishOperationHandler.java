/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import org.opencastproject.google.youtube.VideoPublished;
import org.opencastproject.job.api.Job;
import org.opencastproject.job.api.JobContext;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElement;
import org.opencastproject.mediapackage.MediaPackageElementFlavor;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.mediapackage.MediaPackageReference;
import org.opencastproject.mediapackage.identifier.Id;
import org.opencastproject.google.youtube.YouTubeAPIVersion3Service;
import org.opencastproject.google.youtube.YouTubeUtils;
import org.opencastproject.search.api.SearchException;
import org.opencastproject.search.api.SearchQuery;
import org.opencastproject.search.api.SearchResult;
import org.opencastproject.search.api.SearchService;
import org.opencastproject.workflow.api.AbstractWorkflowOperationHandler;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workflow.api.WorkflowOperationResult.Action;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.workspace.api.Workspace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Workflow operation for handling "republish" operations
 */
public final class YouTubeRepublishOperationHandler extends AbstractWorkflowOperationHandler {

  /** Logging facility */
  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  /** The configuration options for this handler */
  private static final SortedMap<String, String> CONFIG_OPTIONS;

  /** The configuration options */
  private static final String OPT_SOURCE_FLAVORS = "source-flavors";
  private static final String OPT_SOURCE_TAGS = "source-tags";
  private static final String OPT_MERGE = "merge";
  private static final String OPT_EXISTING_ONLY = "existing-only";

  /** The search service */
  private SearchService searchService;

  /** workspace instance */
  private YouTubeWorkspace youTubeWorkspace = null;

  static {
    CONFIG_OPTIONS = new TreeMap<String, String>();
    CONFIG_OPTIONS.put(OPT_SOURCE_FLAVORS, "Republish any mediapackage elements with one of these flavors");
    CONFIG_OPTIONS.put(OPT_SOURCE_TAGS, "Republish only mediapackage elements that are tagged with one of these tags");
    CONFIG_OPTIONS.put(OPT_MERGE, "Merge with existing published data");
    CONFIG_OPTIONS.put(OPT_EXISTING_ONLY, "Republish only if it can be merged with or replace existing published data");
  }

  /**
   * {@inheritDoc}
   *
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#getConfigurationOptions()
   */
  @Override
  public SortedMap<String, String> getConfigurationOptions() {
    return CONFIG_OPTIONS;
  }

  /**
   * {@inheritDoc}
   *
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#start(org.opencastproject.workflow.api.WorkflowInstance,
   *      JobContext)
   */
  public WorkflowOperationResult start(final WorkflowInstance workflowInstance, final JobContext context)
          throws WorkflowOperationException {
    final MediaPackage mediaPackage = workflowInstance.getMediaPackage();

    // The flavors of the elements that are to be published
    final Set<MediaPackageElementFlavor> flavors = new HashSet<MediaPackageElementFlavor>();

    // Check which flavors have been configured
    final String configuredFlavors = workflowInstance.getCurrentOperation().getConfiguration(OPT_SOURCE_FLAVORS);
    if (StringUtils.trimToNull(configuredFlavors) == null) {
      logger.warn("No flavors have been specified, republishing all media package elements");
      for (final MediaPackageElement e : mediaPackage.elements()) {
        flavors.add(e.getFlavor());
      }
    } else {
      for (final String flavor : asList(configuredFlavors)) {
        flavors.add(MediaPackageElementFlavor.parseFlavor(flavor));
      }
    }

    // Apply tags and flavors to the current mediapackage
    final MediaPackage filteredMediaPackage;
    try {
      // Get the configured tags
      final String configuredTags = workflowInstance.getCurrentOperation().getConfiguration(OPT_SOURCE_TAGS);
      filteredMediaPackage = filterMediaPackage(mediaPackage, flavors, asList(configuredTags));
    } catch (MediaPackageException e) {
      throw new WorkflowOperationException("Error filtering media package", e);
    }

    // If merge, load current mediapackage from search service
    final MediaPackage publishedMediaPackage;
    // Merge or replace?
    final boolean merge = Boolean.parseBoolean(workflowInstance.getCurrentOperation().getConfiguration(OPT_MERGE));
    final boolean onlyExisting = Boolean.parseBoolean(workflowInstance.getCurrentOperation().getConfiguration(OPT_EXISTING_ONLY));
    if (merge || onlyExisting) {
      final Id mId = mediaPackage.getIdentifier();
      final SearchQuery query = new SearchQuery().withId(mId.toString());
      final SearchResult result = searchService.getByQuery(query);
      if (result.size() == 0) {
        logger.info("The search service doesn't know mediapackage {}", mId);
        if (onlyExisting) {
          logger.info("Skipping republish for {} since it is not currently published", mId);
          return createResult(mediaPackage, Action.SKIP);
        }
        publishedMediaPackage = filteredMediaPackage;
      } else if (result.size() > 1) {
        logger.warn("More than one mediapackage with id {} returned from search service", mId);
        throw new WorkflowOperationException("More than one mediapackage with id " + mId + " found");
      } else {
        publishedMediaPackage = merge(filteredMediaPackage, result.getItems()[0].getMediaPackage());
      }
    } else {
      publishedMediaPackage = filteredMediaPackage;
    }

    // Publish the media package to the search index
    try {
      logger.info("Publishing media package {} to search index", publishedMediaPackage);
      try {
        final Job publishJob = searchService.add(publishedMediaPackage);
        if (!waitForStatus(publishJob).isSuccess()) {
          throw new WorkflowOperationException("Mediapackage " + mediaPackage + " could not be republished");
        }
        final VideoPublished video = youTubeWorkspace.extractVideoInformation(mediaPackage, youTubeWorkspace.getYouTubeVideoTags());
        final YouTubeAPIVersion3Service youTubeService = YouTubeUtils.getYouTubeAPIVersion3Service();
        youTubeService.updateVideoMetadata(video);
      } catch (SearchException e) {
        throw new WorkflowOperationException("Error republishing media package", e);
      } catch (MediaPackageException e) {
        throw new WorkflowOperationException("Error parsing media package", e);
      }

      logger.info("Completed republish operation on {}", mediaPackage.getIdentifier());
      return createResult(mediaPackage, Action.CONTINUE);

    } catch (final Throwable t) {
      if (t instanceof WorkflowOperationException) {
        throw (WorkflowOperationException) t;
      } else {
        throw new WorkflowOperationException(t);
      }
    }
  }

  /**
   * Merges the updated mediapackage with the one that is currently published in a way where the updated elements
   * replace existing ones in the published mediapackage based on their flavor.
   * <p>
   * If <code>publishedMp</code> is <code>null</code>, this method returns the updated mediapackage without any
   * modifications.
   *
   * @param updatedMp
   *          the updated media package
   * @param publishedMp
   *          the mediapackage that is currently published
   * @return the merged mediapackage
   */
  private MediaPackage merge(final MediaPackage updatedMp, final MediaPackage publishedMp) {
    if (publishedMp == null) {
      return updatedMp;
    }
    final MediaPackage mergedMediaPackage = (MediaPackage) updatedMp.clone();
    for (final MediaPackageElement element : publishedMp.elements()) {
      if (updatedMp.getElementsByFlavor(element.getFlavor()).length == 0) {
        final String type = element.getElementType().toString().toLowerCase();
        logger.info("Merging {} '{}' into the updated mediapackage", type, element.getIdentifier());
        mergedMediaPackage.add((MediaPackageElement) element.clone());
      }
    }
    return mergedMediaPackage;
  }

  /**
   * Creates a clone of the mediapackage and removes those elements that do not match the flavor and tags filter
   * criteria.
   *
   * @param mediaPackage
   *          the media package
   * @param flavors
   *          the flavors
   * @param tags
   *          the tags
   * @return the filtered media package
   */
  private MediaPackage filterMediaPackage(final MediaPackage mediaPackage, final Set<MediaPackageElementFlavor> flavors,
          final List<String> tags) throws MediaPackageException {
    // The list of elements to keep
    final List<MediaPackageElement> keep = new ArrayList<MediaPackageElement>();

    // Filter by flavor
    if (flavors.size() > 0) {
      logger.debug("Filtering elements based on flavors");
      for (final MediaPackageElementFlavor flavor : flavors) {
        keep.addAll(Arrays.asList(mediaPackage.getElementsByFlavor(flavor)));
      }
    }

    // Keep those elements that have been identified in the tags
    if (tags.size() > 0) {
      logger.debug("Filtering elements based on tags");
      if (keep.size() > 0) {
        keep.retainAll(Arrays.asList(mediaPackage.getElementsByTags(tags)));
      } else {
        keep.addAll(Arrays.asList(mediaPackage.getElementsByTags(tags)));
      }
    }

    // If no filter has been supplied, take all elements
    if (flavors.size() == 0 && tags.size() == 0) {
      keep.addAll(Arrays.asList(mediaPackage.getElements()));
    }

    // Fix references and flavors
    final MediaPackage filteredMediaPackage = (MediaPackage) mediaPackage.clone();
    for (final MediaPackageElement element : filteredMediaPackage.getElements()) {
      if (!keep.contains(element)) {
        logger.info("Removing {} '{}' from media-package '{}'", element.getElementType().toString().toLowerCase(),
            element.getIdentifier(), filteredMediaPackage.getIdentifier().toString());
        filteredMediaPackage.remove(element);
        continue;
      }

      // Is the element referencing anything?
      MediaPackageReference reference = element.getReference();
      if (reference != null) {
        Map<String, String> referenceProperties = reference.getProperties();
        MediaPackageElement referencedElement = filteredMediaPackage.getElementByReference(reference);

        // if we are distributing the referenced element, everything is fine. Otherwise...
        if (referencedElement != null && !keep.contains(referencedElement)) {

          // Follow the references until we find a flavor
          MediaPackageElement parent;
          while ((parent = mediaPackage.getElementByReference(reference)) != null) {
            if (parent.getFlavor() != null && element.getFlavor() == null) {
              element.setFlavor(parent.getFlavor());
            }
            if (parent.getReference() == null)
              break;
            reference = parent.getReference();
          }

          // Done. Let's cut the path but keep references to the mediapackage itself
          if (reference.getType().equals(MediaPackageReference.TYPE_MEDIAPACKAGE)) {
            element.setReference(reference);
          } else if (referenceProperties == null || referenceProperties.size() == 0) {
            element.clearReference();
          } else {
            // Ok, there is more to that reference than just pointing at an element. Let's keep the original,
            // you never know.
            referencedElement.setURI(null);
            referencedElement.setChecksum(null);
          }
        }
      }
    }
    return filteredMediaPackage;
  }

  /**
   * Callback for declarative services configuration that will introduce us to the search service. Implementation
   * assumes that the reference is configured as being static.
   *
   * @param searchService
   *          an instance of the search service
   */
  protected void setSearchService(final SearchService searchService) {
    this.searchService = searchService;
  }

  /**
   * Callback for the OSGi environment to set the workspace reference.
   *
   * @param workspace
   *          the workspace
   */
  protected void setWorkspace(final Workspace workspace) {
    this.youTubeWorkspace = new YouTubeWorkspace(workspace);
  }

}
