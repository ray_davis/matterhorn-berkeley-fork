/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreValue;
import org.opencastproject.publication.api.PublisherNamespace;

import java.util.List;

/**
 * @author John Crossman
 */
public final class YouTubeWorkspaceUtils {

  private YouTubeWorkspaceUtils() {
  }

  public static String extractYouTubePlaylistId(final DublinCoreCatalog seriesCatalog) {
    final PublisherNamespace namespace = PublisherNamespace.YOUTUBE;
    final String publisher = getPublisher(namespace, seriesCatalog);
    final String value = StringUtils.trimToNull(publisher == null
            ? null
            : namespace.parsePlayListFromPublisherUrn(publisher));
    return value == null || StringUtils.equalsIgnoreCase(value, "null") ? null : value;
  }

  public static String getPublisher(final PublisherNamespace namespace, final DublinCoreCatalog catalog) {
    String publisher = null;
    final List<DublinCoreValue> publisherList = catalog == null ? null : catalog.get(DublinCore.PROPERTY_PUBLISHER);
    if (publisherList != null) {
      for (final DublinCoreValue value : publisherList) {
        if (namespace.isPublisher(value.getValue())) {
          publisher = value.getValue();
        }
      }
    }
    return publisher;
  }

}
