/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.metadata.dublincore;

import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author John Crossman
 */
public class DublinCoreCatalogImplTest {

  @Test(expected = IllegalStateException.class)
  public void testMalformedDublinCoreXML() {
    final InputStream stream = getClass().getClassLoader().getResourceAsStream("dublincore-malformed.xml");
    new DublinCoreCatalogImpl(stream);
  }

  @Test
  public void testLoadCatalogData() {
    final InputStream stream = getClass().getClassLoader().getResourceAsStream("dublincore-happy.xml");
    final DublinCoreCatalogImpl catalog = new DublinCoreCatalogImpl(stream);
    assertEquals("51879", catalog.getFirst(DublinCore.PROPERTY_IDENTIFIER));
  }

  @Test
  public void testLoadCatalogDataAndIdentifier() {
    final InputStream stream = getClass().getClassLoader().getResourceAsStream("dublincore.xml");
    final DublinCoreCatalogImpl catalog = new DublinCoreCatalogImpl(stream);
    assertNull(catalog.getIdentifier());
    assertEquals("10.0000/5819", catalog.getFirst(DublinCore.PROPERTY_IDENTIFIER));
  }

}
