/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import static com.google.api.client.util.BackOff.STOP;
import static org.fest.assertions.Assertions.assertThat;

import com.google.api.client.util.BackOff;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StagedBackOffTest {

  private List<StagedBackOffPolicy> policies;
  private static final long MINUTES_IN_MILLISECONDS = 60000;

  @Before
  public void setUp() throws Exception {
    policies = new ArrayList<StagedBackOffPolicy>();
  }

  @Test
  public void testNextBackOffMillisReturnsStopWhenDefaultPolicyImplicitlyUsed() throws IOException {
    final BackOff aNullPolicy = new StagedBackOff(null);
    assertThat(aNullPolicy.nextBackOffMillis()).isEqualTo(STOP);
    final List<StagedBackOffPolicy> list = Collections.emptyList();
    final BackOff aEmptyList = new StagedBackOff(list);
    assertThat(aEmptyList.nextBackOffMillis()).isEqualTo(STOP);
  }
  
  @Test
  public void testNextBackOffMillisReturnsRetryStageDelayBeforeRetryWaitTime() throws IOException {
    final long retryStageDelay = 0;
    final long retryWaitTime = 1000;
    final int retryRepetitions = 2;
    policies.add(new StagedBackOffPolicy("Stage One", retryStageDelay, retryWaitTime, retryRepetitions));
    final BackOff aBackOff = new StagedBackOff(policies);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryStageDelay);
  }
  
  @Test
  public void testNextBackOffMillisCalledRetryRepetitionsTimes() throws IOException {
    final long retryStageDelay = 0;
    final long retryWaitTime = 1000;
    final int retryRepetitions = 2;
    policies.add(new StagedBackOffPolicy("Stage One", retryStageDelay, retryWaitTime, retryRepetitions));
    final BackOff aBackOff = new StagedBackOff(policies);
    aBackOff.nextBackOffMillis(); // first value returned for each stage is always retryStageDelay
    for (int i = 0; i < retryRepetitions; i++) {
      assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryWaitTime);
    }
  }
  
  @Test
  public void testTotalWaitTimeIsEqualToRetryStageDelayPlusRetryRepetitionsTimesRetryWaitTime() throws IOException {
    final long retryStageDelay = 1;
    final long retryWaitTime = 1000;
    final int retryRepetitions = 3;
    long totaExpectedlWaitTime = 3001;
    long totalActualWaitTime = 0;
    policies.add(new StagedBackOffPolicy("Stage One", retryStageDelay, retryWaitTime, retryRepetitions));
    final BackOff aBackOff = new StagedBackOff(policies);
    totalActualWaitTime = aBackOff.nextBackOffMillis(); 
    for (int i = 0; i < retryRepetitions; i++) {
      totalActualWaitTime = totalActualWaitTime + aBackOff.nextBackOffMillis();
    }
    assertThat(totalActualWaitTime).isEqualTo(totaExpectedlWaitTime);
  }
  
  @Test
  public void testNextBackOffMillisReturnsAcrossTwoPoliciesInNaturalOrderBeforeStopping() throws IOException {
    final long retryStageDelay = 1;
    final long retryWaitTime = 1000;
    policies.add(new StagedBackOffPolicy("Stage One", retryStageDelay, retryWaitTime, 2));
    policies.add(new StagedBackOffPolicy("Stage Three", retryStageDelay, retryWaitTime, 3));
    final BackOff aBackOff = new StagedBackOff(policies);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryStageDelay);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryWaitTime);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryWaitTime);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryStageDelay);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryWaitTime);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryWaitTime);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(retryWaitTime);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(STOP);
  }
  
  @Test
  public void testAllNonDefaultPolicyTypes() throws IOException {
    int expectedOncePerPolicy = 0;
    for (int i = 1; i < 5; i++) {
      policies.add(new StagedBackOffPolicy("A Description",0, 0, 0));
      expectedOncePerPolicy++;
    }
    final BackOff aBackOff = new StagedBackOff(policies);
    int countDownFromExpectedOncePerPolicy = expectedOncePerPolicy;
    while (aBackOff.nextBackOffMillis() != STOP) {
      assertThat(countDownFromExpectedOncePerPolicy--).isNotEqualTo(0);
    }
  }
  
  @Test
  public void testResetStartsAllOverAgain() throws IOException {
    int expectedOncePerPolicy = 0;
    for (int i = 1; i < 5; i++) {
      policies.add(new StagedBackOffPolicy("A Description",0, 0, 0));
      expectedOncePerPolicy++;
    }
    final BackOff aBackOff = new StagedBackOff(policies);
    int countDownFromExpectedOncePerPolicy = expectedOncePerPolicy;
    while (aBackOff.nextBackOffMillis() != STOP) {
      assertThat(countDownFromExpectedOncePerPolicy--).isNotEqualTo(0);
    }
    aBackOff.reset();
    countDownFromExpectedOncePerPolicy = expectedOncePerPolicy;
    while (aBackOff.nextBackOffMillis() != STOP) {
      assertThat(countDownFromExpectedOncePerPolicy--).isNotEqualTo(0);
    }
  }
  
  // Inspired by the original description in JIRA WCT-4614:
  // - First failure - immediately retry
  // - Second failure - retry in 15 minutes
  // - Third failure - retry in 1 hour.
  // - Fourth failure - retry in 24 hours.
  // - If there is a fifth failure, fail the job entirely. 
  @Test
  public void testWCT4614() throws IOException {
    final long firstFailure = 0;
    final long secondFailure = 15 * MINUTES_IN_MILLISECONDS;
    final long thirdFailure =  60 * MINUTES_IN_MILLISECONDS;
    final long fourthFailure = 24 * 60 * MINUTES_IN_MILLISECONDS;
    final long fifthFailure = STOP;
    
    policies.add(new StagedBackOffPolicy("", firstFailure, secondFailure, 1));
    policies.add(new StagedBackOffPolicy("", thirdFailure, fourthFailure, 1));
    
    final BackOff aBackOff = new StagedBackOff(policies);

    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(firstFailure);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(secondFailure);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(thirdFailure);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(fourthFailure);
    assertThat(aBackOff.nextBackOffMillis()).isEqualTo(fifthFailure);
  }
}
