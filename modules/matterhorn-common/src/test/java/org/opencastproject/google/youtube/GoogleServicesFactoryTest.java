/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import org.junit.Test;
import org.opencastproject.key.ConfigUtils;

import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author John Crossman
 */
public class GoogleServicesFactoryTest {

  @Test(expected = IllegalArgumentException.class)
  public void testGetWhenRequired() {
    ConfigUtils.get(new Properties(), YouTubeKey.clientSecretsV3, true);
  }

  @Test
  public void testNull() {
    assertNull(ConfigUtils.get(new Properties(), YouTubeKey.clientSecretsV3, false));
  }

  @Test
  public void testGet() {
    final String value = "value";
    final String keyString = "org.opencastproject.publication.youtube." + YouTubeKey.clientSecretsV3.name();
    final Properties properties = new Properties();
    properties.put(keyString, value);
    assertEquals(value, ConfigUtils.get(properties, YouTubeKey.clientSecretsV3, true));
    assertEquals(value, ConfigUtils.get(properties, YouTubeKey.clientSecretsV3, false));
    assertEquals(value, ConfigUtils.get(properties, YouTubeKey.clientSecretsV3, false));
  }

}
