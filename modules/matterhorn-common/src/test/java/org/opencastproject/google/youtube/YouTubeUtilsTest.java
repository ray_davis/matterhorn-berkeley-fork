/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import com.google.api.client.util.BackOff;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.opencastproject.util.data.Tuple;
import org.opencastproject.util.env.EnvironmentUtil;
import org.osgi.service.cm.ConfigurationException;

import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.Properties;

import static com.google.api.client.util.BackOff.STOP;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author John Crossman
 */
public class YouTubeUtilsTest {

  private final String retryPolicy = "First failure:0:0:1,Second failure:900000:0:1,Third failure:3600000:0:1,Fourth failure:0:7200000:12";
  private final int totalExpectedTries = 19;

  @Test
  public void testYouTubeRetry() throws IOException {
    final StagedBackOff backOff = getBackOff(retryPolicy);
    assertEquals(4, backOff.getPolicies().size());
    // First failure
    assertEquals(totalExpectedTries, backOff.getStageStack().size());
    assertEquals((long) 0, backOff.nextBackOffMillis());
    assertEquals((long) 0, backOff.nextBackOffMillis());
    // Second failure
    assertEquals(totalExpectedTries - 2, backOff.getStageStack().size());
    assertEquals((long) 900000, backOff.nextBackOffMillis());
    assertEquals((long) 0, backOff.nextBackOffMillis());
    // Third failure
    assertEquals(totalExpectedTries - 4, backOff.getStageStack().size());
    assertEquals((long) 3600000, backOff.nextBackOffMillis());
    assertEquals((long) 0, backOff.nextBackOffMillis());
    // Third failure
    assertEquals(totalExpectedTries - 6, backOff.getStageStack().size());
    assertEquals((long) 0, backOff.nextBackOffMillis());
    for (int index = 1; index <= 12; index++) {
      assertEquals((long) 7200000, backOff.nextBackOffMillis());
    }
    assertEquals(0, backOff.getStageStack().size());
  }

  @Test
  public void testYouTubeRetryReset() throws IOException {
    final StagedBackOff backOff = getBackOff(retryPolicy);
    assertEquals(4, backOff.getPolicies().size());
    // First failure
    assertEquals(totalExpectedTries, backOff.getStageStack().size());
    backOff.nextBackOffMillis();
    backOff.nextBackOffMillis();
    backOff.nextBackOffMillis();
    backOff.nextBackOffMillis();
    assertEquals(totalExpectedTries - 4, backOff.getStageStack().size());
    backOff.reset();
    assertEquals(totalExpectedTries, backOff.getStageStack().size());
  }

  @Test
  public void testYouTubeRetryDefaultConfig() throws IOException {
    final File file = EnvironmentUtil.getFileUnderFelixHome(
            "etc/services/org.opencastproject.publication.youtube.YouTubeV3PublicationServiceImpl.properties");
    final Properties p = new Properties();
    p.load(FileUtils.openInputStream(file));
    final String value = p.getProperty("org.opencastproject.publication.youtube.retryPolicy");
    assertNotNull(value);
    final StagedBackOff backOff = YouTubeUtils.getStagedBackOff(p);
    //
    assertEquals(4, backOff.getPolicies().size());
    int index = 0;
    for (final StagedBackOffPolicy policy : backOff.getPolicies()) {
      final String description = policy.getDescription();
      assertTrue(description.contains(" failure"));
      // TODO: the internals of StagedBackOff maintain policies list in reverse order
      final long retryStageDelay = policy.getRetryStageDelay();
      final long retryWaitTime = policy.getRetryWaitTime();
      final int retryRepetitions = policy.getRetryRepetitions();
      switch (index) {
        case 0:
          assertEquals(7200000, retryStageDelay);
          assertEquals(7200000, retryWaitTime);
          assertEquals(12, retryRepetitions);
          break;
        case 1:
          assertEquals(3600000, retryStageDelay);
          assertEquals(0, retryWaitTime);
          assertEquals(1, retryRepetitions);
          break;
        case 2:
          assertEquals(900000, retryStageDelay);
          assertEquals(0, retryWaitTime);
          assertEquals(1, retryRepetitions);
          break;
        case 3:
          assertEquals(0, retryStageDelay);
          assertEquals(0, retryWaitTime);
          assertEquals(1, retryRepetitions);
          break;
        default:
          fail("Unexpected number of policies");
      }
      index++;
    }
  }

  @Test
  public void testNoYouTubeRetry() throws IOException {
    testNoYouTubeRetry(null);
    testNoYouTubeRetry("   ");
  }

  private void testNoYouTubeRetry(final String retryPolicy) throws IOException {
    final StagedBackOff backOff = getBackOff(retryPolicy);
    assertEquals(BackOff.STOP, backOff.nextBackOffMillis());
    //
    assertEquals(1, backOff.getPolicies().size());
    final StagedBackOffPolicy policy = backOff.getPolicies().get(0);
    assertTrue(policy.getDescription().contains("Default"));
    assertEquals(0, policy.getRetryRepetitions());
    //
    final Deque<Tuple<String, Long>> stageStack = backOff.getStageStack();
    assertTrue(stageStack.isEmpty());
  }

  @Test
  public void testNullRetryPropertyInvokesDefaultPolicy() throws ConfigurationException, IOException {
    assertThat(getBackOff(null).nextBackOffMillis()).isEqualTo(STOP);
    assertThat(getBackOff("  ").nextBackOffMillis()).isEqualTo(STOP);
  }

  @Test
  public void testUpdatedConfiguration() throws ConfigurationException, IOException {
    final StagedBackOff policy = getBackOff("First failure:0:0:0,Second failure:900000:0:0,Third failure:3600000:0:0,Fourth failure:86400000:0:0");
    assertThat(policy.nextBackOffMillis()).isEqualTo(0);
    assertThat(policy.nextBackOffMillis()).isEqualTo(900000);
    assertThat(policy.nextBackOffMillis()).isEqualTo(3600000);
    assertThat(policy.nextBackOffMillis()).isEqualTo(86400000);
    assertThat(policy.nextBackOffMillis()).isEqualTo(STOP);
  }

  @Test
  public void testInvalidRetryPropertyIsSkipped() throws ConfigurationException, IOException {
    final StagedBackOff policy = getBackOff("Good property:0:0:0,Invalid property - missing param:0:0,Invalid property - number format:3600000:o:0,Good property:86400000:0:0");
    assertThat(policy.nextBackOffMillis()).isEqualTo(0);
    assertThat(policy.nextBackOffMillis()).isEqualTo(86400000);
    assertThat(policy.nextBackOffMillis()).isEqualTo(STOP);
  }

  private StagedBackOff getBackOff(final String retryPolicy) {
    final Properties properties = new Properties();
    if (retryPolicy != null) {
      properties.put(YouTubeKey.retryPolicy.getKey(), retryPolicy);
    }
    return YouTubeUtils.getStagedBackOff(properties);
  }

}
