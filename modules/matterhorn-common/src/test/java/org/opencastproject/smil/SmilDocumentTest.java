/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.smil;

import static org.fest.assertions.Assertions.assertThat;

import org.opencastproject.util.MimeType;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

/**
 * A suite of tests focusing on the marshalling and unmarshalling of {@link SmilDocument} .
 * 
 */
public class SmilDocumentTest {
  private static final String AUDIO_ELEMENT = "<audio src=\"track-2.mp3\" clipBegin=\"0ms\" clipEnd=\"58000ms\"/>";
  private static final String VIDEO_ELEMENT = "<video src=\"track-1.mp4\" clipBegin=\"0ms\" clipEnd=\"58000ms\"/>";
  private SmilClipElement audioElement;
  private SmilClipElement videoElement;
  private MimeType videoMt;
  private SmilDocument smilDocument;
  private String smilDocumentSerialized;
  
  @Test
  public void marshalSmilDocument() {
    assertThat(smilDocument.marshalSmilDocument()).isEqualTo(smilDocumentSerialized);
  }
  
  @Test
  public void unMarshalSmilDocument() {
    assertThat(SmilDocument.unMarshalSmilDocument(smilDocumentSerialized)).isEqualTo(smilDocument);
  }

  @Test
  public void marshalSmilClipElement() {
    assertThat(audioElement.marshalSmilClipElement()).isEqualTo(AUDIO_ELEMENT);
    assertThat(videoElement.marshalSmilClipElement()).isEqualTo(VIDEO_ELEMENT);
  }
  
  @Test
  public void unMarshalSmilClipElement() {
    assertThat(SmilClipElement.unMarshalSmilClipElement(VIDEO_ELEMENT)).isEqualTo(videoElement);
    assertThat(SmilClipElement.unMarshalSmilClipElement(AUDIO_ELEMENT)).isEqualTo(audioElement);
  }
  
  @Before
  public void buildSmilDocumentSerialized() throws Exception {
    final StringBuilder sb = new StringBuilder();
    sb.append("<smil xmlns=\"http://www.w3.org/ns/SMIL\" version=\"3.0\" baseProfile=\"Language\">");
    sb.append("<body>");
    sb.append(AUDIO_ELEMENT);
    sb.append(VIDEO_ELEMENT);
    sb.append("</body>");
    sb.append("</smil>");
    smilDocumentSerialized = sb.toString();
  }
  
  @Before
  public void instantiateSmilDocument() {
    smilDocument = new SmilDocument();
    final MimeType audioMt = MimeType.mimeType("audio", StringUtils.EMPTY);
    audioElement = new SmilClipElement(audioMt, "track-2.mp3");
    audioElement.setClipBegin(0);
    audioElement.setClipEnd(58000);
    smilDocument.addClip(audioElement);
    videoMt = MimeType.mimeType("video", StringUtils.EMPTY);
    videoElement = new SmilClipElement(videoMt, "track-1.mp4");
    videoElement.setClipBegin(0);
    videoElement.setClipEnd(58000);
    smilDocument.addClip(videoElement);
  }

}
