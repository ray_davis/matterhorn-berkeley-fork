/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.key.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author John Crossman
 */
public final class YouTubeUtils {

  private YouTubeUtils() {
  }

  public static YouTubeAPIVersion3Service getYouTubeAPIVersion3Service() throws IOException {
    final Properties properties = GoogleUtils.getYouTubeSettings();
    final GoogleServicesFactory servicesFactory = new GoogleServicesFactory(properties);
    final YouTubeAPIVersion3Service youTubeAPIVersion3Service = new YouTubeAPIVersion3ServiceImpl();
    youTubeAPIVersion3Service.initialize(servicesFactory);
    return youTubeAPIVersion3Service;
  }

  public static StagedBackOff getStagedBackOff(final Properties properties) {
    final String retryPolicy = ConfigUtils.get(properties, YouTubeKey.retryPolicy, false);
    final List<StagedBackOffPolicy> policyList = new ArrayList<StagedBackOffPolicy>();
    if (!StringUtils.isEmpty(retryPolicy)) {
      String[] policies = retryPolicy.split(",");
      for (String policy : policies) {
        final Logger logger = LoggerFactory.getLogger(GoogleServicesFactory.class);
        try {
          String[] parms = policy.split(":");
          if (parms.length == 4) {
            final String description = parms[0];
            final long retryStageDelay = Long.parseLong(parms[1]);
            final long retryWaitTime = Long.parseLong(parms[2]);
            final int retryRepetitions = Integer.parseInt(parms[3]);
            policyList.add(new StagedBackOffPolicy(description, retryStageDelay, retryWaitTime, retryRepetitions));
          } else {
            logger.warn("Missing parameters, stage  " + policy + " will be ignored");
          }
        } catch (NumberFormatException e) {
          logger.warn("Invalid number, stage " + policy + " will be ignored");
        }
      }
    }
    return new StagedBackOff(policyList);
  }

}
