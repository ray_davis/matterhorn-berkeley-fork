/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import com.google.api.client.http.HttpStatusCodes;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.opencastproject.util.env.Environment;
import org.opencastproject.util.env.EnvironmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;

/**
 * @author John Crossman
 */
public class StatusCodeOverride implements HasStatusCodes {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Override public Integer getJsonCode() {
    return getCode(ForceFailureType.forceGoogleErrorJSON);
  }

  @Override public Integer getResponseStatus() {
    return getCode(ForceFailureType.forceFailureYouTubeAPI);
  }

  protected final Integer getCode(final ForceFailureType type) {
    final Integer code;
    if (isProduction()) {
      code = null;
    } else {
      final String key = type.getKey();
      final String value = StringUtils.trimToNull(getProperties().get(key));
      if (value == null) {
        code = null;
      } else {
        code = NumberUtils.toInt(value, -1);
        final int min = HttpStatusCodes.STATUS_CODE_MULTIPLE_CHOICES;
        if (code < min) {
          final String message = type.name()
                  .concat(" has illegal value = ")
                  .concat(value)
                  .concat(". The property must be numeric and greater than  ")
                  .concat(Integer.toString(min));
          throw new UnsupportedOperationException(message);
        } else {
          logger.warn("Force failure in YouTube API: " + key + '=' + value);
        }
      }
    }
    return code;
  }

  protected boolean isProduction() {
    return Environment.prod.equals(EnvironmentUtil.getEnvironment());
  }

  protected Dictionary<String, String> getProperties() {
    return EnvironmentUtil.getRuntimePropertyOverrides();
  }

}
