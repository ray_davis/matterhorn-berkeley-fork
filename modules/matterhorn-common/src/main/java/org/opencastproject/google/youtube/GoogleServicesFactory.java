/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.google.youtube;

import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;

import java.io.IOException;
import java.util.Properties;
/**
 * Supports YouTube property management.
 *
 * TODO: {@link org.opencastproject.google.youtube.GoogleHttpTransport} GoogleHttpTransport is a copy-and-paste of
 * TODO:  {@link com.google.api.client.http.javanet.NetHttpTransport}. Why? Because, we need a publish-to-YouTube-retry
 * TODO:  implementation that works. Unfortunately, the standard {@link com.google.api.client.http.javanet.NetHttpRequest#execute()}
 * TODO:  does not throw an IOException when response status code >= 500. So we are forced to create our own
 * TODO:  {@link GoogleHttpRequest#execute()}. The IOException we throw will cause the catch block in
 * TODO:  {@link com.google.api.client.http.HttpRequest#execute()} to happen. I.e., Retry algorithm will kick in.
 * TODO:  If {@link com.google.api.client.http.javanet.NetHttpTransport} is ever NOT a "final" class then we might be
 * TODO:  able to reduce our copy-and-paste lines of code.
 *
 * @author John Crossman
 */
public final class GoogleServicesFactory {

  private YouTube youTube;
  private final Properties properties;

  public GoogleServicesFactory(final Properties properties) {
    this.properties = properties;
  }

  YouTube getYouTube() throws IOException {
    if (youTube == null) {
      final GoogleHttpRequestInitializer initializer = new GoogleHttpRequestInitializer(properties);
      final GoogleHttpTransport transport = new GoogleHttpTransport(new StatusCodeOverride());
      youTube = new YouTube.Builder(transport, new JacksonFactory(), initializer)
              .setApplicationName("UC Berkeley Webcast")
              .build();
    }
    return youTube;
  }

}
