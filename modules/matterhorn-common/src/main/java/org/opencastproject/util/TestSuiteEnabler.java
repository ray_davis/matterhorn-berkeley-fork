/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.util.env.EnvironmentUtil;

/**
 * @author John Crossman
 */
public class TestSuiteEnabler {

  protected final String testSuiteName;
  protected final boolean testSuiteEnabled;

  public TestSuiteEnabler(final String testSuiteName) {
    final String[] integrationTests = StringUtils
            .split(EnvironmentUtil.getRuntimePropertyOverrides().get("unit.integrationTest.enabled"), ",");
    this.testSuiteEnabled = ArrayUtils.contains(integrationTests, testSuiteName);
    this.testSuiteName = testSuiteName;
  }

  public String getTestSuiteName() {
    return testSuiteName;
  }

  public boolean isTestSuiteEnabled() {
    return testSuiteEnabled;
  }
}
