/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.smil;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Rudimentary abstraction of a Synchronized Media interface Language (SMIL)
 * file used for attaching trim point metadata to a media package.
 *
 */
public class SmilDocument {
  private List<SmilClipElement> smilClipElements;
  
  public SmilDocument() {
    smilClipElements = new ArrayList<SmilClipElement>();
  }
  
  public List<SmilClipElement> getSmilClipElements() {
    return smilClipElements;
  }
  
  public void addClip(SmilClipElement smilClipElement) {
    smilClipElements.add(smilClipElement);
  }
  
  public String marshalSmilDocument() {
    StringBuilder sb = new StringBuilder();
    sb.append("<smil xmlns=\"http://www.w3.org/ns/SMIL\" version=\"3.0\" baseProfile=\"Language\">");
    sb.append("<body>");
    for (SmilClipElement smilClipElement : smilClipElements) {
      sb.append(smilClipElement.marshalSmilClipElement());
    }
    sb.append("</body>");
    sb.append("</smil>");
    return sb.toString();
  }
  
  public static SmilDocument unMarshalSmilDocument(String str) {
    final SmilDocument smilDocument = new SmilDocument();
    final String regex = "^<smil.+body>(<.+/>)</body></smil>$";
    final Pattern pattern = Pattern.compile(regex);
    final Matcher m = pattern.matcher(str);
    String elementStr = null;
    while (m.find()) {
      elementStr = m.group(1);
    }
    final String[] elements = StringUtils.split(elementStr, '>');
    for (String element : elements) {
      SmilClipElement smilClipElement = SmilClipElement.unMarshalSmilClipElement(element + ">");
      smilDocument.addClip(smilClipElement);
    }
    return smilDocument;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((smilClipElements == null) ? 0 : smilClipElements.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof SmilDocument)) {
      return false;
    }
    SmilDocument other = (SmilDocument) obj;
    if (smilClipElements == null) {
      if (other.smilClipElements != null) {
        return false;
      }
    } else if (!smilClipElements.equals(other.smilClipElements)) {
      return false;
    }
    return true;
  }
  
}
