/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.key;

import org.apache.commons.lang.StringUtils;
import org.osgi.framework.BundleContext;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * @author John Crossman
 */
public final class ConfigUtils {

  private ConfigUtils() {
  }

  /**
   * @param properties Null not allowed
   * @param configKeys One or more keys to use for property get.
   * @return Never null
   */
  public static List<String> get(final Properties properties, final ConfigKey... configKeys) {
    final List<String> list = new LinkedList<String>();
    for (final ConfigKey configKey : configKeys) {
      list.add(properties.getProperty(configKey.getKey()));
    }
    return list;
  }

  /**
   * @param bundleContext Null not allowed
   * @param configKeys One or more keys to use for property get.
   * @return Never null
   */
  public static List<String> get(final BundleContext bundleContext, final ConfigKey... configKeys) {
    final List<String> list = new LinkedList<String>();
    for (final ConfigKey configKey : configKeys) {
      list.add(getProperty(bundleContext, configKey));
    }
    return list;
  }

  /**
   * @param bundleContext Null not allowed
   * @param configKey may not be {@code null}
   * @return Never null
   */
  public static String getProperty(final BundleContext bundleContext, final ConfigKey configKey) {
    return bundleContext.getProperty(configKey.getKey());
  }

  /**
   * Disciplined way of getting required properties.
   *
   * @param configKey may not be {@code null}
   * @param required when true, and property result is null, we throw {@link java.lang.IllegalArgumentException}
   * @return associated value or null
   */
  public static String get(final Properties properties, final ConfigKey configKey, final boolean required) {
    final String value = (String) properties.get(configKey.getKey());
    final String trimmed = StringUtils.trimToNull(value);
    if (required && trimmed == null) {
      throw new IllegalArgumentException("Null or blank value for YouTube-related property: " + configKey);
    }
    return trimmed;
  }

}
