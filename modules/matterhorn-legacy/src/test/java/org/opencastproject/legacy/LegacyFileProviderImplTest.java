/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.mediapackage.MediaPackageImpl;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.env.EnvironmentUtil;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.Recording;
import org.opencastproject.warehouse.WarehouseBuilding;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseRoom;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.easymock.EasyMock.createNiceMock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
@Ignore
public class LegacyFileProviderImplTest {

  private final LegacyFileProviderImpl fileProvider;

  public LegacyFileProviderImplTest() throws IOException {
    this.fileProvider = new LegacyFileProviderImpl();
  }

  @Test
  public void testSpring2014RecordingsFromMediaPackageMock() throws IOException {
    final LegacyFileProvider mock = new LegacyFileProviderMock();
    assertTrue(mock.getSpring2014RecordingsFromMediaPackage().size() > 0);
  }

  @Test
  public void testLegacyFileProviderMock() throws IOException {
    final LegacyFileProvider mock = new LegacyFileProviderMock();
    assertTrue(mock.getWebcastBerkeleyLegacyJSON().exists());
    final Map<Integer, ITunesCollection> map = mock.getITunesCollectionMap();
    assertEquals("Environ Sci, Policy, and Management 117 Lab, 101 - Fall 2013", map.get(716979484).getTitle());
    assertEquals("Environ Sci, Policy, and Management 117, 001 - Fall 2013", map.get(703067761).getTitle());
    //
    final ITunesCollection physics7A = map.get(461120796);
    assertEquals("Physics 7A 1 - Spring 2007: Physics for Scientists and Engineers", physics7A.getTitle());
    assertEquals(461120796, physics7A.getITunesId().intValue());
    assertEquals(2007, physics7A.getYear().intValue());
    assertEquals(Semester.Spring, physics7A.getSemester());
    //
    for (final Integer mediaId : map.keySet()) {
      final ITunesCollection collection = map.get(mediaId);
      assertEquals(mediaId, collection.getITunesId());
      assertNotNull(collection.getTitle());
      assertNotNull(collection.getYear());
      assertNotNull(collection.getSemester());
    }
  }

  @Test
  public void testGetEpisodesPerSeries() throws IOException {
    final File file = EnvironmentUtil
            .getFileUnderFelixHome("etc/data/webcast_warehouse/2014-Spring_ALL_mediapackage_xml.txt");
    final Map<String, List<MediaPackageImpl>> episodesPerSeries = fileProvider.getEpisodesPerSeries(file);
    testGetEpisodesPerSeries(episodesPerSeries, "2014B28117", 26);
    testGetEpisodesPerSeries(episodesPerSeries, "2014B28927", 20);
    testGetEpisodesPerSeries(episodesPerSeries, "2014B39564", 24);
  }

  private void testGetEpisodesPerSeries(final Map<String, List<MediaPackageImpl>> episodesPerSeries, final String seriesId,
          final int expectedRecordingCount) throws IOException {
    final List<MediaPackageImpl> english117S = episodesPerSeries.get(seriesId);
    final String errorMessage = "Fail for " + seriesId;
    //
    final Map<String, List<MediaPackageImpl>> map = new HashMap<String, List<MediaPackageImpl>>(1);
    map.put(seriesId, english117S);
    final Map<String, Set<RecordingMediaPackage>> seriesIdMap = fileProvider.getSeriesIdMap(map);
    //
    final Set<RecordingMediaPackage> recordings = seriesIdMap.get(seriesId);
    for (final RecordingMediaPackage next : recordings) {
      System.out.println(next.getRecording().toString());
    }
    assertEquals(errorMessage, expectedRecordingCount, recordings.size());
  }

  @Test
  public void testFilesExist() {
    assertTrue(fileProvider.getWebcastBerkeleyLegacyJSON().exists());
  }

  @Test
  public void testSpring2014Recordings() throws IOException {
    final List<WarehouseCourse> list = new LinkedList<WarehouseCourse>();
    final CanonicalCourse cc = new CanonicalCourse(26420, "Computer Science 70, 001", "Discrete Mathematics and Probability Theory");
    final WarehouseCourse w = new WarehouseCourse(cc, 2014, Semester.Spring, "70",
            "Chem", "001", Collections.list(DayOfWeek.Tuesday, DayOfWeek.Thursday),
            new WarehouseRoom(WarehouseBuilding.Pimentel, "1"),
            null,
            new String[] { "Anant Sahai"}, "2gfXZSA1oMg&list=PL-XXv-cvA_iDze6fOp3qofgyjJVUioedA",
            new CapturePreferences(RecordingType.videoAndScreencast, RecordingAvailability.studentsOnly, 7),
            AssociatedLicense.all_rights_reserved);
    list.add(w);
  }

  @Test
  public void testGetSeriesIdMap() throws IOException {
    final Map<String, Set<RecordingMediaPackage>> seriesIdMap = fileProvider.getSpring2014RecordingsFromMediaPackage();
    final Set<RecordingMediaPackage> recordings = seriesIdMap.get("2014B26420");
    assertNotNull(recordings);
    assertEquals(53, recordings.size());
    final Set<Integer> expectedCCNs = Collections.set(7049, 39576, 26420, 51881, 70564, 58334, 25165, 76270, 64503, 80913, 27603, 81603, 5915, 26651, 64746, 3717, 26561, 14069, 55492, 26420);
    final List<Integer> foundCCNs = new LinkedList<Integer>();
    for (final String seriesId : seriesIdMap.keySet()) {
      final CourseKey courseKey = CourseUtils.getCourseKey(seriesId);
      final int ccn = courseKey.getCcn();
      if (expectedCCNs.contains(ccn)) {
        expectedCCNs.remove(ccn);
        foundCCNs.add(ccn);
      }
    }
    testPutSpring2014Recordings(seriesIdMap);
    //
    System.out.println("Not found:");
    for (final Integer ccn : expectedCCNs) {
      System.out.println(ccn);
    }
    System.out.println("Found:");
    for (final Integer ccn : foundCCNs) {
      System.out.println(ccn);
    }
  }

  private void testPutSpring2014Recordings(Map<String, Set<RecordingMediaPackage>> seriesIdMap) throws IOException {
    final HashMap<CourseKey, WarehouseCourse> map = new HashMap<CourseKey, WarehouseCourse>();
    final Notifier notifier = createNiceMock(Notifier.class);
    final DatabaseCredentials credentials = LegacyUtils.getCourseDatabaseCredentials();
    final ProvidesCourseCatalogData courseDatabase = new UCBerkeleyCourseDatabaseImpl(credentials, notifier);
    LegacyUtils.putSpring2014Recordings(map, seriesIdMap, courseDatabase);
    final LastMinuteLegacyDataFixer fixer = new LastMinuteLegacyDataFixer(map.values());
    final List<WarehouseCourse> fixed = fixer.getFixed();
    Set<Recording> recordingSet = null;
    for (final WarehouseCourse w : fixed) {
      if (w.getCcn() == 26420 && w.getYear() == 2014 && w.getSemester() == Semester.Spring) {
        recordingSet = w.getRecordings();
        for (final Recording next : recordingSet) {
          assertNotNull(next.getYouTubeVideoId());
        }
        break;
      }
    }
    assertNotNull(recordingSet);
    assertEquals(25, recordingSet.size());
  }

  @Test
  public void testGetITunesCollectionMap() throws IOException {
    final Map<Integer, Term> idTermMap = getIdTermMap();
    final Set<Integer> uniqueIds = new TreeSet<Integer>();
    final Map<Integer, ITunesCollection> map = fileProvider.getITunesCollectionMap();
    assertEquals(1022, map.size());
    for (final Integer key : map.keySet()) {
      final ITunesCollection c = map.get(key);
      assertFalse("Duplicate id found in file: " + key, uniqueIds.contains(c.getITunesId()));
      uniqueIds.add(c.getITunesId());
      assertEquals(key, c.getITunesId());
      assertNotNull(c.getYear());
      assertNotNull(c.getSemester());
      final Term term = idTermMap.get(key);
      if (term != null) {
        assertEquals(term.getTermYear(), c.getYear());
        assertEquals(term.getSemester(), c.getSemester());
      }
    }
  }

  private Map<Integer, Term> getIdTermMap() {
    final Map<Integer, Term> m = new HashMap<Integer, Term>();
    m.put(461114859, new Term(Semester.Spring, 2007));
    m.put(461114896, new Term(Semester.Spring, 2008));
    m.put(461115048, new Term(Semester.Spring, 2006));
    m.put(461115073, new Term(Semester.Spring, 2008));
    m.put(461115156, new Term(Semester.Spring, 2006));
    m.put(461115212, new Term(Semester.Spring, 2008));
    m.put(461112466, new Term(Semester.Fall, 2007));
    m.put(701393890, new Term(Semester.Fall, 2013));
    m.put(461114042, new Term(Semester.Spring, 2007));
    m.put(701354940, new Term(Semester.Fall, 2013));
    m.put(461114039, new Term(Semester.Spring, 2006));
    m.put(819847887, new Term(Semester.Spring, 2014));
    m.put(701364884, new Term(Semester.Fall, 2013));
    m.put(804529002, new Term(Semester.Spring, 2014));
    m.put(461113981, new Term(Semester.Fall, 2007));
    m.put(461113945, new Term(Semester.Fall, 2006));
    m.put(701364898, new Term(Semester.Fall, 2013));
    m.put(807939667, new Term(Semester.Spring, 2014));
    m.put(461113925, new Term(Semester.Spring, 2008));
    m.put(804282688, new Term(Semester.Spring, 2014));
    m.put(701354962, new Term(Semester.Fall, 2013));
    m.put(461114124, new Term(Semester.Spring, 2006));
    m.put(461114193, new Term(Semester.Spring, 2007));
    m.put(461114253, new Term(Semester.Spring, 2008));
    m.put(461114291, new Term(Semester.Fall, 2006));
    m.put(461114305, new Term(Semester.Spring, 2007));
    m.put(461114313, new Term(Semester.Spring, 2008));
    m.put(703849100, new Term(Semester.Fall, 2013));
    m.put(701399027, new Term(Semester.Fall, 2013));
    m.put(806030902, new Term(Semester.Spring, 2014));
    m.put(701345945, new Term(Semester.Fall, 2013));
    m.put(461113687, new Term(Semester.Spring, 2007));
    m.put(461113613, new Term(Semester.Spring, 2006));
    m.put(461113565, new Term(Semester.Fall, 2007));
    m.put(461113546, new Term(Semester.Spring, 2008));
    m.put(704083893, new Term(Semester.Fall, 2013));
    m.put(806030953, new Term(Semester.Spring, 2014));
    m.put(461113154, new Term(Semester.Spring, 2007));
    m.put(701399006, new Term(Semester.Fall, 2013));
    m.put(461123181, new Term(Semester.Fall, 2007));
    m.put(461112960, new Term(Semester.Spring, 2006));
    m.put(461112941, new Term(Semester.Fall, 2007));
    m.put(804555659, new Term(Semester.Spring, 2014));
    m.put(461112891, new Term(Semester.Spring, 2008));
    m.put(461112610, new Term(Semester.Spring, 2006));
    m.put(701398996, new Term(Semester.Fall, 2013));
    m.put(461112495, new Term(Semester.Fall, 2006));
    m.put(701354927, new Term(Semester.Fall, 2013));
    m.put(805311547, new Term(Semester.Spring, 2014));
    m.put(805311403, new Term(Semester.Spring, 2014));
    m.put(701354923, new Term(Semester.Fall, 2013));
    m.put(805311012, new Term(Semester.Spring, 2014));
    m.put(808579351, new Term(Semester.Spring, 2014));
    m.put(701354921, new Term(Semester.Fall, 2013));
    m.put(701354906, new Term(Semester.Fall, 2013));
    m.put(704078979, new Term(Semester.Fall, 2013));
    m.put(701354904, new Term(Semester.Fall, 2013));
    m.put(461115306, new Term(Semester.Spring, 2008));
    m.put(461115310, new Term(Semester.Fall, 2006));
    m.put(461115334, new Term(Semester.Fall, 2007));
    m.put(461115368, new Term(Semester.Fall, 2007));
    m.put(701354900, new Term(Semester.Fall, 2013));
    m.put(805328862, new Term(Semester.Spring, 2014));
    m.put(804343791, new Term(Semester.Spring, 2014));
    m.put(704083896, new Term(Semester.Fall, 2013));
    m.put(805310921, new Term(Semester.Spring, 2014));
    m.put(714660829, new Term(Semester.Fall, 2013));
    m.put(805328142, new Term(Semester.Spring, 2014));
    m.put(461115384, new Term(Semester.Spring, 2008));
    m.put(805327956, new Term(Semester.Spring, 2014));
    m.put(461120554, new Term(Semester.Fall, 2007));
    m.put(804524534, new Term(Semester.Spring, 2014));
    m.put(701381047, new Term(Semester.Fall, 2013));
    m.put(805328018, new Term(Semester.Spring, 2014));
    m.put(807939354, new Term(Semester.Spring, 2014));
    m.put(805328800, new Term(Semester.Spring, 2014));
    m.put(716979484, new Term(Semester.Fall, 2013));
    m.put(703067761, new Term(Semester.Fall, 2013));
    m.put(837722074, new Term(Semester.Spring, 2014));
    m.put(805328044, new Term(Semester.Spring, 2014));
    m.put(805328563, new Term(Semester.Spring, 2014));
    m.put(701398981, new Term(Semester.Fall, 2013));
    m.put(804513325, new Term(Semester.Spring, 2014));
    m.put(805328514, new Term(Semester.Spring, 2014));
    m.put(806034944, new Term(Semester.Spring, 2014));
    m.put(704083924, new Term(Semester.Fall, 2013));
    m.put(704945873, new Term(Semester.Fall, 2013));
    m.put(701366885, new Term(Semester.Fall, 2013));
    m.put(461115391, new Term(Semester.Fall, 2007));
    m.put(461115408, new Term(Semester.Fall, 2006));
    m.put(461115451, new Term(Semester.Fall, 2007));
    m.put(461115457, new Term(Semester.Spring, 2006));
    m.put(461115461, new Term(Semester.Spring, 2006));
    m.put(461115467, new Term(Semester.Spring, 2007));
    m.put(461115472, new Term(Semester.Spring, 2008));
    m.put(461115482, new Term(Semester.Fall, 2006));
    m.put(703067778, new Term(Semester.Fall, 2013));
    m.put(806029885, new Term(Semester.Spring, 2014));
    m.put(461115519, new Term(Semester.Fall, 2006));
    m.put(461115666, new Term(Semester.Spring, 2006));
    m.put(461115838, new Term(Semester.Fall, 2007));
    m.put(461115891, new Term(Semester.Spring, 2007));
    m.put(461115940, new Term(Semester.Spring, 2008));
    m.put(461115948, new Term(Semester.Spring, 2008));
    m.put(461123179, new Term(Semester.Spring, 2008));
    m.put(461123174, new Term(Semester.Spring, 2007));
    m.put(461123168, new Term(Semester.Fall, 2006));
    m.put(804555276, new Term(Semester.Spring, 2014));
    m.put(461123165, new Term(Semester.Fall, 2007));
    m.put(461123162, new Term(Semester.Fall, 2006));
    m.put(461123159, new Term(Semester.Spring, 2008));
    m.put(461122977, new Term(Semester.Spring, 2008));
    m.put(461122909, new Term(Semester.Fall, 2006));
    m.put(461122892, new Term(Semester.Spring, 2007));
    m.put(461122847, new Term(Semester.Fall, 2007));
    m.put(461122823, new Term(Semester.Fall, 2006));
    m.put(700513668, new Term(Semester.Fall, 2013));
    m.put(806024888, new Term(Semester.Spring, 2014));
    m.put(461114053, new Term(Semester.Spring, 2008));
    m.put(804555174, new Term(Semester.Spring, 2014));
    m.put(461115974, new Term(Semester.Spring, 2008));
    m.put(826599127, new Term(Semester.Spring, 2014));
    m.put(461120614, new Term(Semester.Spring, 2008));
    m.put(804529209, new Term(Semester.Spring, 2014));
    m.put(701399051, new Term(Semester.Fall, 2013));
    m.put(819827827, new Term(Semester.Spring, 2014));
    m.put(703849607, new Term(Semester.Fall, 2013));
    m.put(461123197, new Term(Semester.Fall, 2006));
    m.put(806620060, new Term(Semester.Spring, 2014));
    m.put(806031415, new Term(Semester.Spring, 2014));
    m.put(804533115, new Term(Semester.Spring, 2014));
    m.put(461115995, new Term(Semester.Fall, 2007));
    m.put(461116019, new Term(Semester.Spring, 2008));
    m.put(703885884, new Term(Semester.Fall, 2013));
    m.put(461116137, new Term(Semester.Fall, 2007));
    m.put(461116162, new Term(Semester.Fall, 2006));
    m.put(461116254, new Term(Semester.Fall, 2007));
    m.put(461116329, new Term(Semester.Spring, 2006));
    m.put(461116547, new Term(Semester.Spring, 2007));
    m.put(461116707, new Term(Semester.Spring, 2008));
    m.put(461116811, new Term(Semester.Spring, 2006));
    m.put(461123189, new Term(Semester.Spring, 2008));
    m.put(461120546, new Term(Semester.Spring, 2006));
    m.put(461117510, new Term(Semester.Fall, 2007));
    m.put(461117846, new Term(Semester.Fall, 2006));
    m.put(461117874, new Term(Semester.Fall, 2007));
    m.put(461117949, new Term(Semester.Spring, 2006));
    m.put(461118087, new Term(Semester.Spring, 2008));
    m.put(461118135, new Term(Semester.Fall, 2006));
    m.put(461118177, new Term(Semester.Spring, 2006));
    m.put(461118280, new Term(Semester.Spring, 2007));
    m.put(461118419, new Term(Semester.Spring, 2008));
    m.put(461118583, new Term(Semester.Spring, 2008));
    m.put(461118594, new Term(Semester.Spring, 2007));
    m.put(461118656, new Term(Semester.Fall, 2007));
    m.put(461118733, new Term(Semester.Spring, 2008));
    m.put(461118785, new Term(Semester.Fall, 2006));
    m.put(461119003, new Term(Semester.Fall, 2006));
    m.put(703881890, new Term(Semester.Fall, 2013));
    m.put(701345916, new Term(Semester.Fall, 2013));
    m.put(804507907, new Term(Semester.Spring, 2014));
    m.put(723185348, new Term(Semester.Fall, 2013));
    m.put(805299824, new Term(Semester.Spring, 2014));
    m.put(461111069, new Term(Semester.Spring, 2006));
    m.put(461110417, new Term(Semester.Spring, 2008));
    m.put(461110404, new Term(Semester.Spring, 2006));
    m.put(461110401, new Term(Semester.Fall, 2007));
    m.put(461110398, new Term(Semester.Spring, 2008));
    m.put(701200204, new Term(Semester.Fall, 2013));
    m.put(852681397, new Term(Semester.Spring, 2014));
    m.put(809297954, new Term(Semester.Spring, 2014));
    m.put(461120538, new Term(Semester.Spring, 2007));
    m.put(461120446, new Term(Semester.Fall, 2006));
    m.put(701366893, new Term(Semester.Fall, 2013));
    m.put(843411759, new Term(Semester.Spring, 2014));
    m.put(804503889, new Term(Semester.Spring, 2014));
    m.put(461114362, new Term(Semester.Spring, 2008));
    m.put(461110345, new Term(Semester.Spring, 2008));
    m.put(461110306, new Term(Semester.Spring, 2007));
    m.put(808581977, new Term(Semester.Spring, 2014));
    m.put(461110263, new Term(Semester.Fall, 2007));
    m.put(461110238, new Term(Semester.Fall, 2006));
    m.put(461114447, new Term(Semester.Fall, 2007));
    m.put(461114556, new Term(Semester.Spring, 2007));
    m.put(461119526, new Term(Semester.Spring, 2008));
    m.put(461114619, new Term(Semester.Spring, 2008));
    m.put(703067772, new Term(Semester.Fall, 2013));
    m.put(852691737, new Term(Semester.Spring, 2014));
    m.put(461109738, new Term(Semester.Fall, 2006));
    m.put(461119727, new Term(Semester.Spring, 2007));
    m.put(461109939, new Term(Semester.Spring, 2006));
    m.put(461110086, new Term(Semester.Fall, 2007));
    m.put(804533076, new Term(Semester.Spring, 2014));
    m.put(703849662, new Term(Semester.Fall, 2013));
    m.put(461120407, new Term(Semester.Fall, 2007));
    m.put(461120401, new Term(Semester.Spring, 2007));
    m.put(461120399, new Term(Semester.Spring, 2006));
    m.put(461120382, new Term(Semester.Fall, 2007));
    m.put(461120197, new Term(Semester.Fall, 2006));
    m.put(461119816, new Term(Semester.Fall, 2007));
    m.put(806026057, new Term(Semester.Spring, 2014));
    m.put(461120619, new Term(Semester.Spring, 2007));
    m.put(701366904, new Term(Semester.Fall, 2013));
    m.put(461122820, new Term(Semester.Spring, 2007));
    m.put(461122814, new Term(Semester.Spring, 2006));
    m.put(461122773, new Term(Semester.Fall, 2007));
    m.put(461122715, new Term(Semester.Fall, 2006));
    m.put(701374888, new Term(Semester.Fall, 2013));
    m.put(461120622, new Term(Semester.Spring, 2008));
    m.put(461120626, new Term(Semester.Fall, 2006));
    m.put(703849610, new Term(Semester.Fall, 2013));
    m.put(461120636, new Term(Semester.Fall, 2007));
    m.put(712909642, new Term(Semester.Fall, 2013));
    m.put(461120640, new Term(Semester.Spring, 2006));
    m.put(461120649, new Term(Semester.Spring, 2007));
    m.put(461120689, new Term(Semester.Spring, 2008));
    m.put(806033899, new Term(Semester.Spring, 2014));
    m.put(705613890, new Term(Semester.Fall, 2013));
    m.put(461120777, new Term(Semester.Fall, 2007));
    m.put(806609521, new Term(Semester.Spring, 2014));
    m.put(461121372, new Term(Semester.Spring, 2008));
    m.put(805328216, new Term(Semester.Spring, 2014));
    m.put(806032889, new Term(Semester.Spring, 2014));
    m.put(461119848, new Term(Semester.Spring, 2007));
    m.put(461119906, new Term(Semester.Spring, 2007));
    m.put(701393916, new Term(Semester.Fall, 2013));
    m.put(461119957, new Term(Semester.Fall, 2007));
    m.put(804529017, new Term(Semester.Spring, 2014));
    m.put(804524189, new Term(Semester.Spring, 2014));
    m.put(701377886, new Term(Semester.Fall, 2013));
    m.put(701380926, new Term(Semester.Fall, 2013));
    m.put(461114632, new Term(Semester.Spring, 2006));
    m.put(701380934, new Term(Semester.Fall, 2013));
    m.put(461121354, new Term(Semester.Spring, 2008));
    m.put(804529583, new Term(Semester.Spring, 2014));
    m.put(804555512, new Term(Semester.Spring, 2014));
    m.put(711831189, new Term(Semester.Fall, 2013));
    m.put(461119960, new Term(Semester.Fall, 2006));
    m.put(461119994, new Term(Semester.Fall, 2006));
    m.put(461120043, new Term(Semester.Fall, 2007));
    m.put(461120088, new Term(Semester.Fall, 2007));
    m.put(461120090, new Term(Semester.Fall, 2006));
    m.put(461120095, new Term(Semester.Spring, 2007));
    m.put(461120184, new Term(Semester.Fall, 2006));
    m.put(703881884, new Term(Semester.Fall, 2013));
    m.put(461121345, new Term(Semester.Spring, 2008));
    m.put(461121330, new Term(Semester.Fall, 2006));
    m.put(701398891, new Term(Semester.Fall, 2013));
    m.put(701393951, new Term(Semester.Fall, 2013));
    m.put(461114685, new Term(Semester.Spring, 2006));
    m.put(461121203, new Term(Semester.Spring, 2006));
    m.put(461121192, new Term(Semester.Spring, 2006));
    m.put(461121080, new Term(Semester.Spring, 2008));
    m.put(804518898, new Term(Semester.Spring, 2014));
    m.put(461121065, new Term(Semester.Fall, 2007));
    m.put(461110235, new Term(Semester.Fall, 2007));
    m.put(461110233, new Term(Semester.Fall, 2006));
    m.put(461110108, new Term(Semester.Fall, 2006));
    m.put(703876888, new Term(Semester.Fall, 2013));
    m.put(805328453, new Term(Semester.Spring, 2014));
    m.put(701393936, new Term(Semester.Fall, 2013));
    m.put(461120912, new Term(Semester.Spring, 2007));
    m.put(461120796, new Term(Semester.Spring, 2007));
    return m;
  }
}
