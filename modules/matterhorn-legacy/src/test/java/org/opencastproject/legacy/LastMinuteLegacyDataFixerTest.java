/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.TestSuiteEnabler;
import org.opencastproject.util.data.Collections;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.WarehouseBuilding;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseDatabase;
import org.opencastproject.warehouse.WarehouseRoom;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

/**
 * @author John Crossman
 */
@Ignore
public class LastMinuteLegacyDataFixerTest {

  private WarehouseCourse course2014B7049;
  private List<WarehouseCourse> fixed;
  private final WarehouseDatabase warehouseDatabase;

  public LastMinuteLegacyDataFixerTest() {
//    this.warehouseDatabase = WarehouseUtils.getWarehouseDatabase();
    throw new UnsupportedOperationException("WarehouseDatabase needs to be initialized here");

  }

  @BeforeClass
  public static void beforeClass() {
    final TestSuiteEnabler enabler = new TestSuiteEnabler("warehouseDatabaseTests");
    assumeTrue(enabler.getTestSuiteName() + " disabled", enabler.isTestSuiteEnabled());
  }

  @Before
  public void before() {
    course2014B7049 = getWarehouse(new CourseKey(2014, Semester.Spring, 7049));
    final List<WarehouseCourse> list = Collections.list(course2014B7049);
    final LastMinuteLegacyDataFixer fixer = new LastMinuteLegacyDataFixer(list);
    fixed = fixer.getFixed();
  }

  @Test
  public void testGet2010Spring69171() {
    assertEquals("2014B7049", CourseUtils.getSeriesId(course2014B7049));
    assertTrue(fixed.contains(course2014B7049));
    assertEquals(805299824, course2014B7049.getITunesVideoId().intValue());
    assertEquals("-XXv-cvA_iCwnFP6YVN3eUvxaozN0_Og", course2014B7049.getYouTubePlaylistId());
  }

  @Test
  public void test2008D16072() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2008, Semester.Fall, 16072));
    assertEquals(c.getITunesAudioId().intValue(), 461123874);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Cognitive_Science_C127__Psychology_C127_Fall_2008_Audio__webcast.rss"));
  }

  @Test
  public void test2008D76145() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2008, Semester.Fall, 76145));
    assertEquals(c.getITunesAudioId().intValue(), 461123850);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Public_Health_245_Fall_2008_Audio__webcast.rss"));
  }

  @Test
  public void test2009B18215() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2009, Semester.Spring, 18215));
    assertEquals(c.getITunesAudioId().intValue(), 461123238);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Demography_C175__001__Economics_C175__001_Spring_2009_Audio__webcast.rss"));
  }

  @Test
  public void test2009B22669() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2009, Semester.Spring, 22669));
    assertEquals(c.getITunesAudioId().intValue(), 461123238);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Demography_C175__001__Economics_C175__001_Spring_2009_Audio__webcast.rss"));
  }

  @Test
  public void test2009B29304() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2009, Semester.Spring, 29304));
    assertEquals(c.getITunesAudioId().intValue(), 461123850);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Demography_C175__001__Economics_C175__001_Spring_2009_Audio__webcast.rss"));
  }

  @Test
  public void test2009B41009() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2009, Semester.Spring, 41009));
    assertEquals(c.getITunesAudioId(), null);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/industrial_engin_and_oper_research_131_Spring_2009_Audio__itunes.rss"));
  }

  @Test
  public void test2009B81852() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2009, Semester.Spring, 81852));
    assertEquals(c.getITunesAudioId().intValue(), 461123891);
    assertEquals(c.getITunesVideoId().intValue(), 461123880);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Sociology_150A__001_Spring_2009_Audio__webcast.rss"));
    assertEquals(c.getVideoFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Sociology_150A__001_Spring_2009_Video__webcast.rss"));
  }

  @Test
  public void test2010B29401() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2010, Semester.Spring, 29401));
    assertEquals(c.getITunesAudioId().intValue(), 461123652);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Environ_Sci__Policy__and_Management_114__001_Spring_2010_Audio__webcast.rss"));
  }

  @Test
  public void test2010B51962() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2010, Semester.Spring, 51962));
    assertEquals(c.getITunesAudioId().intValue(), 461123867);
    assertEquals(c.getITunesVideoId().intValue(), 354822728);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Audio__webcast.rss"));
    assertEquals(c.getVideoFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Video__webcast.rss"));
  }

  @Test
  public void test2010B52013() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2010, Semester.Spring, 52013));
    assertEquals(c.getITunesAudioId().intValue(), 461123864);
    assertEquals(c.getITunesVideoId().intValue(), 461123965);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_140D__001_Spring_2010_Audio__webcast.rss"));
    assertEquals(c.getVideoFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_140D__001_Spring_2010_Video__webcast.rss"));
  }

  @Test
  public void test2010B69390() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2010, Semester.Spring, 69390));
    assertEquals(c.getITunesAudioId().intValue(), 461123864);
    assertEquals(c.getITunesVideoId().intValue(), 461123867);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Audio__webcast.rss"));
    assertEquals(c.getVideoFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/Letters_and_Science_C70V__001__Physics_C10__001_Spring_2010_Video__webcast.rss"));
  }

  @Test
  public void test2010B76235() {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2010, Semester.Spring, 76235));
    assertNull(c);
  }

  @Test
  public void test2010D25491() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2010, Semester.Fall, 25491));
    assertEquals(c.getITunesVideoId(), null);
    assertEquals(c.getITunesAudioId(), null);
    assertEquals(c.getVideoFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/electrical_engineering_141_Fall_2010_Video__itunes.rss"));
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/electrical_engineering_141_Fall_2010_Audio__itunes.rss"));
  }

  @Test
  public void test2012B51806() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2012, Semester.Spring, 51806));
    assertEquals(c.getITunesAudioId().intValue(), 498093212);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/rss/espm_c11_Spring_2012_Audio__itunes.rss"));
  }

  @Test
  public void test2014B66724() throws MalformedURLException {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2014, Semester.Spring, 66724));
    assertEquals(c.getITunesAudioId(), null);
    assertEquals(c.getAudioFeedRSS(), new URL("http://wbe-itunes.berkeley.edu/media/common/courses/spring_2014/rss/peace_and_conflict_studies_94_001_audio.rss"));
  }

  @Test
  public void test2014B7049() {
    final WarehouseCourse c = warehouseDatabase.getCourse(new CourseKey(2014, Semester.Spring, 7049));
    assertEquals(c.getITunesVideoId().intValue(), 805299824);
    assertEquals(c.getYouTubePlaylistId(), "-XXv-cvA_iCwnFP6YVN3eUvxaozN0_Og");
  }

  private static WarehouseCourse getWarehouse(final CourseKey courseKey) {
    return new WarehouseCourse(new CanonicalCourse(courseKey.getCcn(), "Name", "Title"), courseKey.getYear(),
            courseKey.getSemester(), "catalogId", "departmentName", "section", Collections.list(DayOfWeek.Wednesday),
            new WarehouseRoom(WarehouseBuilding.Barker, "200"), "description", new String[] {"instructors"},
            "youTubePlaylistId", new CapturePreferences(), AssociatedLicense.creative_commons);
  }

}
