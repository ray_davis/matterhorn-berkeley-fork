/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public interface LegacyFileProvider {

  /**
   * @return single file with all mediaPackage XMLs of Spring 2014
   */
  Map<String, Set<RecordingMediaPackage>> getSpring2014RecordingsFromMediaPackage() throws IOException;

  /**
   * Keys in the map are audio and video ids extracted from file.
   * @return audio and video collection data scraped form iTunesU and dumped in CSV
   */
  Map<Integer, ITunesCollection> getITunesCollectionMap() throws IOException;

  /**
   * @return contents of http://webcast.berkeley.edu/itunesu_podcasts.js after a little massaging of data.
   */
  File getWebcastBerkeleyLegacyJSON();

}
