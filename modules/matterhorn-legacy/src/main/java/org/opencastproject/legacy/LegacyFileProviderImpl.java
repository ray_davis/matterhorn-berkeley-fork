/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import org.opencastproject.util.env.EnvironmentUtil;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public class LegacyFileProviderImpl extends AbstractLegacyFileProvider {

  public LegacyFileProviderImpl() throws IOException {
    super();
  }

  @Override
  public Map<String, Set<RecordingMediaPackage>> getSpring2014RecordingsFromMediaPackage() throws IOException {
    final File file = EnvironmentUtil
            .getFileUnderFelixHome("etc/data/webcast_warehouse/2014-Spring_ALL_mediapackage_xml.txt");
    return getSeriesIdMap(file);
  }

  @Override
  public Map<Integer, ITunesCollection> getITunesCollectionMap() throws IOException {
    final File csv = EnvironmentUtil
            .getFileUnderFelixHome("etc/data/webcast_warehouse/UCBerkeley-iTunesU-Collections-stripped.csv");
    return getITunesCollectionMap(csv);
  }

  @Override public File getWebcastBerkeleyLegacyJSON() {
    return EnvironmentUtil.getFileUnderFelixHome("etc/data/webcast_warehouse/webcast-berkeley-edu-itunesu_podcasts.js");
  }

}
