/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.legacy;

import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.PlaylistItem;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.google.youtube.YouTubeAPIVersion3Service;
import org.opencastproject.google.youtube.YouTubeUtils;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.env.EnvironmentUtil;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.Recording;
import org.opencastproject.warehouse.WarehouseCourse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author John Crossman
 */
public final class LegacyUtils {

  private static final Logger logger = LoggerFactory.getLogger(LegacyUtils.class);

  private LegacyUtils() {
  }

  static Set<Recording> getRecordingSet(final Map<String, Set<RecordingMediaPackage>> map, final String key) {
    final Set<RecordingMediaPackage> set = map.get(key);
    final Set<Recording> result;
    if (set == null) {
      result = null;
    } else {
      result = new HashSet<Recording>();
      for (final RecordingMediaPackage next : set) {
        result.add(next.getRecording());
      }
    }
    return result;
  }

  static Recording getCorresponding(final Set<Recording> recordings, final String title, final String videoId) {
    if (recordings != null) {
      for (final Recording recording : recordings) {
        if (videoId.equals(recording.getYouTubeVideoId()) || StringUtils.equalsIgnoreCase(recording.getTitle(), title)) {
          return recording;
        }
      }
    }
    return null;
  }

  static void putSpring2014Recordings(final Map<CourseKey, WarehouseCourse> map,
          final Map<String, Set<RecordingMediaPackage>> spring2014Recordings,
          final ProvidesCourseCatalogData catalogDataDAO)
          throws IOException {
    final YouTubeAPIVersion3Service youTubeService = YouTubeUtils.getYouTubeAPIVersion3Service();
    for (final WarehouseCourse next : map.values()) {
      final String playlistId = next.getYouTubePlaylistId();
      final Playlist playlist = playlistId == null ? null : youTubeService.getPlaylistById(playlistId);
      final String seriesId = CourseUtils.getSeriesId(next);
      final Set<Recording> legacyRecordings = getRecordingSet(spring2014Recordings, seriesId);
      if (playlist != null) {
        final List<PlaylistItem> playlistItems = youTubeService.getPlaylistItems(playlistId);
        for (final PlaylistItem item : playlistItems) {
          final String videoId = item.getContentDetails().getVideoId();
          final String itemTitle = item.getSnippet().getTitle();
          final Recording r = getCorresponding(legacyRecordings, itemTitle, videoId);
          final String title = (r == null) ? itemTitle : r.getTitle();
          final Date start = (r == null) ? null : r.getRecordingStart();
          final Date end = (r == null) ? null : r.getRecordingEnd();
          final Integer length = (r == null) ? null : r.getTrimmedLengthSeconds();
          final Recording recording = new Recording(title, item.getSnippet().getDescription(), videoId, start, end, length, null);
          next.addRecording(recording);
        }
      } else if (legacyRecordings != null) {
        // Course with no YouTube videos gets recordings (if any) from legacy data dump
        next.setRecordings(legacyRecordings);
      }
      logger.info(next.toString() + " has " + next.getRecordings().size() + " recordings");
    }
    final Map<Integer, CourseData> courseDataMap = getCCNMapSpring2014(catalogDataDAO);
    for (final String seriesId : spring2014Recordings.keySet()) {
      final CourseKey courseKey = CourseUtils.getCourseKey(seriesId);
      if (courseKey != null && !map.containsKey(courseKey)) {
        final Set<RecordingMediaPackage> set = spring2014Recordings.get(seriesId);
        if (set.size() > 0) {
          MediaPackage mp = null;
          Recording sampleRecording = null;
          final Set<Recording> recordingList = new HashSet<Recording>();
          for (final RecordingMediaPackage next : set) {
            mp = mp == null ? next.getMediaPackage() : mp;
            sampleRecording = sampleRecording == null ? next.getRecording() : sampleRecording;
            recordingList.add(next.getRecording());
          }
          if (mp != null && sampleRecording != null) {
            final CourseData courseData = courseDataMap.get(courseKey.getCcn());
            final boolean hasYouTubeVideo = StringUtils.isNotBlank(sampleRecording.getYouTubeVideoId());
            final List<PlaylistItem> playlistItemList = hasYouTubeVideo ? getPlaylistItemListByVideoId(
                    sampleRecording.getYouTubeVideoId()) : null;
            final AssociatedLicense license = StringUtils.containsIgnoreCase(mp.getLicense(), "Creative Commons")
                    ? AssociatedLicense.creative_commons : AssociatedLicense.all_rights_reserved;
            final CanonicalCourse cc = new CanonicalCourse(courseKey.getCcn(), mp.getTitle(), mp.getTitle());
            final String playlistId = playlistItemList == null
                    ? null
                    : playlistItemList.get(0).getSnippet().getPlaylistId();
            final WarehouseCourse w = new WarehouseCourse(cc, courseKey.getYear(), courseKey.getSemester(),
                    courseData.getCatalogId(), courseData.getDeptName(), courseData.getSection(),
                    courseData.getMeetingDays(), null, null, mp.getContributors(),
                    playlistId, null, license);
            w.setRecordings(recordingList);
            map.put(courseKey, w);
          }
        }
      }
    }
  }

  private static List<PlaylistItem> getPlaylistItemListByVideoId(final String youTubeVideoId) {
    throw new UnsupportedOperationException("YouTubeService no longer supports this operation");
  }

  public static List<String> getSeriesIdIgnorelist() {
    return Collections.list("2010B76235", "2014B52001", "2014B58196", "2014B60170", "2013D7049",
            "0a0c97ba-2f5b-4184-8bb0-7dbf06cf8f2f", "1917c0a7-9f4c-4aa5-96df-70555cc57d89",
            "cd1e4f39-29f0-4171-be8a-958b44315fb6", "f569df74-d5cb-4b13-a7bc-cdf95fd8e727");
  }

  /**
   * In some cases - for example, legacy data, you'll find a "screen" RSS URL that has been associated with "video".
   * Other than WCT-4949, I did not find other such bad-data issues. This method will inspect the supposed video RSS URL
   * to make sure it is not, in fact, "screen" related. Based on result of inspection, the proper setter method of
   * course is used.
   * @param course null not allowed.
   * @param videoURL if null, do nothing
   */
  public static void setVideoFeedRSSCautiously(final WarehouseCourse course, final URL videoURL) {
    if (videoURL != null) {
      final String url = videoURL.toString();
      if (StringUtils.endsWithIgnoreCase(url, "screen.rss")) {
        course.setScreenFeedRSS(videoURL);
      } else {
        course.setVideoFeedRSS(videoURL);
      }
    }
  }

  private static Map<Integer, CourseData> getCCNMapSpring2014(final ProvidesCourseCatalogData catalogDataDAO) {
    throw new UnsupportedOperationException("SQL query getCourseDataForLegacyDataMover is no longer supported.");

//    final Set<CourseData> courseDataSet = catalogDataDAO.getCourseDataForLegacyDataMover(Semester.Spring, 2014);
//    final Map<Integer, CourseData> map = new HashMap<Integer, CourseData>();
//    for (final CourseData courseData : courseDataSet) {
//      map.put(courseData.getCcn(), courseData);
//    }
//    return map;
  }

  public static Integer getYear(final String value) {
    final int year = NumberUtils.toInt(value, 0);
    return (year > 1990 && year < 9999) ? year : null;
  }

  public static Semester getSemester(final String value) {
    final String trimmed = StringUtils.trimToNull(value);
    for (final Semester semester : Semester.values()) {
      if (semester.name().equalsIgnoreCase(trimmed)) {
        return semester;
      }
    }
    return null;
  }


  public static Integer toWhole(final String value, final Integer fallbackValue) {
    final int toInt = NumberUtils.toInt(value, -1);
    return (toInt < 0) ? fallbackValue : new Integer(toInt);
  }

  public static DatabaseCredentials getCourseDatabaseCredentials() throws IOException {
    final File file = EnvironmentUtil.getFileUnderFelixHome(
            "etc/services/org.opencastproject.participation.impl.CourseManagementServiceImpl.properties");
    final Properties p2 = new Properties();
    p2.load(new FileInputStream(file));
    return CourseUtils.getCatalogCredentials(EnvironmentUtil.createEProperties(p2, true));
  }

}
