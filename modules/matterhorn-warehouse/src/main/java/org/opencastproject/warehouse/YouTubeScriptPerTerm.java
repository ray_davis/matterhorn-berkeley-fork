package org.opencastproject.warehouse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.google.youtube.GoogleServicesFactory;
import org.opencastproject.google.youtube.YouTubeAPIVersion3Service;
import org.opencastproject.google.youtube.YouTubeAPIVersion3ServiceImpl;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.env.EProperties;
import org.opencastproject.util.env.EnvironmentUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.opencastproject.warehouse.synchronization.WarehouseSynchronizerServiceImpl.getWarehouseDatabase;

public abstract class YouTubeScriptPerTerm {

  protected final String lb = CharUtils.toString('\n');
  protected final String hr = lb.concat(lb).concat("---------------------------------------------").concat(lb);

  private YouTubeAPIVersion3Service youTubeService;
  protected final WarehouseService warehouseService;
  protected final Term term;

  protected YouTubeScriptPerTerm(Term term) throws IOException {
    this.term = term;
    warehouseService = new WarehouseServiceImpl(getWarehouseDatabase());
  }

  YouTubeAPIVersion3Service youTubeService() throws IOException {
    if (youTubeService == null) {
      String relativePath = "etc/services/org.opencastproject.publication.youtube.YouTubeV3PublicationServiceImpl.properties";
      final File file = EnvironmentUtil.getFileUnderFelixHome(relativePath);
      FileInputStream inputStream = null;
      try {
        inputStream = new FileInputStream(file);
        final Properties p = new Properties();
        p.load(inputStream);
        final EProperties properties = EnvironmentUtil.createEProperties(p, true);
        //
        youTubeService = new YouTubeAPIVersion3ServiceImpl();
        youTubeService.initialize(new GoogleServicesFactory(properties));
      } finally {
        IOUtils.closeQuietly(inputStream);
      }
    }
    return youTubeService;
  }

  protected static Integer requiredIntArg(String arg, String description) {
    arg = StringUtils.trimToNull(arg);
    Integer value = (arg == null || !StringUtils.isNumeric(arg)) ? null : Integer.parseInt(arg);
    if (value == null) {
      throw new IllegalArgumentException("Invalid or missing " + description + " arg: " + arg);
    }
    return value;
  }

  static String requiredStringArg(String arg, String description) {
    arg = StringUtils.trimToNull(arg);
    if (arg == null) {
      throw new IllegalArgumentException("Missing " + description + " arg: " + arg);
    }
    return arg;
  }

  protected String toS(Term term) {
    return term.getSemester().name() + ' ' + term.getYear();
  }

}
