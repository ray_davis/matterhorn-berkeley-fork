/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Semester;

import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
public interface WarehouseDatabase {

  /**
   * Add or update course in warehouse db.
   *
   * @param course null not allowed
   */
  void upsertCourse(WarehouseCourse course);

  /**
   * Select course from warehouse db.
   *
   * @param courseKey null not allowed
   */
  WarehouseCourse getCourse(HasCourseKey courseKey);

  /**
   * Select all courses from warehouse db.
   */
  List<WarehouseCourse> getCourses(Integer year, Semester semester, boolean adjusted);

  /**
   * Select all courses from warehouse db.
   * @param forFeed if false then return ALL courses and ignore include_in_cc_feed column.
   */
  List<WarehouseCourse> getAllCourses(boolean forFeed);

  /**
   * Remove course from warehouse db.
   *
   * @param courseKey null not allowed
   */
  void deleteCourse(HasCourseKey courseKey);

  /**
   * YouTube videos per term.
   */
  Set<String> getYouTubeVideoIds(Integer year, Semester semester);

  /**
   * YouTube videos that have not had privacy status vetted.
   */
  Set<String> getUnadjustedYouTubeVideos(Integer year, Semester semester, int limit, boolean retryPastFailures);

  /**
   * @param year null not allowed
   * @param semester null not allowed
   * @param videoId null not allowed
   * @param includeInFeed if true then recording will be included in feed consumed by downstream services
   */
  void updateIncludeInFeed(Integer year, Semester semester, String videoId, boolean includeInFeed);

  /**
   * Record these YouTube videos as having privacy status vetted.
   */
  void youTubeTreatedBySelenium(Integer year, Semester semester, String videoId);

  /**
   * Get YouTube video ids on which our Selenium code tried to edit metadata.
   */
  Set<String> getBySeleniumStatus(Integer year, Semester semester, boolean treatedBySelenium, boolean includeInFeed);

}
