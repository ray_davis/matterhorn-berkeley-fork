/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.model.Term;
import org.opencastproject.warehouse.WarehouseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

class YouTubeSessionImpl implements YouTubeSession {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private final WarehouseService warehouseService;
  private final Notifier notifier;
  private final int limit;
  private final Config config;

  YouTubeSessionImpl(WarehouseService warehouseService, Notifier notifier, int limit, Config config) {
    this.warehouseService = warehouseService;
    this.notifier = notifier;
    this.limit = limit;
    this.config = config;
  }

  public PrivacyAdjustmentSummary ucBerkeleyOnly(Term term, boolean retryPastFailures) {
    final Set<String> unadjusted = warehouseService.getUnadjustedYouTubeVideos(term.getYear(), term.getSemester(), limit, retryPastFailures);
    final String termDescription = "(Term: " + term.getSemester() + " " + term.getYear() + ")";
    final PrivacyAdjustmentSummary summary;
    if (unadjusted.isEmpty()) {
      logger.info("According to the warehouse database, no YouTube videos need a privacy adjustment. " + termDescription);
      summary = new EmptyAdjustmentSummary(term);
    } else {
      logger.info("Log in to YouTube with the goal of adjusting privacy on " + unadjusted.size() + " YouTube video(s). " + termDescription);
      AdjustYouTubePrivacy operation = new AdjustYouTubePrivacy(term, unadjusted, warehouseService, notifier, config);
      summary = SeUtils.perform(1, operation);
    }
    return summary;
  }
}
