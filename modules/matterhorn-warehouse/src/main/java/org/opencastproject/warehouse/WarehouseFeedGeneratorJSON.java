/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.impl.Column;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public class WarehouseFeedGeneratorJSON {

  enum ColJSON implements Column<String> {
    year, semester, ccn, youTubePlaylist, courses, catalogId, deptName, recordings, lecture,
    youTubeId, audioOnly, recordingStartUTC;

    @Override public Class<String> getType() {
      return String.class;
    }
  }

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  public String toJson(final Set<WarehouseCourse> courses) throws IOException {
    // TODO: Remove this hack per WCT-5194 and fix the SQL query which identifies ALL cross-listings.
    final File file = new File("/opt/matterhorn/catalog-data/cross-listings_WCT-5194.json");
    final Map<HasCourseKey, Set<CourseCatalogEntry>> hardCodedCrossListingMap = WarehouseUtils.loadHardCodedCrossListings(file);
    //
    final List<JSONObject> list = new LinkedList<JSONObject>();
    for (final WarehouseCourse w : courses) {
      if (excludeFromJSON(w, Term.getTermFromProperties())) {
        logger.debug("JSON feed will not include past course with zero recordings: " + CourseUtils.getSeriesId(w));
      } else {
        list.add(getJSONObject(w, w));
        final Set<CourseCatalogEntry> hardCodedCrossListings = hardCodedCrossListingMap.get(w);
        if (hardCodedCrossListings != null) {
          for (final CourseCatalogEntry next : hardCodedCrossListings) {
            list.add(getJSONObject(next, w));
          }
        }
      }
    }
    final JSONObject jsonObject = new JSONObject();
    put(jsonObject, ColJSON.courses, new JSONArray(list));
    return StringUtils.replace(jsonObject.toString(), "},{\"year", "},\n{\"year");
  }

  boolean excludeFromJSON(final WarehouseCourse w, final Term currentTerm) {
    boolean isFromThePast = w.getYear() < currentTerm.getYear()
        || w.getSemester().ordinal() < currentTerm.getSemester().ordinal();
    return isFromThePast && !hasYouTubeVideo(w);
  }

  private boolean hasYouTubeVideo(final WarehouseCourse w) {
    boolean hasYouTubeVideo = false;
    final Set<Recording> recordings = w.getRecordings();
    final boolean empty = CollectionUtils.isEmpty(recordings);
    if (!empty) {
      for (final Recording recording : recordings) {
        if (recording.getYouTubeVideoId() != null) {
          hasYouTubeVideo = true;
          break;
        }
      }
    }
    return hasYouTubeVideo;
  }

  private JSONObject getJSONObject(final CourseCatalogEntry catalogEntry, final WarehouseCourse w) {
    final int year = catalogEntry.getYear();
    final Semester semester = catalogEntry.getSemester();
    final JSONObject entry = new JSONObject();
    put(entry, ColJSON.year, year);
    put(entry, ColJSON.semester, semester.name());
    put(entry, ColJSON.ccn, catalogEntry.getCcn());
    put(entry, ColJSON.deptName, catalogEntry.getDepartmentName());
    put(entry, ColJSON.catalogId, catalogEntry.getCatalogId());
    final RecordingAvailability availability = w.getCapturePreferences() == null
            ? null
            : w.getCapturePreferences().getRecordingAvailability();
    final boolean isPublic = RecordingAvailability.publicCreativeCommons.equals(availability)
            || RecordingAvailability.publicNoRedistribute.equals(availability);
    put(entry, getColumn("public"), isPublic);
    put(entry, ColJSON.youTubePlaylist, w.getYouTubePlaylistId());
    final Set<Recording> set = w.getRecordings();
    boolean audioOnly = true;
    if (set != null && set.size() > 0) {
      final JSONArray array = new JSONArray();
      final List<String> existingShortNames = new LinkedList<String>();
      for (final Recording recording : set) {
        // Exclude recordings from JSON where YouTubeSite videoId is null, per WCT-4923
        if (recording.getYouTubeVideoId() != null) {
          audioOnly = false;
          final JSONObject subEntry = new JSONObject();
          final String shortName = getShortName(recording.getTitle(), existingShortNames, year < 2014);
          existingShortNames.add(shortName);
          put(subEntry, ColJSON.lecture, shortName);
          put(subEntry, ColJSON.youTubeId, recording.getYouTubeVideoId());
          final Date recordingStart = recording.getRecordingStart();
          if (recordingStart != null) {
            put(subEntry, ColJSON.recordingStartUTC, DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.format(recordingStart));
          }
          array.put(subEntry);
        }
      }
      if (array.length() > 0) {
        put(entry, ColJSON.recordings, array);
      }
    }
    put(entry, ColJSON.audioOnly, audioOnly);
    return entry;
  }

  private Column<String> getColumn(final String value) {
    return new Column<String>() {
      @Override public Class<String> getType() {
        return String.class;
      }
      @Override public String name() {
        return value;
      }
    };
  }

  /**
   * Package-local for unit test coverage.
   * @param title recording title is never null
   * @param existingShortNames never null
   * @param isYearBefore2014 never null
   * @return title for JSON feed
   */
  String getShortName(final String title, final List<String> existingShortNames, final boolean isYearBefore2014) {
    final String shortName;
    final String dash = " - ";
    if (isYearBefore2014) {
      final String lectureMarker = "Lecture ";
      final String separator = dash + lectureMarker;
      final String makeUpMarker = " - Make";
      if (title.contains(makeUpMarker)) {
        shortName = "Make" + StringUtils.substringAfter(title, makeUpMarker);
      } else if (title.contains(separator)) {
        shortName = lectureMarker + StringUtils.substringAfterLast(title, separator);
      } else if (title.contains(lectureMarker)) {
        shortName = lectureMarker + StringUtils.substringAfterLast(title, lectureMarker);
      } else {
        shortName = title;
      }
    } else {
      if (title.contains(dash)) {
        shortName = StringUtils.substringAfter(title, dash);
      } else if (title.contains(" 20")) {
        shortName = "20" + StringUtils.substringAfter(title, "20");
      } else {
        shortName = title;
      }
    }
    final boolean duplicate = existingShortNames.contains(shortName);
    if (duplicate) {
      logger.warn("Duplicate lecture title: " + title);
    }
    return duplicate ? shortName + ", Part 2" : shortName;
  }

  private void put(final JSONObject entry, Column<?> column, final Object object) {
    if (object != null) {
      try {
        entry.put(column.name(), object);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
  }

}
