/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.HasCourseKey;

import java.util.Comparator;

/**
 * @author John Crossman
 */
public class JSONFeedComparator implements Comparator<HasCourseKey> {

  @Override
  public int compare(final HasCourseKey c1, final HasCourseKey c2) {
    final int compareTo;
    if (c1.getSemester() == null || c2.getSemester() == null) {
      if (c1.getYear() == c2.getYear()) {
        compareTo = new Integer(c1.getCcn()).compareTo(c2.getCcn());
      } else {
        compareTo = new Integer(c1.getYear()).compareTo(c2.getYear());
      }
    } else {
      compareTo = CourseUtils.getSeriesId(c1).compareTo(CourseUtils.getSeriesId(c2));
    }
    return compareTo;
  }

}
