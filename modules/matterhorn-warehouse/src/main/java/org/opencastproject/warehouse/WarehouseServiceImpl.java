/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Semester;

import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
public class WarehouseServiceImpl implements WarehouseService {

  private final WarehouseDatabase warehouseDatabase;

  public WarehouseServiceImpl(final WarehouseDatabase warehouseDatabase) {
    this.warehouseDatabase = warehouseDatabase;
  }

  @Override
  public List<WarehouseCourse> getAllCourses(final boolean forFeed) {
    return warehouseDatabase.getAllCourses(forFeed);
  }

  @Override
  public void upsertCourse(final WarehouseCourse course) {
    warehouseDatabase.upsertCourse(course);
  }

  @Override
  public List<WarehouseCourse> getCourses(final Integer year, final Semester semester, final boolean adjusted) {
    return warehouseDatabase.getCourses(year, semester, adjusted);
  }

  @Override
  public void deleteCourse(final HasCourseKey courseKey) {
    warehouseDatabase.deleteCourse(courseKey);
  }

  @Override
  public Set<String> getYouTubeVideoIds(Integer year, Semester semester) {
    return warehouseDatabase.getYouTubeVideoIds(year, semester);
  }

  @Override
  public Set<String> getUnadjustedYouTubeVideos(Integer year, Semester semester, int limit, boolean retryPastFailures) {
    return warehouseDatabase.getUnadjustedYouTubeVideos(year, semester, limit, retryPastFailures);
  }

  @Override
  public void updateIncludeInFeed(Integer year, Semester semester, String videoId, boolean includeInFeed) {
    warehouseDatabase.updateIncludeInFeed(year, semester, videoId, includeInFeed);
  }

  @Override
  public void youTubeTreatedBySelenium(Integer year, Semester semester, String videoId) {
    warehouseDatabase.youTubeTreatedBySelenium(year, semester, videoId);
  }

  @Override
  public Set<String> getBySeleniumStatus(Integer year, Semester semester, boolean treatedBySelenium, boolean includeInFeed) {
    return warehouseDatabase.getBySeleniumStatus(year, semester, treatedBySelenium, includeInFeed);
  }

}