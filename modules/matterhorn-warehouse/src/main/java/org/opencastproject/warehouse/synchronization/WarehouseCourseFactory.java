/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.joda.time.DateTime;

import org.opencastproject.episode.api.SearchResult;
import org.opencastproject.episode.api.SearchResultItem;
import org.opencastproject.mediapackage.Attachment;
import org.opencastproject.mediapackage.Catalog;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElement;
import org.opencastproject.mediapackage.MediaPackageElementFlavor;
import org.opencastproject.mediapackage.MediaPackageElements;
import org.opencastproject.mediapackage.Publication;
import org.opencastproject.metadata.dublincore.DCMIPeriod;
import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.metadata.dublincore.EncodingSchemeUtils;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.CourseCatalogService;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.smil.SmilClipElement;
import org.opencastproject.smil.SmilDocument;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.Recording;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseRoom;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.handler.YouTubeWorkspaceUtils;
import org.opencastproject.workspace.api.Workspace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Assembles {@code WarehouseCourse} objects.
 *
 */
public final class WarehouseCourseFactory {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private final CourseCatalogService courseCatalogService;
  private final CourseManagementService courseManagementService;

  /**
   *
   * @return instance of {@code WarehouseCourseFactory}
   */
  static WarehouseCourseFactory instanceOfWarehouseCourseFactory(final CourseCatalogService courseCatalogService,
          CourseManagementService courseManagementService) {
    return new WarehouseCourseFactory(courseCatalogService, courseManagementService);
  }

  private WarehouseCourseFactory(final CourseCatalogService courseCatalogService,
          final CourseManagementService courseManagementService) {
    this.courseCatalogService = courseCatalogService;
    this.courseManagementService = courseManagementService;
  }

  /**
   * Returns a set of {@code WarehouseCourse} representing all cross-listed courses associated with a
   * reference course.
   * @param courses  list of {@code CourseData} each of whose element contains the data specific to that course
   * @param recordings apply to cross-listed courses
   * @param meetingDays apply to cross-listed courses
   * @param warehouseRoom apply to cross-listed courses
   * @param youTubePlaylistId apply to cross-listed courses
   * @param capturePreferences apply to cross-listed courses
   * @param license apply to cross-listed courses
   * @return  may be empty if the referenceCourse was null, otherwise contains at least the referenceCourse
   */
  Set<WarehouseCourse> getWarehouseCourseSet(final List<CourseData> courses, final Set<Recording> recordings,
                                             final List<DayOfWeek> meetingDays, final WarehouseRoom warehouseRoom,
                                             final String youTubePlaylistId, final CapturePreferences capturePreferences,
                                             final AssociatedLicense license) {
    if (CollectionUtils.isEmpty(courses)) {
      return Collections.emptySet();
    }
    final Set<WarehouseCourse> set = new HashSet<WarehouseCourse>();
    for (final CourseData course : courses) {
      final WarehouseCourse warehouseCourse = SynchronizerUtils.getWarehouseCourse(course, recordings, meetingDays,
          warehouseRoom, youTubePlaylistId, capturePreferences, license);
      set.add(warehouseCourse);
    }
    return set;
  }

  /**
   * Returns an instance of {@code WarehouseCourse}.
   *
   * @param seriesCatalog  series data
   * @param capturePreferences most likely from Salesforce
   * @param license never null
   * @param episodes  recordings
   * @param workspace  reference to the Workspace needed to read some of the metadata files
   * @return  a new {@code WarehouseCourse}
   */
  WarehouseCourse newInstance(final DublinCoreCatalog seriesCatalog, final CapturePreferences capturePreferences,
          final AssociatedLicense license, final SearchResult episodes,
          final Workspace workspace) {
    final WarehouseCourse warehouseCourse;
    final String seriesId = getSeriesId(seriesCatalog);
    final CourseKey courseKey = CourseUtils.getCourseKey(seriesId);
    if (courseKey == null) {
      throw new IllegalArgumentException("Series ID does not conform to {year}{termCode}{cnn} convention: " + seriesId);
    } else {
      final String title = seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_TITLE);
      final String description = seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_DESCRIPTION);
      final CourseData courseData = courseCatalogService.getCourse(courseKey);
      if (courseData == null) {
        logger.warn("Materialized view has no match for series: " + seriesId);
        warehouseCourse = getWarehouseCourseWhenUnknownCCN(seriesId, seriesCatalog, capturePreferences, license,
                episodes, workspace, title, description);
      } else {
        String deptName = courseData.getDeptName();
        String catalogId = courseData.getCatalogId();
        CanonicalCourse canonicalCourse = new CanonicalCourse(courseKey.getCcn(), title, description);
        warehouseCourse = new WarehouseCourse(
            canonicalCourse,
            courseData.getYear(),
            courseData.getSemester(),
            catalogId,
            deptName,
            courseData.getSection(),
            courseData.getMeetingDays(),
            SynchronizerUtils.getWarehouseRoom(courseData.getRoom()),
            makeDescription(seriesCatalog),
            CourseUtils.getLecturers(courseData),
            YouTubeWorkspaceUtils.extractYouTubePlaylistId(seriesCatalog),
            capturePreferences,
            license);
        warehouseCourse.setRecordings(makeRecordings(episodes, workspace));
      }
    }
    return warehouseCourse;
  }

  private WarehouseCourse getWarehouseCourseWhenUnknownCCN(final String seriesId, final DublinCoreCatalog seriesCatalog,
          final CapturePreferences capturePreferences, final AssociatedLicense license, final SearchResult episodes,
          final Workspace workspace, final String title, final String description) {
    try {
      final CourseKey courseKey = CourseUtils.getCourseKey(seriesId);
      final WarehouseCourse warehouseCourse;
      if (courseKey == null) {
        logger.warn("Warehousing of course is not possible because seriesId does not match convention: " + seriesId);
        warehouseCourse = null;
      } else {
        final CourseOffering courseOffering = courseManagementService.getCourseOffering(seriesId);
        if (courseOffering == null) {
          logger.warn("Warehousing of course is not possible because materialized view has no match nor does Salesforce: " + seriesId);
          warehouseCourse = null;
        } else {
          logger.debug("Construct warehouse object with Salesforce data only. Dept and catalogId: "
                  + courseOffering.getDeptName() + " " + courseOffering.getCatalogId());
          warehouseCourse = new WarehouseCourse(new CanonicalCourse(courseKey.getCcn(), title, description),
                  courseKey.getYear(),
                  courseKey.getSemester(),
                  courseOffering.getCatalogId(),
                  courseOffering.getDeptName(),
                  courseOffering.getSection(),
                  courseOffering.getMeetingDays(),
                  SynchronizerUtils.getWarehouseRoom(courseOffering.getRoom()),
                  makeDescription(seriesCatalog),
                  CourseUtils.getLecturers(courseOffering),
                  YouTubeWorkspaceUtils.extractYouTubePlaylistId(seriesCatalog),
                  capturePreferences,
                  license);
          warehouseCourse.setRecordings(makeRecordings(episodes, workspace));

        }
      }
      return warehouseCourse;
    } catch (final Exception e) {
      logger.warn("Error occurred when building warehouse data with Matterhorn metadata only: " + seriesId, e);
      throw new RuntimeException(e);
    }
  }

  private Recording newInstance(final SearchResultItem episode, final Workspace workspace) {
    final String episodeId = episode != null && StringUtils.isNotBlank(episode.getId()) ? episode.getId() : null;
    return new Recording(makeRecordingTitle(episode),
                        makeRecordingDescription(episode),
                        makeRecordingYouTubeVideoId(episode),
                        makeRecordingStart(episode, workspace),
                        makeRecordingEnd(episode, workspace),
                        makeRecordingTrimmedLengthSeconds(episode, workspace),
                        episodeId);
  }

  String makeCatalogId(final DublinCoreCatalog seriesCatalog) {
    final String title = seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_TITLE);
    final String regexCaseHasSection = "^.*\\s(.*),.*$";
    final String caseHasSection = parseRegex(regexCaseHasSection, title);
    if (StringUtils.isNotBlank(caseHasSection)) {
      return caseHasSection;
    } else {
      final String regexCaseNoSection = "^.*\\s(.*)\\s-.*$";
      final String caseNoSection = parseRegex(regexCaseNoSection, title);
      if (StringUtils.isNotBlank(caseNoSection)) {
        return caseNoSection;
      } else {
        return null;
      }
    }
  }

  String makeDescription(final DublinCoreCatalog seriesCatalog) {
    return seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_DESCRIPTION);
  }

  private Set<Recording> makeRecordings(final SearchResult episodes, final Workspace workspace) {
    final Set<Recording> recordings = new TreeSet<Recording>(new Recording.RecordingComparator());

    // We reverse the order of the episode list so that in case we have duplicate recordings,
    // the "most recent" is the one added to the set
    final List<SearchResultItem> orderLatestRecordingFirst = new ArrayList<SearchResultItem>(episodes.getItems());
    Collections.reverse(orderLatestRecordingFirst);

    for (final SearchResultItem episode : orderLatestRecordingFirst) {
      recordings.add(newInstance(episode, workspace));
    }
    return recordings;
  }

  private String makeRecordingTitle(final SearchResultItem episode) {
    return episode.getDcTitle();
  }

  //TODO this is currently blank in Matterhorn
  private String makeRecordingDescription(final SearchResultItem episode) {
    return episode.getDcDescription();
  }

  String makeRecordingYouTubeVideoId(final SearchResultItem episode) {
    String youTubeId = null;
    final String channelName = "youtube";
    Publication youtube;
    for (Publication publication : episode.getMediaPackage().getPublications()) {
      if (channelName.equals(publication.getChannel())) {
        youtube = publication;
        URI uri = youtube.getURI();
        youTubeId = StringUtils.substringAfter(uri.toString(), "=");
      }
    }
    return youTubeId;
  }

  private Date makeRecordingStart(final SearchResultItem episode, final Workspace workspace) {
    final MediaPackage mediaPackage = episode.getMediaPackage();
    Date startDate = episode.getMediaPackage().getDate();
    try {
      final DublinCoreCatalog catalog = findDublinCoreCatalog(mediaPackage, MediaPackageElements.EPISODE, workspace);
      if (catalog != null && catalog.hasValue(DublinCore.PROPERTY_TEMPORAL)) {
        final DCMIPeriod temporal = EncodingSchemeUtils.decodePeriod(catalog.getFirst(DublinCore.PROPERTY_TEMPORAL));
        startDate = (temporal != null && temporal.getStart() != null) ? temporal.getStart() : startDate;
      }
    } catch (final WorkflowOperationException e) {
      logger.warn("Failed to extract start date/time of recording", e);
    }
    return startDate;
  }

  private Date makeRecordingEnd(SearchResultItem episode, Workspace workspace) {
    final Date startTime = makeRecordingStart(episode, workspace);
    final Long duration = episode.getMediaPackage().getDuration();
    return (startTime == null || duration == null) ? null : new DateTime(startTime).plus(duration).toDate();
  }

  private Integer makeRecordingTrimmedLengthSeconds(SearchResultItem episode, Workspace workspace) {
    // This is the expected scenario if the media was not trimmed
    Long trimmedLengthSeconds = episode.getMediaPackage().getDuration();
    final Attachment attachment =  episode.getMediaPackage().getAttachment("trim-points");
    if (attachment != null) {
      final InputStream is = readMediaPackageElement(attachment, workspace);
      if (is != null) {
        String attachmentAsString = null;
        try {
          attachmentAsString = IOUtils.toString(is, "UTF-8");
        } catch (IOException e) {
          logger.error("Error loading attachment metadata: {}", e.getMessage());
        } finally {
          IOUtils.closeQuietly(is);
        }
        if (StringUtils.isNotBlank(attachmentAsString)) {
          final SmilDocument smilDoc = SmilDocument.unMarshalSmilDocument(attachmentAsString);
          for (SmilClipElement element : smilDoc.getSmilClipElements()) {
            long elementDuration = element.getClipEnd() - element.getClipBegin();
            trimmedLengthSeconds = (trimmedLengthSeconds < elementDuration) ? trimmedLengthSeconds : elementDuration;
          }
        }
      }
    }
    return trimmedLengthSeconds.intValue();
  }

  private String getSeriesId(final DublinCoreCatalog seriesCatalog) {
    return seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_IDENTIFIER);
  }


  private String parseRegex(String regex, String str) {
    final Pattern pattern = Pattern.compile(regex);
    final Matcher m = pattern.matcher(str);
    if (m.find()) {
      return m.group(1);
    } else {
      return null;
    }
  }

  private DublinCoreCatalog findDublinCoreCatalog(final MediaPackage mediaPackage,
          final MediaPackageElementFlavor catalogType, final Workspace workspace) throws WorkflowOperationException {
    DublinCoreCatalog dc = null;
    for (Catalog catalog : mediaPackage.getCatalogs(catalogType)) {
      InputStream inputStream = null;
      try {
        inputStream = readMediaPackageElement(catalog, workspace);
        dc = inputStream == null ? null : new DublinCoreCatalogImpl(inputStream);
      } catch (final Exception e) {
        final String id = mediaPackage.getIdentifier() == null ? "null" : mediaPackage.getIdentifier().toString();
        logger.error("Failed to load mediaPackage " + id, e);
      } finally {
        IOUtils.closeQuietly(inputStream);
      }
    }
    if (dc == null) {
      final String toString = ToStringBuilder.reflectionToString(mediaPackage, ToStringStyle.MULTI_LINE_STYLE);
      logger.warn("The media package does not contain a Dublin Core Catalog: " + toString);
    }
    return dc;
  }

  private <T extends MediaPackageElement> InputStream readMediaPackageElement(final T mediaPackageElement,
          final Workspace workspace) {
    InputStream is = null;
    try {
      final File file = workspace.get(mediaPackageElement.getURI());
      is = new FileInputStream(file);
    } catch (final Exception e) {
      logger.error("Error loading episode metadata: " + mediaPackageElement, e);
    }
    return is;
  }

}
