/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.selenium;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.model.Term;
import org.opencastproject.warehouse.WarehouseService;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

class AdjustYouTubePrivacy implements Attemptable<PrivacyAdjustmentSummary> {

  private static final int INTOLERABLE_FAILURE_COUNT = 12;
  private static final int INTOLERABLE_NOT_FOUND_COUNT = 3;

  private static final String LOGIN_URL = "https://accounts.google.com/ServiceLogin?continue=https://www.youtube.com";
  private static final String BASE_YOUTUBE_URL = "https://www.youtube.com";

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final List<String> successes = new LinkedList<String>();
  private final List<String> failures = new LinkedList<String>();
  private final List<String> notFound = new LinkedList<String>();
  private final Term term;
  private final LinkedList<String> unadjusted;
  private final WarehouseService warehouseService;
  private final Notifier notifier;
  private final Config config;
  private Se se = null;

  AdjustYouTubePrivacy(Term term, Set<String> unadjusted, WarehouseService warehouseService, Notifier notifier, Config config) {
    this.term = term;
    this.unadjusted = new LinkedList<String>(unadjusted);
    this.warehouseService = warehouseService;
    this.notifier = notifier;
    this.config = config;
  }

  @Override
  public PrivacyAdjustmentSummary perform() {
    logger.info("YouTube privacy will be UC Berkeley only for: " + ArrayUtils.toString(unadjusted));
    if (se == null) {
      se = new Se(LOGIN_URL, config);
    }
    logIn(se);
    while (unadjusted.size() > 0) {
      // Pop next id off the stack
      final String id = unadjusted.removeFirst();
      boolean failure = false;
      // When we're done, the includeInFeed value will be written to the database.
      boolean includeInFeed = false;
      logger.info("Start work on YouTube video " + id);
      try {
        // If the video does not exist (e.g., deleted) then the following will hit TimeoutException
        toWatchPage(se, id);
        // The video exists. Next, adjust privacy-status.
        if (shareUcBerkeleyOnly(se, id)) {
          logger.info("Successfully adjusted privacy-status of video " + id);
          successes.add(id);
          includeInFeed = true;
        } else {
          logger.error("Failed to adjust privacy-status of video " + id);
          failure = true;
        }
      } catch (TimeoutException e) {
        logger.error("Failed to load video " + id + " due to " + e.getClass().getSimpleName() + ": " + e.getMessage());
        notFound.add(id);
      } catch (WebDriverException e) {
        logger.error("Error on YouTube video " + id, e);
        failure = true;
      }
      if (failure) {
        failures.add(id);
      }
      warehouseService.updateIncludeInFeed(term.getYear(), term.getSemester(), id, includeInFeed);
      warehouseService.youTubeTreatedBySelenium(term.getYear(), term.getSemester(), id);
      logger.info("Done with YouTube video " + id);
    }
    return new PrivacyAdjustmentSummary() {
      @Override
      public Term term() {
        return term;
      }
      @Override
      public List<String> successes() {
        return successes;
      }
      @Override
      public List<String> failures() {
        return failures;
      }
      @Override
      public List<String> notFound() {
        return notFound;
      }
    };
  }

  @Override
  public void close() {
    if (se != null) {
      se.quit();
    }
    if (successes.isEmpty() && failures.isEmpty() && notFound.isEmpty()) {
      logger.info("No videos need a privacy-status adjustment.");
    } else {
      String taskName = this.getClass().getSimpleName();
      if (notFound.size() > 0) {
        String subject = taskName + ": " + notFound.size() + " YouTube videos not found";
        String message = "Not found: " + join(notFound);
        logger.error(subject + '\n' + message);
        if (notFound.size() >= INTOLERABLE_NOT_FOUND_COUNT) {
          // Raise the alarm! Ops might need to delete a row from db or retract recording.
          notifier.notifyEngineeringTeam(subject, message);
        }
      }
      // Next, report on the videos we did find.
      String subject = successes.size() + " success(es); " + failures.size() + " failure(s)";
      String message = "Success(es): " + join(successes) + '\n' + '\n' + "Failure(s): " + join(failures);
      if (failures.isEmpty()) {
        logger.info(subject + '\n' + message);
      } else {
        if (failures.size() < INTOLERABLE_FAILURE_COUNT) {
          logger.warn(subject + '\n' + message);
        } else {
          // Ops should take action if failure count is intolerable
          notifier.notifyEngineeringTeam(taskName + ": " + subject, message);
          logger.error(subject + '\n' + message);
        }
      }
    }
  }

  private void logIn(final Se se) {
    String userName = config.get("selenium.youTube.userName");
    logger.info("Begin YouTube login attempt with userName: " + userName);
    By googleEmail = By.id("Email");
    se.type(googleEmail, userName);
    se.click(By.id("next"));
    SeUtils.sleep(2);
    logger.info("Enter CAS credentials");
    se.type(By.id("username"), config.get("selenium.youTube.casUserName"));
    se.type(By.id("password"), config.get("selenium.youTube.casPassword"));
    SeUtils.sleep(2);
    se.click(By.className("button"));
    SeUtils.sleep(2);
    logger.info("YouTube login step is complete where userName: " + userName);
  }

  private boolean shareUcBerkeleyOnly(final Se se, final String videoId) {
    boolean success;
    try {
      logger.info("Begin shareUcBerkeleyOnly(" + videoId + ")");
      String privacyPrivate = "private";
      selectPrivacy(se, privacyPrivate, videoId);
      String shareButtonClassName = "metadata-share-button";
      se.click(By.className(shareButtonClassName));
      logger.info("Successfully clicked on " + shareButtonClassName);
      WebElement container = se.find(
          By.xpath("//div[contains(@class, 'dasher-container')]"),
          By.xpath("//span[contains(@class, 'yt-uix-form-input-checkbox-container')]"));
      String containerClass = container.getAttribute("class");
      if (containerClass.contains("checked")) {
        se.click(By.className("sharing-dialog-cancel"));
        logger.info("Do nothing. Video " + videoId + " is already 'private' and shared with UC Berkeley");
      } else {
        String shareWithDasher = "share_with_dasher";
        String sharingDialogOkay = "sharing-dialog-ok";
        logger.info("Video " + videoId + " requires action on privacy setting. We will click on " + shareWithDasher
            + " and then click on " + sharingDialogOkay);
        se.click(By.name(shareWithDasher));
        se.click(By.className(sharingDialogOkay));
        logger.info("We successfully clicked " + shareWithDasher + " and " + shareWithDasher);
      }
      logger.info("Video " + videoId + " now has privacy setting 'private', shared with UC Berkeley only.");
      String saveChangesClassName = "save-changes-button";
      By saveChangesButton = By.className(saveChangesClassName);
      if (se.find(saveChangesButton).isEnabled()) {
        se.click(saveChangesButton);
        logger.info("Button with class " + saveChangesClassName + " found and clicked");
        SeUtils.perform(5, new Attemptable<String>() {
          @Override
          public String perform() {
            WebElement saveMessage = se.find(By.className("save-error-message"));
            String text = saveMessage == null ? null : saveMessage.getText();
            String expected = "saved";
            if (text == null || !text.contains(expected)) {
              throw new WebDriverException(expected + " is the expected message. But we found: " + text);
            } else {
              logger.info("Message after save: " + text + " [videoId: " + videoId + "]");
            }
            return text;
          }
          @Override
          public void close() {
          }
        });
      } else {
        logger.info("Button with class " + saveChangesClassName + " is NOT enabled");
      }
      // Let the dust settle before we leave the page
      SeUtils.sleep(2);
      // Verify video status on the 'watch' page
      logger.info("We are done adjusting privacy on videoId: " + videoId + "; the final step is to verify results.");
      toWatchPage(se, videoId);
      String privacyIconId = "watch-privacy-icon";
      WebElement element = se.find(By.id(privacyIconId));
      String title = element == null ? null : element.getAttribute("title");
      success = StringUtils.contains(title, "is private")
          && StringUtils.contains(title, "those with access can see it");
      logger.info("The element with id = " + privacyIconId + " has title attribute: " + title);
      logger.info(success ? "Success with " + videoId : "No success with " + videoId);
    } catch (Throwable e) {
      logger.error("Failed to share video with UC Berkeley only: " + videoId, e);
      success = false;
    }
    return success;
  }

  private void selectPrivacy(Se se, String privacyValue, String videoId) {
    logger.info("Begin attempt to selectPrivacy(" + privacyValue + ") on video " + videoId);
    final String privacySelectClassName = "metadata-privacy-input";
    toEditPage(se, videoId);
    Select select = findSelect(se, privacySelectClassName);
    boolean desiredValue = privacyValue.equals(value(selectedOption(select)));
    if (!desiredValue) {
      select.selectByValue(privacyValue);
      if (!isSelected(se, privacySelectClassName, privacyValue)) {
        throw new IllegalStateException(privacyValue + " is not selected in menu with className: " + privacySelectClassName);
      }
    }
    logger.info("YouTube privacy set to " + privacyValue + " for videoId: " + videoId);
  }

  private void toWatchPage(Se se, String videoId) {
    se.load(BASE_YOUTUBE_URL + "/watch?v=" + videoId);
    se.acceptAlert();
    se.find(By.id("watch7-creator-bar"));
    logger.info("Found page element to WATCH video " + videoId);
  }

  private void toEditPage(Se se, String videoId) {
    se.load(BASE_YOUTUBE_URL + "/edit?video_id=" + videoId);
    findSelect(se, "metadata-privacy-input");
    logger.info("Found page element to EDIT video " + videoId);
  }

  private boolean isSelected(Se se, String className, String optionValue) {
    Select select = findSelect(se, className);
    boolean isSelected = select != null && optionValue.equals(value(selectedOption(select)));
    logger.info("Select element " + className + " is " + (isSelected ? "" : "NOT") + " set to optionValue: " + optionValue);
    return isSelected;
  }

  private WebElement selectedOption(Select select) {
    int size = select.getAllSelectedOptions().size();
    if (size != 1) {
      throw new IllegalStateException("The YouTube video privacy select menu has unexpected number of selected options: " + size);
    }
    return select.getAllSelectedOptions().get(0);
  }

  private Select findSelect(Se se, String className) {
    WebElement element = se.find(By.className(className));
    element.click();
    logger.info("Element " + className + " was found and then clicked");
    return new Select(element);
  }

  private String value(WebElement element) {
    return element.getAttribute("value");
  }

  private String join(List<String> list) {
    return StringUtils.join(list, ", ");
  }

}
