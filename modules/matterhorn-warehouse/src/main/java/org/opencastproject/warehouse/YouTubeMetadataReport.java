/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import com.google.api.services.youtube.model.Video;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.google.youtube.YouTubeAPIVersion3Service;
import org.opencastproject.participation.model.Term;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class YouTubeMetadataReport extends YouTubeScriptPerTerm {

  public static void main(String[] args) throws IOException {
    Integer edoTermId = requiredIntArg(args[0], "edoTermId");
    String filePath = requiredStringArg(args[1], "filePath");
    Term term = Term.construct(edoTermId.toString(), null, null);
    if (term == null || term.getYear() == null || term.getSemester() == null) {
      throw new IllegalArgumentException("Invalid edoTermId: " + edoTermId);
    }
    new YouTubeMetadataReport(term).writeReport(filePath);
  }

  private YouTubeMetadataReport(Term term) throws IOException {
    super(term);
  }

  private void writeReport(String filePath) throws IOException {
    Set<String> videoIds = warehouseService.getYouTubeVideoIds(term.getYear(), term.getSemester());
    String b = hr.concat(lb);
    if (videoIds.isEmpty()) {
      b = b.concat("No YouTube video ids for ").concat(toS(term));
    } else {
      YouTubeAPIVersion3Service service = youTubeService();
      List<ReportRow> rows = new LinkedList<ReportRow>();
      List<String> errors = new LinkedList<String>();
      for (String videoId : videoIds) {
        try {
          Video video = service.getVideoById(videoId);
          if (video == null) {
            errors.add("Not found: " + videoId);
          }
          rows.add(new ReportRow(videoId, video));
        } catch (Exception e) {
          errors.add("Error retrieving YouTube video id: " + videoId + '(' + e.getClass().getSimpleName() + ": " + e.getMessage() + ')');
        }
      }
      writeReport(filePath, rows);
      b = b.concat(toS(term)).concat(" YouTube video ids (CSV file):").concat(lb).concat("  ").concat(filePath);
      if (errors.size() > 0) {
        b = b.concat(lb).concat(lb).concat("Error(s) and/or 404(s):");
        for (String error : errors) {
          b = b.concat(lb).concat("  ").concat(error);
        }
      }
    }
    LoggerFactory.getLogger(this.getClass()).info(b.concat(hr));
  }

  private void writeReport(String filePath, List<ReportRow> rows) throws IOException {
    File file = new File(filePath);
    StringBuilder b = new StringBuilder(ReportRow.HEADERS).append(lb);
    for (ReportRow row : rows) {
      b.append(row.toString()).append(lb);
    }
    FileUtils.write(file, b);
  }

  private class ReportRow {
    private static final String HEADERS = "id,title,privacy-status";
    private final Video video;
    private final String id;

    private ReportRow(String id, Video video) {
      this.id = id;
      this.video = video;
    }

    @Override
    public String toString() {
      String dq = "\"";
      String title = video == null
          ? "[ VIDEO NOT FOUND OR PRIVATELY OWNED BY A DIFFERENT ACCOUNT ]"
          : StringUtils.replace(video.getSnippet().getTitle(), dq, "'");
      String privacyStatus = video == null ? "" : video.getStatus().getPrivacyStatus();
      return dq.concat(id).concat(dq).concat(",")
          .concat(dq).concat(title).concat(dq).concat(",")
          .concat(dq).concat(privacyStatus).concat(dq);
    }
  }
}
