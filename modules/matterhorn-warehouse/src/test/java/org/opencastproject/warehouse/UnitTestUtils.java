/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.data.Collections;

import java.util.Date;

/**
 * @author John Crossman
 */
public final class UnitTestUtils {

  static WarehouseCourse createWarehouseCourse(final int year, final Semester semester, final int ccn) {
    final int uid = year + ccn * 2;
    final String unescaped = " with a'postrophe";
    return new WarehouseCourse(new CanonicalCourse(ccn, "name-" + uid + unescaped, "title-" + uid + unescaped),
            year, semester, "" + uid + unescaped, "dept-" + uid + unescaped,
            "section-" + uid + unescaped, Collections.list(DayOfWeek.Tuesday, DayOfWeek.Thursday),
            new WarehouseRoom(WarehouseBuilding.Barker, uid + unescaped), "description-" + uid + unescaped,
            new String[] {"John " + unescaped}, "youTubePlaylistId-" + uid + unescaped, new CapturePreferences(),
            AssociatedLicense.all_rights_reserved);
  }

  static Recording createRecording(final int index, final Date recordingStart, final Date recordingEnd, final boolean assignYouTubeVideoId) {
    final String unescaped = "A'postrophe";
    final String youTubeVideoId = assignYouTubeVideoId ? "youTubeVideoId-" + index + unescaped : null;
    return new Recording(index, "title-" + index + unescaped, "description-" + index + unescaped,
            youTubeVideoId, recordingStart, recordingEnd, index, "a852bfae-9cd3-4822-9364-e7e6557d518" + index);
  }

  private UnitTestUtils() {
  }

}
