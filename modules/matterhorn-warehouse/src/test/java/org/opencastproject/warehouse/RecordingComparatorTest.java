/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * @author John Crossman
 */
public class RecordingComparatorTest {

  @Test
  public void testCompareWhenNullDates() {
    final Set<Recording> set = new TreeSet<Recording>(new Recording.RecordingComparator());
    final String title33 = "Statistics 133 - Lecture 33: No audio first 17 mins of lec.";
    set.add(new Recording(1, title33, null, null, null, null, null, null));
    final String title30 = "Statistics 133- Lecture 30";
    set.add(new Recording(2, title30, null, null, null, null, null, null));
    final String title3 = "Statistics 133 -Lecture 3 : mic turned off - bad";
    set.add(new Recording(3, title3, null, null, null, null, null, null));
    //
    int index = 0;
    for (final Recording next : set) {
      switch (index) {
        case 0:
          assertEquals(title3, next.getTitle());
          break;
        case 1:
          assertEquals(title30, next.getTitle());
          break;
        case 2:
          assertEquals(title33, next.getTitle());
          break;
        default:
          throw new IllegalStateException("Our set only has three entries. How did we get here?");
      }
      index++;
    }
  }


  @Test
  public void testCompareDate() {
    final Set<Recording> set = new TreeSet<Recording>(new Recording.RecordingComparator());
    // The titles do NOT indicate expected order. We're trying to trick comparator.
    final Date today = new Date();
    final String titleTenDaysAgo = "Title 1";
    set.add(new Recording(1, titleTenDaysAgo, null, null, DateUtils.addDays(today, -10), null, null, null));
    final String titleFiveDaysAgo = "Title 2";
    set.add(new Recording(2, titleFiveDaysAgo, null, null, DateUtils.addDays(today, -5), null, null, null));
    final String titleToday = "Title 3";
    set.add(new Recording(3, titleToday, null, null, today, null, null, null));
    //
    int index = 0;
    for (final Recording next : set) {
      switch (index) {
        case 0:
          assertEquals(titleTenDaysAgo, next.getTitle());
          break;
        case 1:
          assertEquals(titleFiveDaysAgo, next.getTitle());
          break;
        case 2:
          assertEquals(titleToday, next.getTitle());
          break;
        default:
          throw new IllegalStateException("Our set only has three entries. How did we get here?");
      }
      index++;
    }
  }

  @Test
  public void testDescriptionHasNumber() {
    final Set<Recording> set = new TreeSet<Recording>(new Recording.RecordingComparator());
    final String lecture26 = "Sociology 150A - Lecture 26: Carpe Diem, Final Thoughts";
    set.add(new Recording(1, lecture26, null, null, null, null, null, null));
    final String lecture5 = "Sociology 150A Lecture 5: Cognitive Biases 3";
    set.add(new Recording(2, lecture5, null, null, null, null, null, null));
    final String lecture14 = "Sociology150A - Lecture14: Social Identity Theory 1";
    set.add(new Recording(3, lecture14, null, null, null, null, null, null));
    //
    int index = 0;
    for (final Recording next : set) {
      switch (index) {
        case 0:
          assertEquals(lecture5, next.getTitle());
          break;
        case 1:
          assertEquals(lecture14, next.getTitle());
          break;
        case 2:
          assertEquals(lecture26, next.getTitle());
          break;
        default:
          throw new IllegalStateException("Our set only has three entries. How did we get here?");
      }
      index++;
    }
  }

  @Test
  public void testTwoDashes() {
    final Set<Recording> set = new TreeSet<Recording>(new Recording.RecordingComparator());
    final String lecture40 = "Chemistry 1A - Lecture 40(really good one)";
    set.add(new Recording(1, lecture40, null, null, null, null, null, null));
    final String lecture12 = "Chemistry 1A - Lecture 12 - beginning of lecture a short 3 m";
    set.add(new Recording(2, lecture12, null, null, null, null, null, null));
    final String lecture6 = "Chemistry 1A - Lecture 6, and more";
    set.add(new Recording(3, lecture6, null, null, null, null, null, null));
    //
    int index = 0;
    for (final Recording next : set) {
      switch (index) {
        case 0:
          assertEquals(lecture6, next.getTitle());
          break;
        case 1:
          assertEquals(lecture12, next.getTitle());
          break;
        case 2:
          assertEquals(lecture40, next.getTitle());
          break;
        default:
          throw new IllegalStateException("Our set only has three entries. How did we get here?");
      }
      index++;
    }
  }

  @Test
  public void testLectureNumberHasLetter() {
    final Set<Recording> set = new TreeSet<Recording>(new Recording.RecordingComparator());
    final String lecture40 = "Chemistry 1A - Lecture 40";
    set.add(new Recording(1, lecture40, null, null, null, null, null, null));
    final String lecture12 = "Chemistry 1A - Lecture 12 - beginning of lecture a short 3 m";
    set.add(new Recording(2, lecture12, null, null, null, null, null, null));
    final String lecture6 = "Chemistry 1A - Lecture 6a";
    set.add(new Recording(3, lecture6, null, null, null, null, null, null));
    //
    int index = 0;
    for (final Recording next : set) {
      switch (index) {
        case 0:
          assertEquals(lecture6, next.getTitle());
          break;
        case 1:
          assertEquals(lecture12, next.getTitle());
          break;
        case 2:
          assertEquals(lecture40, next.getTitle());
          break;
        default:
          throw new IllegalStateException("Our set only has three entries. How did we get here?");
      }
      index++;
    }
  }

}
