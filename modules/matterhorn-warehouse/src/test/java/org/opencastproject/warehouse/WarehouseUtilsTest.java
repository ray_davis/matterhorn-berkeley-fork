/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.opencastproject.participation.CourseUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.UrlSupport;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.env.EnvironmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class WarehouseUtilsTest {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Test
  public void testToURL() {
    assertNull(UrlSupport.toURL(null, logger));
    assertNull(UrlSupport.toURL("a;sjdfhapsfhasjfdasfnd.asdf", logger));
    final String url = "http://wbe-itunes.berkeley.edu/media/common/courses/spring_2014/media/electrical_engineering_127_001/47b34fbe-cb3a-4105-9be0-ebe25d853919_screen.mp4";
    final URL toURL = UrlSupport.toURL(url, logger);
    assertNotNull(toURL);
    assertEquals(url, toURL.toString());
  }

  @Test
  public void testGetLecturersZero() {
    assertEquals(0, CourseUtils.getLecturers(new CourseData()).length);
  }

  @Test
  public void testGetLecturersOne() {
    final CourseData courseData = new CourseData();
    final Set<Participation> set = new HashSet<Participation>();
    set.add(createParticipation("John", "O'Malley"));
    courseData.setParticipationSet(set);
    final String[] lecturers = CourseUtils.getLecturers(courseData);
    assertEquals(1, lecturers.length);
    assertEquals("John O'Malley", lecturers[0]);
  }

  @Test
  public void testGetLecturersMany() {
    final CourseData courseData = new CourseData();
    final Set<Participation> set = new HashSet<Participation>();
    set.add(createParticipation("Judy", "Bernly "));
    set.add(createParticipation(" Doralee", "Rhodes"));
    set.add(createParticipation(" Violet  ", "  Newstead"));
    courseData.setParticipationSet(set);
    final List<String> lecturers = Collections.list(CourseUtils.getLecturers(courseData));
    assertEquals(3, lecturers.size());
    assertTrue(lecturers.contains("Judy Bernly"));
    assertTrue(lecturers.contains("Doralee Rhodes"));
    assertTrue(lecturers.contains("Violet Newstead"));
  }

  private Participation createParticipation(final String firstName, final String lastName) {
    final Instructor instructor = new Instructor();
    instructor.setCalNetUID(firstName + lastName);
    instructor.setFirstName(firstName);
    instructor.setLastName(lastName);
    return new Participation(null, instructor, false);
  }

  @Test
  public void formatTrimmedLengthFormatIsInterval() {
    final int trimmedLengthInMatterhornIsInMilliseconds = 119000;
    final String formattedAsInterval = "0:01:59";
    assertThat(WarehouseUtils.formatTrimmedLength(trimmedLengthInMatterhornIsInMilliseconds)).isEqualTo(
            formattedAsInterval);
  }

  @Test
  public void testVerifyAndConstruct() {
    assertNull(verifyAndConstruct(null));
    assertNull(verifyAndConstruct("   "));
    assertNull(verifyAndConstruct(" This is not my beautiful house  "));
    final String invalidURL = "https://www.imagine-a-world-without-google.com/";
    assertNull(verifyAndConstruct(invalidURL));
    final String url404 = "http://stackoverflow.com/questions/9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999";
    assertNull(verifyAndConstruct(url404));
  }

  private URL verifyAndConstruct(final String url) {
    return UrlSupport.verifyAndConstruct(url, logger);
  }

  @Test
  public void testWarehouseBuildingValueOf() {
    assertNull(WarehouseUtils.getWarehouseBuilding(null));
    assertNull(WarehouseUtils.getWarehouseBuilding("  "));
    assertNull(WarehouseUtils.getWarehouseBuilding("Stanford Hall"));
    final String wheelerAud = "WHEELER AUD";
    final String liKaShing = "LI KA SHING";
    final String valleyLSB = "VALLEY LSB";
    final String wheeler = "WHEELER";
    final String[] names = new String[] {"BARKER", "BARROWS", "BIRGE", "BOALT", "CORY", "DAVIS", "DONNER LAB",
            "DWINELLE", "ETCHEVERRY", "EVANS", "GIANNINI", "GPB", "GSPP", "HAAS", "KROEBER", "LATIMER", "LECONTE",
            "LEWIS", liKaShing, "MCCONE", "MCLAUGHLIN", "MOFFITT", "MORGAN", "MULFORD", "NORTH GATE", "OFF CAMPUS",
            "PIMENTEL", "SODA", "STANLEY", "TAN", "TOLMAN", valleyLSB, wheeler, wheelerAud, "WURSTER"};
    for (final String name : names) {
      final WarehouseBuilding building = WarehouseUtils.getWarehouseBuilding(name);
      assertNotNull("Null return when: " + name, building);
      if (name.equals(wheelerAud)) {
        assertSame(WarehouseBuilding.Wheeler, building);
      } else if (name.equals(liKaShing)) {
        assertSame(WarehouseBuilding.LiKaShing, building);
      } else if (name.equals(valleyLSB)) {
        assertSame(WarehouseBuilding.ValleyLSB, building);
      } else if (name.equals(wheeler)) {
        assertSame(WarehouseBuilding.Wheeler, building);
      }
    }
  }

  @Test
  public void testWarehouseBuildingPerEdoDb() {
    assertSame(WarehouseBuilding.Haas, WarehouseUtils.getWarehouseBuilding("Haas Pavilion"));
    assertSame(WarehouseBuilding.HearstMin, WarehouseUtils.getWarehouseBuilding("Hearst Mining "));
    assertSame(WarehouseBuilding.Moffitt, WarehouseUtils.getWarehouseBuilding("Moffitt Library "));
    assertSame(WarehouseBuilding.Wheeler, WarehouseUtils.getWarehouseBuilding(" Wheeler"));
  }

  @Test
  public void testGetAssociatedRecordingTypeDefault() {
    final WarehouseCourse w = UnitTestUtils.createWarehouseCourse(2015, Semester.Fall, 1);
    w.getCapturePreferences().setRecordingType(null);
    w.setYouTubePlaylistId(null);
    assertEquals(AssociatedRecordingType.audio, WarehouseUtils.getAssociatedRecordingType(w));
  }

  @Test
  public void testGetAssociatedRecordingTypeByRecordingType() {
    final WarehouseCourse warehouseCourse = UnitTestUtils.createWarehouseCourse(2015, Semester.Fall, 1);
    for (final RecordingType recordingType : RecordingType.values()) {
      warehouseCourse.getCapturePreferences().setRecordingType(recordingType);
      final AssociatedRecordingType actual = WarehouseUtils.getAssociatedRecordingType(warehouseCourse);
      switch (recordingType) {
        case audioOnly:
          assertEquals(AssociatedRecordingType.audio, actual);
          break;
        case screencast:
          assertEquals(AssociatedRecordingType.screencast, actual);
          break;
        case videoOnly:
          assertEquals(AssociatedRecordingType.video, actual);
          break;
        case videoAndScreencast:
          assertEquals(AssociatedRecordingType.video_and_screencast, actual);
          break;
        default:
          throw new UnsupportedOperationException("Unknown recordingType: " + recordingType);
      }
    }
  }

  @Test
  public void testSQLFreemarkerSyntax() throws IOException, TemplateException {
    final Configuration configuration = new Configuration();
    final File templateDirectory = EnvironmentUtil.getFileUnderFelixHome("etc/sql/webcast_warehouse");
    configuration.setDirectoryForTemplateLoading(templateDirectory);
    final String[] fileNameArray = new String[] {"insertRecording.sql", "insertWarehouseCourse.sql", "updateRecording.sql", "updateWarehouseCourse.sql"};
    final HashMap<Object, Object> map = new HashMap<Object, Object>();
    map.put(ColStr.course.name(), UnitTestUtils.createWarehouseCourse(2015, Semester.Fall, 1));
    final Date recordingStart = new Date();
    final Date recordingEnd = DateUtils.addMinutes(recordingStart, 50);
    final Recording recording = UnitTestUtils.createRecording(2, recordingStart, recordingEnd, true);
    map.put(ColStr.recording.name(), recording);
    map.put(ColStr.title.name(), recording.getTitle());
    map.put(ColStr.dateParsePattern.name(), "yyyy-MM-dd HH:mmz");
    map.put(ColStr.audience_type.name(), "public");
    map.put(ColStr.recording_type.name(), "video_and_screencast");
    map.put(ColStr.license_type.name(), "creative_commons");
    for (final String fileName : fileNameArray) {
      final Template template = configuration.getTemplate(fileName);
      final StringWriter result = new StringWriter();
      // Syntax checking happens here
      template.process(map, result);
      assertTrue(StringUtils.trimToEmpty(result.toString()).length() > 0);
    }
  }

  @Test
  public void testFallbackRecordingType() throws MalformedURLException {
    final WarehouseCourse w = UnitTestUtils.createWarehouseCourse(2015, Semester.Fall, 1);
    w.getCapturePreferences().setRecordingType(null);
    w.setYouTubePlaylistId(null);
    assertEquals(AssociatedRecordingType.audio, WarehouseUtils.getAssociatedRecordingType(w));
  }

  @Test
  public void testInferRecordingTypeVideo() throws MalformedURLException {
    final WarehouseCourse w = UnitTestUtils.createWarehouseCourse(2015, Semester.Fall, 1);
    w.setYouTubePlaylistId("ajkshfasdufhaw5845");
    assertEquals(AssociatedRecordingType.video, WarehouseUtils.getAssociatedRecordingType(w));
  }

  @Test
  public void testHardCodedCrossListingsMapNull() throws IOException {
    final String[] emptyMapCases = new String[] { "  ", " { \"malformed\": JSON ][ }" };
    for (final String emptyMapCase : emptyMapCases) {
      final File file = File.createTempFile("emptyMapCase", ".json");
      FileUtils.writeStringToFile(file, emptyMapCase);
      final Map<HasCourseKey, Set<CourseCatalogEntry>> map = WarehouseUtils.loadHardCodedCrossListings(file);
      assertEquals("Non-zero received when " + emptyMapCase, 0, map.size());
      assertTrue(file.delete());
    }
  }

  @Test
  public void testHardCodedCrossListingsMap() throws IOException {
    final File file = File.createTempFile("crossListedCCNs", ".json");
    FileUtils.writeStringToFile(file, "{\"crossListedCCNs\":[{\"matterhornSeriesId\":\"2014D12345\", \"crossListedCCN\":54321, \"deptName\":\"CHEM\", \"catalogId\":\"1A\"}, {\"matterhornSeriesId\":\"2014D6789\", \"crossListedCCN\":9876, \"deptName\":\"POL SCI\", \"catalogId\":\"C123\"}]}");
    final Map<HasCourseKey, Set<CourseCatalogEntry>> map = WarehouseUtils.loadHardCodedCrossListings(file);
    assertEquals(2, map.size());
    //
    final Set<CourseCatalogEntry> crossListed12345 = map.get(getWarehouseCourse(2014, Semester.Fall, 12345));
    assertEquals(1, crossListed12345.size());
    final CourseCatalogEntry entry54321 = crossListed12345.iterator().next();
    assertEquals(54321, entry54321.getCcn());
    assertEquals("CHEM", entry54321.getDepartmentName());
    assertEquals("1A", entry54321.getCatalogId());
    //
    final Set<CourseCatalogEntry> crossListed6789 = map.get(getWarehouseCourse(2014, Semester.Fall, 6789));
    assertEquals(1, crossListed6789.size());
    final CourseCatalogEntry entry9876 = crossListed6789.iterator().next();
    assertEquals(9876, entry9876.getCcn());
    assertEquals("POL SCI", entry9876.getDepartmentName());
    assertEquals("C123", entry9876.getCatalogId());
    //
    assertTrue(file.delete());
  }

  @Test
  public void testHardCodedCrossListingsMulitple() throws IOException {
    final File file = File.createTempFile("crossListedCCNs", ".json");
    FileUtils.writeStringToFile(file, "{\"crossListedCCNs\":[{\"matterhornSeriesId\": \"2014D12345\",\"crossListedCCN\": 54321,\"deptName\": \"CHEM\",\"catalogId\": \"1A\"},{\"matterhornSeriesId\": \"2014D12345\",\"crossListedCCN\": 9876,\"deptName\": \"POL SCI\",\"catalogId\": \"C123\"}]}");
    final Map<HasCourseKey, Set<CourseCatalogEntry>> map = WarehouseUtils.loadHardCodedCrossListings(file);
    assertEquals(1, map.size());
    //
    final Set<CourseCatalogEntry> crossListed12345 = map.get(getWarehouseCourse(2014, Semester.Fall, 12345));
    assertEquals(2, crossListed12345.size());
    final Iterator<CourseCatalogEntry> iterator = crossListed12345.iterator();
    //
    final CourseCatalogEntry entry54321 = iterator.next();
    assertEquals(54321, entry54321.getCcn());
    assertEquals("CHEM", entry54321.getDepartmentName());
    assertEquals("1A", entry54321.getCatalogId());
    //
    final CourseCatalogEntry entry9876 = iterator.next();
    assertEquals(9876, entry9876.getCcn());
    assertEquals("POL SCI", entry9876.getDepartmentName());
    assertEquals("C123", entry9876.getCatalogId());
    //
    assertTrue(file.delete());
  }

  private HasCourseKey getWarehouseCourse(final int year, final Semester semester, final int ccn) {
    return new WarehouseCourse(new CanonicalCourse(ccn, "name", "title"), year, semester, null, null, null, null, null, null, null, null, null,null);
  }

}
