/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.schedulableservice.api.SchedulableServiceService;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseService;

import junitparams.JUnitParamsRunner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * A suite of tests focusing on the initialization of {@link WarehouseSynchronizerServiceImpl} prior to
 * performing an execution cycle.
 */
@RunWith(JUnitParamsRunner.class)
public class WarehouseSynchronizerServiceImplInitializationTest {

  private static final String KEY_PERIOD = "org.opencastproject.warehouse.synchronization.WarehouseSynchronizerServiceImpl.schedulingPeriod";
  private WarehouseSynchronizerServiceImpl warehouseSynchronizerServiceImpl = new WarehouseSynchronizerServiceImpl();
  private WarehouseSynchronizerService warehouseSynchronizerService;
  @Mock private SchedulableServiceService schedulableServiceService;
  @Mock private MatterhornCourseService matterhornCourseService;
  @Mock private WarehouseService warehouseService;
  @Mock private Notifier notifier;

  @Test
  public void intializerCreatesNewExecutionEachTimeItRuns() throws Exception {
    final WarehouseSynchronizerState alphaWarehouseSynchronizerState;
    final WarehouseSynchronizerState betaWarehouseSynchronizerState;
    
    warehouseSynchronizerService.callBack();
    alphaWarehouseSynchronizerState = warehouseSynchronizerService.getWarehouseSynchronizerState();
    warehouseSynchronizerService.callBack();
    betaWarehouseSynchronizerState = warehouseSynchronizerService.getWarehouseSynchronizerState();
    
    assertThat(alphaWarehouseSynchronizerState).isNotNull().isNotEqualTo(betaWarehouseSynchronizerState);
  }
  
  @Test
  public void intializerGetsTallyOfTwoMatterhornCourses() {
    
    warehouseSynchronizerService.callBack();
    assertThat(warehouseSynchronizerService.getWarehouseSynchronizerState().getCourseTally()).isEqualTo(2);
  }
  
  @Test
  public void initializerGetsTallyOfThreeWarehouseCourses() {
    warehouseSynchronizerService.callBack();
    assertThat(warehouseSynchronizerService.getWarehouseSynchronizerState().getWarehouseCourseInTally()).isEqualTo(3);
  }

  @Before
  public void instantiateImplAndInjectServices() throws Exception {
    initMocks(this);
    Properties properties = new Properties();
    properties.put(KEY_PERIOD, "1");
    warehouseSynchronizerServiceImpl.setSchedulableServiceService(schedulableServiceService);
    warehouseSynchronizerServiceImpl.setMatterhornCourseService(matterhornCourseService);
    warehouseSynchronizerServiceImpl.setWarehouseService(warehouseService);
    warehouseSynchronizerServiceImpl.setNotifier(notifier);
    warehouseSynchronizerServiceImpl.updatedConfiguration(properties);
    warehouseSynchronizerService = warehouseSynchronizerServiceImpl;
  }
  
  @Before
  public void stubMatterhornCourseRepo() throws Exception {
    final Set<String> courseIds = new HashSet<String>();
    courseIds.add("2");
    courseIds.add("1");
    when(matterhornCourseService.getMatchingSeriesIds(any(Pattern.class))).thenReturn(courseIds);
  }
  
  @Before 
  public void stubWarehouseService() {
    List<WarehouseCourse> warehouseCourses = new ArrayList<WarehouseCourse>();
    final WarehouseCourse alphaWarehouseCourse = new WarehouseCourse(new CanonicalCourse(11111, null, null), 2014,
            Semester.Fall, null, null, null, null, null, null, null, null, null, null);
    warehouseCourses.add(alphaWarehouseCourse);
    final WarehouseCourse betaWarehouseCourse = new WarehouseCourse(new CanonicalCourse(11111, null, null), 2014,
            Semester.Fall, null, null, null, null, null, null, null, null, null, null);
    warehouseCourses.add(betaWarehouseCourse);
    final WarehouseCourse gammaWarehouseCourse = new WarehouseCourse(new CanonicalCourse(11111, null, null), 2014,
            Semester.Fall, null, null, null, null, null, null, null, null, null, null);
    warehouseCourses.add(gammaWarehouseCourse);
    when(warehouseService.getCourses(anyInt(), any(Semester.class), any(Boolean.class))).thenReturn(warehouseCourses);
  }

  @After
  public void tearDown() throws Exception {
  }

}
