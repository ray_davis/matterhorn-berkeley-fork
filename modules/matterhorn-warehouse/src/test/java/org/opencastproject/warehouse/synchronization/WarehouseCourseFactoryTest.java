/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse.synchronization;

import junitparams.JUnitParamsRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.opencastproject.episode.api.SearchResultItem;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.mediapackage.Publication;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreValue;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.CourseCatalogService;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.ConfigurationException;
import org.opencastproject.util.data.Collections;
import org.opencastproject.warehouse.AssociatedLicense;
import org.opencastproject.warehouse.Recording;
import org.opencastproject.warehouse.WarehouseCourse;
import org.opencastproject.warehouse.WarehouseRoom;
import org.opencastproject.workflow.handler.YouTubeWorkspaceUtils;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(JUnitParamsRunner.class)
public class WarehouseCourseFactoryTest {
  private static final String PROPERTY_TITLE = "Public Health 203, 004 - Fall 2013";
  private static final String PROPERTY_TITLE_NO_SECTION = "Rhetoric 135T - Fall 2014";
  private static final String PROPERTY_DESCRIPTION = "Theory of Everything - Judy Bernly, Doralee Rhodes, Violet Newstead";
  private WarehouseCourseFactory warehouseCourseFactory;
  @Mock private DublinCoreCatalog seriesCatalog;
  @Mock private WarehouseCourse warehouseCourse;
  @Mock private SearchResultItem episode;

  @Test
  public void makeCatalogId() {
    when(seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_TITLE)).thenReturn(PROPERTY_TITLE);
    assertThat(warehouseCourseFactory.makeCatalogId(seriesCatalog)).isEqualTo("203");
  }

  @Test
  public void makeDescription() {
    when(seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_DESCRIPTION)).thenReturn(PROPERTY_DESCRIPTION);
    assertThat(warehouseCourseFactory.makeDescription(seriesCatalog)).isEqualTo(PROPERTY_DESCRIPTION);
  }

  @Test
  public void makeYouTubePlaylistId() {
    when(seriesCatalog.get(DublinCoreCatalog.PROPERTY_PUBLISHER)).thenReturn(getPublishers());
    assertThat(YouTubeWorkspaceUtils.extractYouTubePlaylistId(seriesCatalog)).isEqualTo("PL-XXv-cvA_iB1rYQA_WcOLvMO8gCa3JNy");
  }

  @Test
  public void makeRecordingYouTubeVideoId() throws ConfigurationException, MediaPackageException {
    final String youTubeId = "DkGoUBiIvJ0";
    final URI uri = URI.create("http://www.youtube.com/watch?v=" + youTubeId);
    final Publication youtube = mock(Publication.class);
    when(youtube.getChannel()).thenReturn("youtube");
    when(youtube.getURI()).thenReturn(uri);
    final Publication engage = mock(Publication.class);
    when(engage.getChannel()).thenReturn("engage");
    final Publication[] publications = {engage, youtube};
    final MediaPackage mp =  mock(MediaPackage.class);
    when(episode.getMediaPackage()).thenReturn(mp);
    when(mp.getPublications()).thenReturn(publications);
    
    assertThat(warehouseCourseFactory.makeRecordingYouTubeVideoId(episode)).isEqualTo(youTubeId);
  }
  
  @Test
  public void makeRecordingYouTubeVideoIdChoosesLastInList() throws ConfigurationException, MediaPackageException {
    final String youTubeIdAlpha = "DkGoUBiIvJ0";
    final URI uriAlpha = URI.create("http://www.youtube.com/watch?v=" + youTubeIdAlpha);
    final Publication youtubeAlpha = mock(Publication.class);
    when(youtubeAlpha.getChannel()).thenReturn("youtube");
    when(youtubeAlpha.getURI()).thenReturn(uriAlpha);
    
    final String youTubeIdBeta = "DkGoUBiIvJ1";
    final URI uriBeta = URI.create("http://www.youtube.com/watch?v=" + youTubeIdBeta);
    final Publication youtubeBeta = mock(Publication.class);
    when(youtubeBeta.getChannel()).thenReturn("youtube");
    when(youtubeBeta.getURI()).thenReturn(uriBeta);
    
    final Publication engage = mock(Publication.class);
    when(engage.getChannel()).thenReturn("engage");
    final Publication[] publications = {engage, youtubeAlpha, youtubeBeta};
    final MediaPackage mp =  mock(MediaPackage.class);
    when(episode.getMediaPackage()).thenReturn(mp);
    when(mp.getPublications()).thenReturn(publications);
    
    assertThat(warehouseCourseFactory.makeRecordingYouTubeVideoId(episode)).isEqualTo(youTubeIdBeta);
  }
  
  @Test
  public void crossListedWarehouseCoursesReturnsAtMinimumTheReferenceCourse() {
    final List<CourseData> courses = java.util.Collections.emptyList();
    assertThat(getWarehouseCourseSet(warehouseCourse, courses).size()).isEqualTo(0);
  }

  @Test
  public void crossListedWarehouseCoursesStillReturnsTheReferenceCourseWhenCoursesIsNull() {
    assertThat(getWarehouseCourseSet(warehouseCourse, null).size()).isEqualTo(0);
  }
  
  @Test
  public void crossListedWarehouseCoursesReturnsEmptyWhenTheReferenceCourseIsNull() {
    final List<CourseData> courses = java.util.Collections.emptyList();
    assertThat(getWarehouseCourseSet(null, courses)).isEmpty();
  }
  
  @Test
  public void makeCatalogIdFromNoSection() {
    when(seriesCatalog.getFirst(DublinCoreCatalog.PROPERTY_TITLE)).thenReturn(PROPERTY_TITLE_NO_SECTION);
    assertThat(warehouseCourseFactory.makeCatalogId(seriesCatalog)).isEqualTo("135T");
  }
  
  @Before
  public void instantiateImplAndInjectServices() throws Exception {
    initMocks(this);
    final CourseCatalogService courseCatalogService = createMock(CourseCatalogService.class);
    final CourseData courseData = new CourseData();
    courseData.setCanonicalCourse(new CanonicalCourse(12345, "Name", "Title"));
    courseData.setTerm(Term.construct(Semester.Fall, 2013, null, null, null));
    courseData.setDeptName("RHET");
    courseData.setCatalogId("135T");
    courseData.setSection("001");
    courseData.setMeetingDays(Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday));
    courseData.setParticipationSet(new HashSet<Participation>());
    expect(courseCatalogService.getCourse(anyObject(CourseKey.class))).andReturn(courseData).anyTimes();
    final CourseManagementService niceMock = createNiceMock(CourseManagementService.class);
    replay(courseCatalogService, niceMock);
    warehouseCourseFactory = WarehouseCourseFactory.instanceOfWarehouseCourseFactory(courseCatalogService, niceMock);
  }

  private Set<WarehouseCourse> getWarehouseCourseSet(final WarehouseCourse w, final List<CourseData> courses) {
    final Set<Recording> recordings = w == null ? null : w.getRecordings();
    final List<DayOfWeek> meetingDays = w == null ? null : w.getMeetingDays();
    final WarehouseRoom room = w == null ? null : w.getRoom();
    final String youTubePlaylistId = w == null ? null : w.getYouTubePlaylistId();
    final CapturePreferences capturePreferences = w == null ? null : w.getCapturePreferences();
    final AssociatedLicense license = w == null ? null : w.getLicense();
    return warehouseCourseFactory.getWarehouseCourseSet(courses, recordings, meetingDays, room, youTubePlaylistId,
            capturePreferences, license);
  }

  private List<DublinCoreValue> getPublishers() {
    List<DublinCoreValue> values = new ArrayList<DublinCoreValue>();
    values.add(new DublinCoreValue("urn:youtube:com:playlistId:PL-XXv-cvA_iB1rYQA_WcOLvMO8gCa3JNy"));
    values.add(new DublinCoreValue("urn:itunes:com:audio:collectionId:12345"));
    values.add(new DublinCoreValue("urn:itunes:com:video:collectionId:54321"));
    return values;
  }

}
