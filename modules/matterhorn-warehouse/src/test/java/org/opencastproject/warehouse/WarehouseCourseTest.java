/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.data.Collections;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class WarehouseCourseTest {

  @Test
  public void testSetRecordings() {
    final Recording r1 = createRecording(1);
    final Recording r2 = createRecording(2);
    final Recording r3 = createRecording(3);
    final Recording r4 = createRecording(4);
    final WarehouseCourse w = UnitTestUtils.createWarehouseCourse(2015, Semester.Fall, 1);
    w.setRecordings(Collections.set(r1, r2, r3, r4));
    assertEquals(4, w.getRecordings().size());
    w.setRecordings(Collections.set(r2, r4));
    assertEquals(2, w.getRecordings().size());
    w.getRecordings().contains(r2);
    w.getRecordings().contains(r4);
    w.setRecordings(null);
    assertTrue(w.getRecordings().isEmpty());
  }

  @Test
  public void canonicalCourseTitleIsSourcedFromCannonicalCourseNameAndNotTitle() {

    final int ccn = 36425;
    final String salesForceProjectName = "Political Science 3, 001";
    final String seriesTitle = salesForceProjectName + " - Fall 2014";
    final String seriesDescription = "Introduction to Empirical Analysis and Quantitative Methods - Laura Stoker";
    final CanonicalCourse aCanonicalCourse = new CanonicalCourse(ccn, seriesTitle, seriesDescription);
    final WarehouseCourse w = new WarehouseCourse(aCanonicalCourse, 2000 + ccn,
            Semester.Fall, "" + ccn, "dept-" + ccn, "section-" + ccn,
            Collections.list(DayOfWeek.Tuesday, DayOfWeek.Thursday),
            new WarehouseRoom(WarehouseBuilding.Barker, ccn + ""), seriesDescription,
            new String[] {"John Doe"}, "youTubePlaylistId-" + ccn, new CapturePreferences(),
            AssociatedLicense.all_rights_reserved);

    final String title = w.getCanonicalCourse().getName();
    final String description = w.getDescription();
    assertTrue(!title.equals(description));
    assertTrue(title.equals(seriesTitle));
  }


  private Recording createRecording(final int index) {
    final Date date = new Date();
    return new Recording("title-" + index, "description-" + index, "youTubeVideoId-" + index, date,
            DateUtils.addHours(date, index), index * 10000, "a852bfae-9cd3-4822-9364-e7e6557d518f");
  }

}
