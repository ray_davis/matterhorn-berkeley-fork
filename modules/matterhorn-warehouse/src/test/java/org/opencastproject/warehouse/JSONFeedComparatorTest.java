/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.junit.Before;
import org.junit.Test;
import org.opencastproject.participation.model.CourseKey;
import org.opencastproject.participation.model.HasCourseKey;
import org.opencastproject.participation.model.Semester;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author John Crossman
 */
public class JSONFeedComparatorTest {

  private Set<HasCourseKey> set;

  @Before
  public void before() {
    set = new TreeSet<HasCourseKey>(new JSONFeedComparator());
  }

  @Test
  public void testCompareNull() {
    try {
      set.add(new CourseKey(2010, null, 123));
      set.add(new CourseKey(2009, null, 123));
      final Iterator<HasCourseKey> iterator = set.iterator();
      assertEquals(2009, iterator.next().getYear());
      assertEquals(2010, iterator.next().getYear());
    } catch (final Exception e) {
      fail("Comparator should be null tolerant. Instead we get " + e.getClass().getSimpleName() + " - " + e.getMessage());
    }
  }

  @Test
  public void testCompareYear() {
    set.add(new CourseKey(2010, Semester.Fall, 123));
    set.add(new CourseKey(1999, Semester.Fall, 123));
    final Iterator<HasCourseKey> iterator = set.iterator();
    assertEquals(1999, iterator.next().getYear());
    assertEquals(2010, iterator.next().getYear());
  }

  @Test
  public void testCompareSemester() {
    set.add(new CourseKey(2010, Semester.Fall, 123));
    set.add(new CourseKey(2010, Semester.Spring, 123));
    final Iterator<HasCourseKey> iterator = set.iterator();
    assertEquals(Semester.Spring, iterator.next().getSemester());
    assertEquals(Semester.Fall, iterator.next().getSemester());
  }

  @Test
  public void testCompareCCN() {
    set.add(new CourseKey(2010, Semester.Fall, 2));
    set.add(new CourseKey(2010, Semester.Fall, 1));
    final Iterator<HasCourseKey> iterator = set.iterator();
    assertEquals(1, iterator.next().getCcn());
    assertEquals(2, iterator.next().getCcn());
  }
}