/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.warehouse;

import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertSame;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author John Crossman
 */
public class ResultConverterForWarehouseTest extends AbstractResultConverterTest {

  @Test
  public void testConvertBuilding() throws SQLException {
    final Map<ColStr, String> args = new HashMap<ColStr, String>();
    args.put(ColStr.building_name, "LI KA SHING");
    args.put(ColStr.room_number, " 1 ");
    final WarehouseCourse w = convert(args);
    assertEquals(WarehouseBuilding.LiKaShing, w.getRoom().getBuilding());
    assertEquals("1", w.getRoom().getRoomNumber());
  }

  @Test
  public void testConvertEdoDbPauleyBallroom() throws SQLException {
    final Map<ColStr, String> args = new HashMap<ColStr, String>();
    args.put(ColStr.building_name, "Pauley Ballroom");
    args.put(ColStr.room_number, null);
    final WarehouseCourse w = convert(args);
    assertEquals(WarehouseBuilding.PauleyBallroom, w.getRoom().getBuilding());
    assertNull(w.getRoom().getRoomNumber());
  }

  @Test
  public void testConvertEdoDbWheelerAud() throws SQLException {
    final Map<ColStr, String> args = new HashMap<ColStr, String>();
    args.put(ColStr.building_name, " Wheeler");
    args.put(ColStr.room_number, "150");
    final WarehouseCourse w = convert(args);
    assertSame(WarehouseBuilding.Wheeler, w.getRoom().getBuilding());
    assertEquals("150", w.getRoom().getRoomNumber());
  }

  @Test
  public void testConvertEdoDbWheeler200() throws SQLException {
    final Map<ColStr, String> args = new HashMap<ColStr, String>();
    args.put(ColStr.building_name, " Wheeler ");
    args.put(ColStr.room_number, " 200 ");
    final WarehouseCourse w = convert(args);
    assertEquals(WarehouseBuilding.Wheeler, w.getRoom().getBuilding());
  }

  private WarehouseCourse convert(final Map<ColStr, String> args) throws SQLException {
    final ResultSet r = createMock(ResultSet.class);
    expectReturn(r, ColInt.year, 2016);
    expectReturn(r, ColStr.semester_code, "B");
    expectReturn(r, ColInt.ccn, 12345);
    expectReturn(r, ColStr.title);
    expectReturn(r, ColStr.catalog_id);
    expectReturn(r, ColStr.department_name);
    expectReturn(r, ColStr.audience_type);
    expectReturn(r, ColStr.recording_type, "screencast");
    expectReturn(r, ColInt.recording_id, 1);
    expectReturn(r, ColStr.recording_title);
    expectReturn(r, ColStr.recording_description);
    expectReturn(r, ColStr.youtube_video_id);
    expectReturn(r, ColDate.recording_start_utc, null);
    expectReturn(r, ColDate.recording_end_utc, null);
    expectReturn(r, ColStr.matterhorn_episode_id);
    expectReturn(r, ColStr.section);
    expectReturn(r, ColStr.license_type, "creative_commons");
    expectReturn(r, ColArray.meeting_days, null);
    expectReturn(r, ColArray.instructor_names, null);
    expectReturn(r, ColStr.description);
    expectReturn(r, ColStr.youtube_playlist_id);
    if (args != null) {
      for (final ColStr key : args.keySet()) {
        expectReturn(r, key, args.get(key));
      }
    }
    replay(r);
    return new ResultConverterForWarehouseImpl().convert(r);
  }

}
