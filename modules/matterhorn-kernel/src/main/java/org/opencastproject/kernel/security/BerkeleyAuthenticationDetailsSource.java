/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.security;

import org.opencastproject.util.env.ApplicationProfile;
import org.opencastproject.util.env.EnvironmentUtil;

import org.springframework.security.cas.web.authentication.ServiceAuthenticationDetails;
import org.springframework.security.cas.web.authentication.ServiceAuthenticationDetailsSource;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author John Crossman
 */
public class BerkeleyAuthenticationDetailsSource extends ServiceAuthenticationDetailsSource {

  private final ApplicationProfile profile;

  BerkeleyAuthenticationDetailsSource(final ApplicationProfile profile) {
    this.profile = profile;
  }

  public BerkeleyAuthenticationDetailsSource() {
    this(EnvironmentUtil.getApplicationProfile());
  }

  /**
   * @param request the {@code HttpServletRequest} object.
   * @return the {@code ServiceAuthenticationDetails} containing information about the current request
   */
  public ServiceAuthenticationDetails buildDetails(final HttpServletRequest request) {
    final ServiceAuthenticationDetails details = super.buildDetails(request);
    final String prefix = EnvironmentUtil.getHttpProtocol(profile) + "://";
    final String serviceURL = StringUtils.replace(details.getServiceUrl(), "http://", prefix);
    return new ServiceAuthenticationDetails() {
      @Override
      public String getServiceUrl() {
        return serviceURL;
      }
    };
  }

}
