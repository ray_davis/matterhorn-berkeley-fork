/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.opencastproject.util.env.EnvironmentUtil;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

/**
 * @author John Crossman
 */
public final class NotifierUtils {

  static final int MAX_WIDTH_SUBJECT_LINE = 100;

  private NotifierUtils() {
  }

  public static void setSubjectPerSendMode(final Message message, final SendMode sendMode) throws MessagingException {
    final String subject;
    final boolean prependEnvironmentInfo = !sendMode.equals(SendMode.productionMode)
        || isLimitedToGrizzlyPeakTeam(message.getAllRecipients());
    if (prependEnvironmentInfo) {
      final StringBuilder b = new StringBuilder(EnvironmentUtil.getHostName()).append(" : ").append(message.getSubject());
      subject = StringUtils.abbreviate(b.toString(), 0, MAX_WIDTH_SUBJECT_LINE) + getApplicationVersionInfo();
    } else {
      subject = message.getSubject();
    }
    message.setSubject(subject);
  }

  public static String getApplicationVersionInfo() {
    return new StringBuilder(" (").append(EnvironmentUtil.getPomVersion())
        .append(".").append(EnvironmentUtil.getGitRevision())
        .append(".").append(EnvironmentUtil.getBambooBuild()).append(")").toString();
  }

  /**
   * @see TeamAddress
   * @param addresses never null
   * @return true when addresses are within allowed range
   */
  static boolean isLimitedToGrizzlyPeakTeam(final Address[] addresses) {
    boolean developerTeamOnly = true;
    for (final Address address : addresses) {
      if (address == null) {
        continue;
      }
      final String emailAddress = (address instanceof InternetAddress) ? ((InternetAddress) address).getAddress() : null;
      final boolean limitedToGrizzlyPeakTeam = TeamAddress.grizzlyPeakDevelopers.getAddress().equalsIgnoreCase(emailAddress)
          || TeamAddress.grizzlyPeakErrors.getAddress().equalsIgnoreCase(emailAddress);
      if (!limitedToGrizzlyPeakTeam) {
        developerTeamOnly = false;
        break;
      }
    }
    return developerTeamOnly;
  }

  /**
   * When emailing an error report to the engineering team, provide object information relevant to context of error.
   * @param objects zero or more objects
   * @return toString of all objects provided
   */
  public static String getDebugInfo(final Object... objects) {
    final String debugInfo;
    if (objects != null && objects.length > 0) {
      final char lineBreak = '\n';
      final String separator = lineBreak + "------------------------------------------------------------------" + lineBreak;
      final StringBuilder b = new StringBuilder(separator).append(separator).append("Debug information").append(separator);
      for (final Object obj : objects) {
        if (obj != null) {
          try {
            final boolean simpleToString = obj instanceof CharSequence || obj instanceof Number;
            final String toString = simpleToString
                    ? ObjectUtils.toString(obj)
                    : ToStringBuilder.reflectionToString(obj, ToStringStyle.MULTI_LINE_STYLE);
            b.append("TYPE: ").append(obj.getClass().getSimpleName()).append(lineBreak).append(lineBreak).append(toString);
          } catch (final Throwable t) {
            b.append(t.getClass().getSimpleName()).append(" in toString() method: ").append(ExceptionUtils.getMessage(t));
          }
          b.append(separator);
        }
      }
      debugInfo = b.append(separator).toString();
    } else {
      debugInfo = StringUtils.EMPTY;
    }
    return debugInfo;
  }
}
