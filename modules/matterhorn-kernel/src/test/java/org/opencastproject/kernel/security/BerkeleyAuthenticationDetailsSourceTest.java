/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.security;

import org.junit.Test;
import org.opencastproject.util.env.ApplicationProfile;
import org.opencastproject.util.env.EnvironmentUtil;
import org.springframework.security.cas.web.authentication.ServiceAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertFalse;

/**
 * @author John Crossman
 */
public class BerkeleyAuthenticationDetailsSourceTest {

  @Test
  public void testBuildDetails() {
    final HttpServletRequest request = createMock(HttpServletRequest.class);
    expect(request.getScheme()).andReturn("http").anyTimes();
    expect(request.getServerName()).andReturn("foo-" + EnvironmentUtil.getEnvironment().name() + ".ets.berkeley.edu").anyTimes();
    expect(request.getServerPort()).andReturn(80).anyTimes();
    expect(request.getRequestURI()).andReturn("/welcome.html").anyTimes();
    expect(request.getRemoteAddr()).andReturn("foo").anyTimes();
    expect(request.getQueryString()).andReturn(null).anyTimes();
    //
    final HttpSession session = createMock(HttpSession.class);
    expect(session.getId()).andReturn("baz").anyTimes();
    expect(request.getSession(false)).andReturn(session).anyTimes();
    //
    replay(request, session);
    final ServiceAuthenticationDetails details = new BerkeleyAuthenticationDetailsSource(ApplicationProfile.engage).buildDetails(request);
    assertFalse(details.getServiceUrl().startsWith("https"));
  }

}
