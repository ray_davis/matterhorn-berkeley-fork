/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.LowLevelHttpResponse;
import com.google.api.client.testing.http.MockLowLevelHttpResponse;
import org.opencastproject.notify.SendType;
import org.opencastproject.util.env.EnvironmentUtil;

import org.junit.Before;
import org.junit.Test;
import org.osgi.service.cm.ConfigurationException;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author John Crossman
 */
public class SmtpServiceTest {

  private MockSmtpService smtpService;

  @Before
  public void before() throws ConfigurationException {
    smtpService = new MockSmtpService();
  }

  @Test
  public void testNotifyEngineeringTeamDev() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final SendType sendType = smtpService.notifyEngineeringTeam("hello", "world");
    assertEquals(SendType.messageSent, sendType);
    final MimeMessage messageSent = smtpService.getLastMessageSent();
    final String subject = messageSent.getSubject();
    assertTrue(subject.contains("hello"));
  }

  @Test
  public void testLogEmailWhenLocalEnv() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.neverSend);
    final String subject = "Hello World";
    final SendType sendType = smtpService.notifyEngineeringTeam(new Exception(subject));
    assertEquals(SendType.messageLoggedNotSent, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testDivertNecessary() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final MimeMessage message = getMessage("instructor@berkeley.edu");
    final String subject = message.getSubject();
    final SendType sendType = smtpService.send(message);
    assertEquals(SendType.messageDiverted, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testDivertNotNecessary() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final String subject = "Hello World";
    final SendType sendType = smtpService.notifyEngineeringTeam(new Exception(subject));
    assertEquals(SendType.messageSent, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testDivertFromTeamToDevelopers() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final MimeMessage message = getMessage(TeamAddress.grizzlyPeakErrors.getAddress());
    final String subject = message.getSubject();
    final SendType sendType = smtpService.send(message);
    assertEquals(SendType.messageSent, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testProduction() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.productionMode);
    final MimeMessage message = getMessage("instructor@berkeley.edu");
    final String subject = message.getSubject();
    assertEquals(SendType.messageSent, smtpService.send(message));
    assertEquals(subject, smtpService.getLastMessageSent().getSubject());
  }

  @Test
  public void testProductionEngineeringTeam() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.productionMode);
    final String subject = "Subject line";
    assertEquals(SendType.messageSent, smtpService.notifyEngineeringTeam(subject, "body text"));
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }


  @Test
  public void testGoogleJsonResponseExceptionNullDetails() throws Exception {
    // We want to be sure that NULL error details and NULL headers don't crash email creation.
    final LowLevelHttpResponse response = getLowLevelHttpResponse();
    final HttpResponseException.Builder builder = getHttpResponseExceptionBuilder(response, new HttpHeaders());
    final GoogleJsonResponseException exception = construct(builder, null);
    smtpService.notifyEngineeringTeam(exception);
    final MimeMessage messageSent = smtpService.getLastMessageSent();
    final String email = messageSent.getContent().toString();
    assertTrue(email.contains("Google"));
    assertTrue(email.contains(response.getStatusCode() + ""));
    assertTrue(email.contains(response.getReasonPhrase()));
  }

  @Test
  public void testGoogleJsonResponseException() throws Exception {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    // Mock response
    final GoogleJsonError error = new GoogleJsonError();
    final int googleJsonErrorCode = 123;
    error.setMessage("googleJsonErrorMessage");
    error.setCode(googleJsonErrorCode);
    final String errorFieldName = "errorFieldName";
    final String errorValue = "errorValue";
    error.set(errorFieldName, errorValue);
    final List<GoogleJsonError.ErrorInfo> errors = new LinkedList<GoogleJsonError.ErrorInfo>();
    final String errorFieldNameInner = "errorFieldNameInner";
    final String errorValueInner = "errorValueInner";
    errors.add(new GoogleJsonError.ErrorInfo().set(errorFieldNameInner, errorValueInner));
    error.setErrors(errors);
    //
    final String headerKey = "headerKey";
    final String headerValue = "headerValue";
    final HttpHeaders headers = new HttpHeaders();
    headers.set(headerKey, headerValue);
    final LowLevelHttpResponse response = getLowLevelHttpResponse(headerKey, headerValue);
    final HttpResponseException.Builder builder = getHttpResponseExceptionBuilder(response, headers);
    final GoogleJsonResponseException exception = construct(builder, error);
    final String[] expected = { headerKey.toLowerCase(), headerValue, errorFieldName, errorValue, errorFieldNameInner,
            errorFieldNameInner };
    testGoogleJsonResponseException(exception, 0, response, error, expected);
    testGoogleJsonResponseException(exception, 1, response, error, expected);
    testGoogleJsonResponseException(exception, 2, response, error, expected);
  }

  private void testGoogleJsonResponseException(final GoogleJsonResponseException cause, final int wrapCount,
          final LowLevelHttpResponse response, final GoogleJsonError error, final String... expected) throws Exception {
    final Throwable e;
    switch (wrapCount) {
      case 0:
        e = cause;
        break;
      case 1:
        e = new IOException(cause);
        break;
      case 2:
        e = new IOException(new IOException(cause));
        break;
      default:
        throw new UnsupportedOperationException("We do not test for wrap count " + wrapCount);
    }
    // Assert
    smtpService.notifyEngineeringTeam(e);
    final MimeMessage messageSent = smtpService.getLastMessageSent();
    final String email = messageSent.getContent().toString();
    assertTrue(email.contains("Google"));
    assertTrue(email.contains(response.getStatusCode() + ""));
    assertTrue(email.contains(response.getReasonPhrase()));
    // GoogleJsonError
    assertTrue(email.contains(error.getCode() + ""));
    assertTrue(email.contains(error.getMessage()));
    // Arbitrary values we expect
    for (final String next : expected) {
      assertTrue("Not found (where wrapCount=" + wrapCount + ") : " + next, email.contains(next));
    }
  }

  @Test
  public void testTestEmailAtStartup() throws MessagingException, ConfigurationException {
    smtpService.overrideDefaultSendMode(SendMode.neverSend);
    final Properties p = new Properties();
    p.put("mail.test", "true");
    smtpService.updatedConfiguration(p);
    final MimeMessage messageSent = smtpService.getLastMessageSent();
    assertNotNull(messageSent);
    assertTrue(StringUtils.hasText(messageSent.getSubject()));
  }

  private void assertProcessedSubjectLine(final String subject, final MimeMessage message) throws MessagingException {
    final String[] expectedFragments = new String[] {
        EnvironmentUtil.getPomVersion(),
        EnvironmentUtil.getGitRevision(),
        EnvironmentUtil.getBambooBuild(),
        subject
    };
    final String processedSubject = message.getSubject();
    for (final String expected : expectedFragments) {
      assertTrue("'" + expected + "' not found in " + processedSubject, processedSubject.contains(expected));
    }
  }

  private MimeMessage getMessage(final String address) throws MessagingException {
    final MimeMessage message = smtpService.createMessage();
    message.setSubject("Unique subject line: " + message.hashCode());
    message.setRecipient(Message.RecipientType.TO, new InternetAddress(address));
    return message;
  }

  private LowLevelHttpResponse getLowLevelHttpResponse() {
    return getLowLevelHttpResponse(null, null);
  }

  private LowLevelHttpResponse getLowLevelHttpResponse(final String headerKey, final String headerValue) {
    final MockLowLevelHttpResponse response = new MockLowLevelHttpResponse()
            .setStatusCode(503).setReasonPhrase("Service Unavailable");
    if (headerKey != null) {
      response.addHeader(headerKey, headerValue);
    }
    return response;
  }

  private HttpResponseException.Builder getHttpResponseExceptionBuilder(final LowLevelHttpResponse response, final HttpHeaders headers)
          throws IOException {
    return new HttpResponseException.Builder(response.getStatusCode(), response.getReasonPhrase(), headers);
  }

  private GoogleJsonResponseException construct(final HttpResponseException.Builder builder, final GoogleJsonError error) throws Exception {
    final Class[] signature = { HttpResponseException.Builder.class, GoogleJsonError.class };
    return construct(GoogleJsonResponseException.class, signature, builder, error);
  }

  private <T> T construct(final Class<T> clazz, final Class[] constructorSignature, final Object... constructorArgs)
          throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException,
          InstantiationException {
    final Class<?> c = Class.forName(clazz.getName());
    final Constructor<?> constructor = c.getDeclaredConstructor(constructorSignature);
    constructor.setAccessible(true);
    return (T) constructor.newInstance(constructorArgs);
  }

}
