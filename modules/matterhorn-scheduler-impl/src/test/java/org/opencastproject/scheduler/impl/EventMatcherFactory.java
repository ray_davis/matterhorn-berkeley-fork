/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.scheduler.impl;

import static org.opencastproject.scheduler.impl.EventAssert.assertThat;

import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.scheduler.impl.EventPropertyValue.EventProperty;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matchers for {@code DublinCoreCatalog}-based events.
 *  
 * @author Fernando Alvarez
 */
public final class EventMatcherFactory {
  private EventMatcherFactory() {
    throw new AssertionError();
  }
  
  /**
   * A {@code DublinCoreCatalog}-based event has a value equal to the value provided as evaluated by {@link EventAssert} assertions.
   * 
   * @param eventProperty  determines which of the event's properties the value will be compared to
   * @param value  is compared to the event's corresponding value
   * @return  Matcher<DublinCoreCatalog> instance
   */
  public static Matcher<DublinCoreCatalog> isEventWithPropertyValue(final EventProperty eventProperty, final String value) {
    return new EventMatcher<DublinCoreCatalog>(eventProperty, value) {

      @Override
      protected boolean matchesSafely(DublinCoreCatalog item) {
        assertThat(item).hasSame(eventProperty, value);
        return true;
      }
    };
  }
  
  /**
   * A {@code DublinCoreCatalog}-based event is generated from a template if its properties match each of the properties of the 
   * template as evaluated by {@link EventAssert} assertions.
   * 
   * @param template  DublinCoreCatalog with expected values
   * @return  Matcher<DublinCoreCatalog> instance
   */
  public static Matcher<DublinCoreCatalog> isEventGeneratedFromTemplate(final DublinCoreCatalog template) {
    return new EventMatcher<DublinCoreCatalog>(template) {

      @Override
      protected boolean matchesSafely(DublinCoreCatalog item) {
        assertThat(item).hasSamePropertyValuesAs(template);
        return true;
      }
    };
  }
  
  /**
   * A {@code DublinCoreCatalog}-based event ("actual") occurs within another event("expected")'s temporal range if: 
   * <ul>
   * <li>Actual's start date and time is on or after expected's start date and time
   * <li>Actual's end date and time is on or before expected's end date and time
   * <ul>
   * 
   * @param expected  DublinCoreCatalog with expected temporal range
   * @return  Matcher<DublinCoreCatalog> instance
   */
  public static Matcher<DublinCoreCatalog> occursWithinTemporalRangeOf(final DublinCoreCatalog expected) {
    return new EventMatcher<DublinCoreCatalog>(expected) {

      @Override
      protected boolean matchesSafely(DublinCoreCatalog item) {
        assertThat(item).occursWithinTemporalRangeOf(expected);
        return true;
      }
    };
  }
  
  private abstract static class EventMatcher<T extends DublinCoreCatalog> extends TypeSafeMatcher<T> {
    protected final T expected;
    protected final String value;
    protected EventProperty eventProperty;

    private EventMatcher(T expected) {
      super();
      this.expected = expected;
      this.value = null;
      this.eventProperty = null;
    }
    
    private EventMatcher(EventProperty eventProperty, String value) {
      super();
      this.expected = null;
      this.value = value;
      this.eventProperty = eventProperty;
    }
    
    @Override
    public void describeTo(Description description) {
      description.appendText("Expected Event properties are ").appendValue(expected);
    }
    
    protected abstract boolean matchesSafely(T item);
  }
}
