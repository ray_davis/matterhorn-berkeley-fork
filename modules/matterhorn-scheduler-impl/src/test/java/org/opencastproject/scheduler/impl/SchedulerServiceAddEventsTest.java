/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.scheduler.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_IS_PART_OF;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_SPATIAL;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_TEMPORAL;
import static org.opencastproject.metadata.dublincore.DublinCore.PROPERTY_TITLE;
import static org.opencastproject.scheduler.impl.EventMatcherFactory.isEventWithPropertyValue;
import static org.opencastproject.scheduler.impl.EventMatcherFactory.occursWithinTemporalRangeOf;
import static org.opencastproject.scheduler.impl.EventPropertyValue.EventProperty.EVENT_TITLE;
import static org.opencastproject.scheduler.impl.EventPropertyValue.EventProperty.PUBLISH_DELAY;
import static org.opencastproject.scheduler.impl.EventPropertyValue.EventProperty.DATE_RANGE;

import org.opencastproject.capture.CaptureParameters;
import org.opencastproject.ingest.api.IngestService;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElements;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.metadata.dublincore.DublinCoreCatalogImpl;
import org.opencastproject.scheduler.api.SchedulerService;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.workflow.api.WorkflowDefinition;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * A suite of tests focusing on the generation of multiple events from a template {@code DublinCoreCatalog}.
 * <p>
 * The intent of these tests is not to provide 100% coverage of the {@code addReccuringEvent} method, but rather to ensure that any 
 * enhancements or refactoring performed by the Grizzly Peak team does not break the fundamental functionality provided by said method.
 * <p>
 * Coverage: 
 * <p><ul>
 * <li>{@link SchedulerServiceImpl#addReccuringEvent}
 * <ul><p>
 * 
 * @author Fernando Alvarez
 */

@RunWith(MockitoJUnitRunner.class)
public class SchedulerServiceAddEventsTest {
  private static final String SERIES_ID = "2014B69490";
  private static final String TIME_ZONE = "America/Los_Angeles";
  private SchedulerService service;
  private SchedulerServiceImpl serviceImpl;
  @Mock private WorkflowService workflowService;
  @Mock private SeriesService seriesService;
  @Mock private DublinCoreCatalog series;
  @Mock private IngestService ingestService;
  @Mock private SchedulerServiceDatabase schedulerServiceDatabase;
  @Mock private SchedulerServiceIndex schedulerServiceIndex;
  
  /**
   * The base case, verifies that assumptions made in setting up the fixture are still correct; this is not as trivial 
   * a test as it may seem, since it will fail if:
   * <p><ul>
   * <li> The fields required by {@code SchedulerServiceImpl#addReccuringEvent} has changed
   * <li> The collaborators invoked directly or indirectly by {@code SchedulerServiceImpl#addReccuringEvent} has changed
   * <ul><p>
   * <p>
   * Note that the verification of collaborators, while appropriate for all tests, is factored out and performed solely in 
   * this test so that the other tests can focus on their specific goals as well as reducing overall test execution time. 
   */
  @Test
  public void addsRecurringEventsWithRequiredFieldsOnly() throws Exception {
    final DublinCoreCatalog template = getDublinCoreCatalogSeededWithRequiredFields();
    
    final Long[] eventIds = service.addReccuringEvent(template, anyMapOf(String.class, String.class)); 
    
    final int recurringEventCount = 3;
    assertThat(eventIds).hasSize(recurringEventCount);
    verify(schedulerServiceIndex, times(recurringEventCount)).index(argThat(occursWithinTemporalRangeOf(template)));
    verify(schedulerServiceDatabase, times(recurringEventCount)).updateEventWithMetadata(anyLong(), any(Properties.class));
    verify(schedulerServiceIndex, times(recurringEventCount)).index(anyLong(), any(Properties.class));
    verify(workflowService, times(recurringEventCount)).start(any(WorkflowDefinition.class), any(MediaPackage.class), anyMapOf(String.class, String.class));
    verify(ingestService, times(recurringEventCount)).addCatalog(any(InputStream.class), anyString(), eq(MediaPackageElements.EPISODE), any(MediaPackage.class));
    verify(ingestService, times(recurringEventCount)).addCatalog(any(InputStream.class), anyString(), eq(MediaPackageElements.SERIES), any(MediaPackage.class));
  }
  
  /**
   * Generates multiple events from a template {@code DublinCoreCatalog}, the number of events being determined by 
   * a date range filtered by a recurrence rule.
   */
  @Test
  public void addsRecurringEventsFromStartDateToEndDateInclusive() throws Exception {
    final DublinCoreCatalog template = getDublinCoreCatalogSeededWithRequiredFields();
    // 43 weekdays in May - June 2014
    template.set(PROPERTY_TEMPORAL, "start=2014-05-01T19:07:00Z; end=2014-06-30T20:05:00Z; scheme=W3C-DTF;");
    template.set(DublinCoreCatalogImpl.PROPERTY_RECURRENCE, "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;BYHOUR=19;BYMINUTE=7");
    
    final Long[] eventIds = service.addReccuringEvent(template, anyMapOf(String.class, String.class)); 
    
    final int recurringEventCount = 43;
    assertThat(eventIds).hasSize(recurringEventCount);
  }
  
  /**
   * Generates multiple events from a template {@code DublinCoreCatalog}, with each event's start date and end date adjusted  
   * for Daylight Savings Time and the whole period spanning PDT -> PST.
   */
  @Test
  public void addsRecurringEventsWithStartDateSpanningPdtToPst() throws Exception {
    final DublinCoreCatalog template = getDublinCoreCatalogSeededWithRequiredFields();
    template.set(PROPERTY_TEMPORAL, "start=2014-10-27T19:07:00Z; end=2014-11-03T21:05:00Z; scheme=W3C-DTF;");
    template.set(DublinCoreCatalogImpl.PROPERTY_RECURRENCE, "FREQ=WEEKLY;BYDAY=MO;BYHOUR=19;BYMINUTE=7");
    
    service.addReccuringEvent(template, anyMapOf(String.class, String.class)); 
    
    verify(schedulerServiceDatabase).storeEvents(
            argThat(isEventWithPropertyValue(DATE_RANGE, "start=2014-10-27T19:07:00Z; end=2014-10-27T20:05:00Z; scheme=W3C-DTF;")),
            argThat(isEventWithPropertyValue(DATE_RANGE, "start=2014-11-03T20:07:00Z; end=2014-11-03T21:05:00Z; scheme=W3C-DTF;")));
  }
  
  /**
   * Generates multiple events from a template {@code DublinCoreCatalog}, with each event's start date and end date adjusted  
   * for Daylight Savings Time and the whole period spanning PST -> PDT.
   */
  @Test
  public void addsRecurringEventsWithStartDateSpanningPstToPdt() throws Exception {
    final DublinCoreCatalog template = getDublinCoreCatalogSeededWithRequiredFields();
    template.set(PROPERTY_TEMPORAL, "start=2014-03-03T20:07:00Z; end=2014-03-10T20:05:00Z; scheme=W3C-DTF;");
    template.set(DublinCoreCatalogImpl.PROPERTY_RECURRENCE, "FREQ=WEEKLY;BYDAY=MO;BYHOUR=20;BYMINUTE=7");
    
    service.addReccuringEvent(template, anyMapOf(String.class, String.class)); 
    
    verify(schedulerServiceDatabase).storeEvents(
            argThat(isEventWithPropertyValue(DATE_RANGE, "start=2014-03-03T20:07:00Z; end=2014-03-03T21:05:00Z; scheme=W3C-DTF;")),
            argThat(isEventWithPropertyValue(DATE_RANGE, "start=2014-03-10T19:07:00Z; end=2014-03-10T20:05:00Z; scheme=W3C-DTF;")));
  }
  
  /**
   * If the value of {@code recurringEventDateFormat} is null, then the title of each generated event will be comprised of the 
   * {@code PROPERTY_TITLE} as provided in the template {@code DublinCoreCatalog} concatenated with a suffix identifying said event 
   * as the nth occurrence generated from the template. The suffix is in the form {@literal " - Lecture [1-9]*"}.
   */
  @Test
  public void addsRecurringEventsWithSequentialDigitInRecordingTitle() throws Exception {
    final DublinCoreCatalog template = getDublinCoreCatalogSeededWithRequiredFields();
    
    service.addReccuringEvent(template, anyMapOf(String.class, String.class)); 
    
    verify(schedulerServiceDatabase).storeEvents(argThat(isEventWithPropertyValue(EVENT_TITLE, "Physics 137A - Lecture 1")), 
                                                any(DublinCoreCatalog.class), 
                                                argThat(isEventWithPropertyValue(EVENT_TITLE, "Physics 137A - Lecture 3")));
  }
  
  /**
   * If the value of {@code recurringEventDateFormat} is a valid {@code SimpleDateFormat} pattern, then the title of each generated event 
   * will be comprised of the {@code PROPERTY_TITLE} as provided in the template {@code DublinCoreCatalog} concatenated with a suffix 
   * representing the start date for the event. The form of the suffix is dependent on the pattern specified, with the canonical form 
   * being {@literal " - yyyy-MM-dd"}.
   */
//TODO need to add tests of invalid patterns
  @Test
  public void addsRecurringEventsWithDateInRecordingTitle() throws Exception {
    final DublinCoreCatalog template = getDublinCoreCatalogSeededWithRequiredFields();
    final Properties serviceProperties = new Properties();
    final String recurringEventDateFormat = "yyyy-MM-dd";
    final String recurringEventDateFormatKey = "org.opencastproject.scheduler.impl.SchedulerServiceImpl.recurringEventDateFormat";
    serviceProperties.put(recurringEventDateFormatKey, recurringEventDateFormat);
    serviceImpl.updated(serviceProperties);
    
    service.addReccuringEvent(template, anyMapOf(String.class, String.class)); 
    
    verify(schedulerServiceDatabase).storeEvents(argThat(isEventWithPropertyValue(EVENT_TITLE, "Physics 137A - 2014-05-05")), 
                                                 any(DublinCoreCatalog.class), 
                                                 argThat(isEventWithPropertyValue(EVENT_TITLE, "Physics 137A - 2014-05-09")));
  }
  
  /**
   * A Publish Delay value, representing a date some days after the event's start date, will be added to the event's 
   * {@code DublinCoreCatalog} episode catalog as a {@code PROPERTY_AVAILABLE} element; the number of days will be 
   * taken from the {@code wfProperties} map passed to the {@code addReccuringEvent} method. 
   */
  @Test
  public void addsRecurringEventsWithPublishDelayedSevenDays() throws Exception {
    final DublinCoreCatalog template = getDublinCoreCatalogSeededWithRequiredFields();
    final Integer publishDelayDays = 7;
    final Map<String, String> wfProperties = new HashMap<String, String>();
    wfProperties.put(CaptureParameters.DELAY_PUBLISH_BY_DAYS, publishDelayDays.toString());
    
    service.addReccuringEvent(template, wfProperties); 
    
    verify(schedulerServiceDatabase).storeEvents(argThat(isEventWithPropertyValue(PUBLISH_DELAY, "start=2014-05-12T12:07:00Z")), 
                                                 any(DublinCoreCatalog.class), 
                                                 argThat(isEventWithPropertyValue(PUBLISH_DELAY, "start=2014-05-16T12:07:00Z")));
  }
  
  
  @Before
  public void instantiateSchedulerServiceImplAndInjectCollaborators() throws Exception {
    when(series.getFirst(PROPERTY_TITLE)).thenReturn("Physics 137A, 002 - Spring 2014");
    when(series.toXmlString()).thenReturn("");
    when(seriesService.getSeries(SERIES_ID)).thenReturn(series);
    when(workflowService.start(any(WorkflowDefinition.class), any(MediaPackage.class), anyMapOf(String.class, String.class))).thenReturn(mock(WorkflowInstance.class));
    
    serviceImpl = new SchedulerServiceImpl();
    serviceImpl.setPersistence(schedulerServiceDatabase);
    serviceImpl.setWorkflowService(workflowService);
    serviceImpl.setIngestService(ingestService);
    serviceImpl.setIndex(schedulerServiceIndex);
    serviceImpl.setSeriesService(seriesService);
    service = serviceImpl;
  }
  // Of the many fields persisted in an episode's DublinCoreCatalog, these are the only fields needed to invoke the 
  // addReccuringEvent method. Tests will override these values only when doing so is required in order to conduct the test.
  public DublinCoreCatalogImpl getDublinCoreCatalogSeededWithRequiredFields() {
    final DublinCoreCatalogImpl impl = DublinCoreCatalogImpl.newInstance();
    impl.set(PROPERTY_TITLE, "Physics 137A");
    impl.set(PROPERTY_TEMPORAL, "start=2014-05-05T19:07:00Z; end=2014-05-09T20:05:00Z; scheme=W3C-DTF;");
    impl.set(PROPERTY_SPATIAL, "giannini-141");
    impl.set(PROPERTY_IS_PART_OF, SERIES_ID);
    impl.set(DublinCoreCatalogImpl.PROPERTY_RECURRENCE, "FREQ=WEEKLY;BYDAY=MO,WE,FR;BYHOUR=19;BYMINUTE=7");
    impl.set(DublinCoreCatalogImpl.PROPERTY_AGENT_TIMEZONE, TIME_ZONE);
    return impl;
  }
}
